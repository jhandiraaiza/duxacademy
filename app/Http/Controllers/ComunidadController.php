<?php

namespace App\Http\Controllers;

use App\Models\comunidad;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ComunidadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $datosc['comunidad']=comunidad::paginate(20);
        return view('registros.comunidad.index', $datosc);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('registros.comunidad.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //	

        $campos=[
            'titulo'=>'required|string|max:80000',
            'expositor'=>'required|string|max:80000',
            'apel_expositor'=>'required|string|max:80000',
            'descripcion'=>'required|string|max:80000',
            'link'=>'required|string|max:80000',
            'foto'=>'required|max:1000000000|mimes:jpeg,png,jpg,gif',
            
            
        ];

        $mensaje=[
            'required'=>'La :attribute es requerida',
            'foto.required'=>'La foto es requerida',
        ];

        $this->validate($request,$campos,$mensaje);



        $datocomunidad= request()->except('_token');

        if($request->hasFile('foto')){

            $datocomunidad['foto']=$request->file('foto')->store('comunidad','public');
        }
        comunidad::insert($datocomunidad);
        return redirect('home/comunidad')->with('mensaje','Se agrego un nueva comunidad');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\comunidad  $comunidad
     * @return \Illuminate\Http\Response
     */
    public function show(comunidad $comunidad)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\comunidad  $comunidad
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $comunidad=comunidad::findOrFail($id);
        return view('registros.comunidad.edit',compact('comunidad'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\comunidad  $comunidad
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $campos=[
            'titulo'=>'required|string|max:80000',
            'expositor'=>'required|string|max:80000',
            'apel_expositor'=>'required|string|max:80000',
            'descripcion'=>'required|string|max:80000',
            'link'=>'required|string|max:80000',
            

        ];

        $mensaje=[
            'required'=>'La :attribute es requerida',
            
       
        ];
        if($request->hasFile('foto')){

            $campos=[

                'foto'=>'required|max:1000000000|mimes:jpeg,png,jpg,gif',
            ];
    
            $mensaje=[
  
                'imagen.required'=>'la imagen es requerida',
            ];

        }

        $this->validate($request,$campos,$mensaje);


        $datocomunidad= request()->except(['_token','_method']);
        if($request->hasFile('foto')){
            $comunidad=comunidad::findOrFail($id);
            Storage::delete('public/'.$comunidad->imagen);

            $datocomunidad['foto']=$request->file('foto')->store('comunidad','public');
        }

        
        comunidad::where('id','=',$id)->update($datocomunidad);
        $comunidad=comunidad::findOrFail($id);

        return redirect('home/comunidad')->with('mensaje','Se edito exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\comunidad  $comunidad
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $comunidad=comunidad::findOrFail($id);
        if(Storage::delete('public/'.$comunidad->foto)){
            comunidad::destroy($id);
        }
        return redirect('home/comunidad')->with('mensaje','Se elimino la comunidad');
    }
}
