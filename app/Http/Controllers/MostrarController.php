<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\personal;
use App\Models\rol;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Models\Premios;
use App\Models\cursos;
use App\Models\User;
use App\Models\Noticias;
use App\Models\comunidad;

class MostrarController extends Controller
{
    //
    public function index()
    {
        
        $datospersonal['personal']=personal::paginate(5);
        $rolg['rol_registrado']= rol::all();
        $datospremio['premio']=Premios::paginate(5);
        $datoscurso['curso']=cursos::paginate(4);
        $datosnoticia['noticia']=Noticias::paginate(4);
        $datoscomunidad['comunidad']=comunidad::paginate(3);
        return view('welcome',$datospersonal,$rolg)->with($datospremio)->with($datoscurso)->with($datosnoticia)->with($datoscomunidad);
    }
    
}

