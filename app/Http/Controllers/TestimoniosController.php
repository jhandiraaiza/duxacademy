<?php

namespace App\Http\Controllers;

use App\Models\testimonios;
use Illuminate\Http\Request;

class TestimoniosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $datostest['testimonios']=testimonios::paginate(15);
        return view('registros.testimonio.index', $datostest);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('registros.testimonio.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //			
        $campos=[
            'nombre'=>'required|string|max:200',
            'pais'=>'required|string|max:200',
            'ciudad'=>'required|string|max:200',
            'testimonio'=>'required|string|max:80000',
       
        ];

        $mensaje=[
            'required'=>'El :attribute es requerido',
        ];

        $this->validate($request,$campos,$mensaje);

        $datotest= request()->except('_token');

        testimonios::insert($datotest);
        //return response()->json($datocurso);
        return redirect('home/testimonio')->with('mensaje','Se agrego un nuevo testimonio');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\testimonios  $testimonios
     * @return \Illuminate\Http\Response
     */
    public function show(testimonios $testimonios)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\testimonios  $testimonios
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $testimonio=testimonios::findOrFail($id);
        return view('registros.testimonio.edit',compact('testimonio'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\testimonios  $testimonios
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $campos=[
            'nombre'=>'required|string|max:200',
            'pais'=>'required|string|max:200',
            'ciudad'=>'required|string|max:200',
            'testimonio'=>'required|string|max:80000',

        ];

        $mensaje=[
            'required'=>'El :attribute es requerido',
       
        ];
        

        $this->validate($request,$campos,$mensaje);



        $datotest= request()->except(['_token','_method']);
        testimonios::where('id','=',$id)->update($datotest);
        $testimonio=testimonios::findOrFail($id);

        //return view('registros.curso.edit',compact('curso'));
        return redirect('home/testimonio')->with('mensaje','Se edito el testimonio exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\testimonios  $testimonios
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $testimonio=testimonios::findOrFail($id);
        testimonios::destroy($id);
        return redirect('home/testimonio')->with('mensaje','Se elimino el testimonioexitosamente');
    }
}
