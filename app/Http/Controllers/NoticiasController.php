<?php

namespace App\Http\Controllers;

use App\Models\Noticias;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class NoticiasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $datosnoticias['noticias']=Noticias::paginate(15);
        return view('registros.noticias.index', $datosnoticias);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('registros.noticias.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $campos=[
            'titulo'=>'required|string|max:200',
            'descripcion'=>'required|string|max:80000',
            'fecha'=>'required|string|max:200',
            'link'=>'required|string|max:200',
            'imagen'=>'required|max:1000000000|mimes:jpeg,png,jpg,gif',
        ];

        $mensaje=[
            'required'=>'El :attribute es requerido',
            'descripcion.required'=>'La descripcion es requerida',
            'fecha.required'=>'La fecha es requerida',
            'imagen.required'=>'La imagen es requerida',
        ];

        $this->validate($request,$campos,$mensaje);

        $datonoticia= request()->except('_token');

        if($request->hasFile('imagen')){

            $datonoticia['imagen']=$request->file('imagen')->store('noticia','public');
        }
        Noticias::insert($datonoticia);
        //return response()->json($datocurso);
        return redirect('home/noticias')->with('mensaje','Se agrego un nueva noticia');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Noticias  $noticias
     * @return \Illuminate\Http\Response
     */
    public function show(Noticias $noticias)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Noticias  $noticias
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $noticia=Noticias::findOrFail($id);
        return view('registros.noticias.edit',compact('noticia'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Noticias  $noticias
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        //
        $campos=[
            'titulo'=>'required|string|max:200',
            'descripcion'=>'required|string|max:80000',
            'fecha'=>'required|string|max:200',
            'link'=>'required|string|max:200',

        ];

        $mensaje=[
            'required'=>'El :attribute es requerido',
            'descripcion.required'=>'La descripcion es requerida',
            'fecha.required'=>'La fecha es requerida',
       
        ];
        if($request->hasFile('imagen')){

            $campos=[

                'imagen'=>'required|max:1000000000|mimes:jpeg,png,jpg,gif',
            ];
    
            $mensaje=[
  
                'imagen.required'=>'La imagen es requerida',
            ];

        }

        $this->validate($request,$campos,$mensaje);



        $datonoticia= request()->except(['_token','_method']);
        if($request->hasFile('imagen')){
            $noticia=Noticias::findOrFail($id);
            Storage::delete('public/'.$noticia->imagen);

            $datonoticia['imagen']=$request->file('imagen')->store('noticia','public');
        }

        
        Noticias::where('id','=',$id)->update($datonoticia);
        $noticia=Noticias::findOrFail($id);

        //return view('registros.curso.edit',compact('curso'));
        return redirect('home/noticias')->with('mensaje','Se edito la noticia exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Noticias  $noticias
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $noticia=Noticias::findOrFail($id);
        if(Storage::delete('public/'.$noticia->imagen)){
            Noticias::destroy($id);
        }
        return redirect('home/noticias')->with('mensaje','Se elimino la noticia exitosamente');
    }
}
