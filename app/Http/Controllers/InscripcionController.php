<?php

namespace App\Http\Controllers;

use App\Models\inscripcion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use App\Mail\EmergencyCallReceived;

class InscripcionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $datosinscritos['inscripcion']=inscripcion::paginate(15);
        return view('registros.inscritos.index', $datosinscritos);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //curso
       

        $datoinscrito= request()->except('_token');

        if($request->hasFile('imagen')){

            $datoinscrito['imagen']=$request->file('imagen')->store('inscrito','public');
        }
        inscripcion::insert($datoinscrito);
        //return response()->json($datoinscrito);
        return redirect('/cursos')->with('mensaje','Registro exitoso espere respuesta');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\inscripcion  $inscripcion
     * @return \Illuminate\Http\Response
     */
    public function show(inscripcion $inscripcion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\inscripcion  $inscripcion
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $inscripcion=inscripcion::findOrFail($id);
        return view('registros.inscritos.edit',compact('inscripcion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\inscripcion  $inscripcion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $datoinscrito= request()->except(['_token','_method']);
        if($request->hasFile('imagen')){
            $inscrito=inscripcion::findOrFail($id);
            Storage::delete('public/'.$inscrito->imagen);

            $datoinscrito['imagen']=$request->file('imagen')->store('inscrito','public');
        }

        
        inscripcion::where('id','=',$id)->update($datoinscrito);
        $inscrito=inscripcion::findOrFail($id);

        //return view('registros.curso.edit',compact('curso'));
        return redirect('home/inscripcion')->with('mensaje','Se edito exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\inscripcion  $inscripcion
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $inscrito=inscripcion::findOrFail($id);
        if(Storage::delete('public/'.$inscrito->imagen)){
            inscripcion::destroy($id);
        }
        return redirect('home/inscripcion')->with('mensaje','Se elimino exitosamente');
    }

    public function enviar($id)
    {
        $data=array($id);
        $inscrito=inscripcion::findOrFail($id);
        $correo = $inscrito->correo;
        $nombre=$inscrito->nombre;
        $mensaje=$inscrito->mensaje;




       /* Mail::send('mails.emergency_call', array($correo,$nombre,$mensaje), function($message) {
            $message->to($correo, 'Jon Doe')->subject('Welcome to the Laravel 4 Auth App!');
        });*/

        $mail = Mail::to($correo)->send(new EmergencyCallReceived($nombre,$mensaje));
        return redirect('home/inscripcion')->with('mensaje','Mensaje enviado');

        //$mail = Mail::to($correo,$nombre)->send(new EmergencyCallReceived);
       // return Redirect::back()->with('status', 'Email Sent Success');

        
    
    }
}
