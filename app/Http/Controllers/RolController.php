<?php

namespace App\Http\Controllers;

use App\Models\rol;
use Illuminate\Http\Request;

class RolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $rolg['rol_registrado']= rol::paginate(5);
        return view('registros.roles.index',$rolg);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('registros.roles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //$datorol= request()->all();
        $campos=[
            'nombre_rol'=>'required|string|max:200',
        ];

        $mensaje=[
            'nombre_rol.required'=>'Se requiere llenar el espacio',
        ];

        $this->validate($request,$campos,$mensaje);
        $datorol= request()->except('_token');
        rol::insert($datorol);
        //return response()->json($datorol);
        return redirect('home/rol')->with('mensaje','Rol agregado');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\rol  $rol
     * @return \Illuminate\Http\Response
     */
    public function show(rol $rol)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\rol  $rol
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $rol=rol::findOrFail($id);
        return view('registros.roles.edit',compact('rol'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\rol  $rol
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $campos=[
            'nombre_rol'=>'required|string|max:200',
        ];

        $mensaje=[
            'nombre_rol.required'=>'Se requiere llenar el espacio',
        ];

        $this->validate($request,$campos,$mensaje);
        $datorol= request()->except(['_token','_method']);
        rol::where('id','=',$id)->update($datorol);

        $rol=rol::findOrFail($id);
        //return view('registros.roles.edit',compact('rol'));
        return redirect('home/rol')->with('mensaje','Rol editado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\rol  $rol
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        rol::destroy($id);
        return redirect('/home/rol')->with('mensaje','Rol Eliminado');

    }
}
