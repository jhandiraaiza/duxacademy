<?php

namespace App\Http\Controllers;

use App\Models\Tips;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class TipsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $datostips['tips']=Tips::paginate(15);
        return view('registros.tips.index', $datostips);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('registros.tips.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $campos=[
            'titulo'=>'required|string|max:200',
            'descripcion'=>'required|string|max:80000',
            'links'=>'required|string|max:200',
            'imagen'=>'required|max:1000000000|mimes:jpeg,png,jpg,gif',
        ];

        $mensaje=[
            'required'=>'El :attribute es requerido',
            'descripcion.required'=>'La descripcion es requerida',
            'imagen.required'=>'La imagen es requerida',
            
        ];

        $this->validate($request,$campos,$mensaje);




        $datostips= request()->except('_token');

        if($request->hasFile('imagen')){

            $datostips['imagen']=$request->file('imagen')->store('tips','public');
        }
        Tips::insert($datostips);
        //return response()->json($datostips);
        return redirect('home/tips')->with('mensaje','Se agrego un nuevo tip');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tips  $tips
     * @return \Illuminate\Http\Response
     */
    public function show(Tips $tips)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tips  $tips
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $tips=Tips::findOrFail($id);
        return view('registros.tips.edit',compact('tips'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tips  $tips
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $campos=[
            'titulo'=>'required|string|max:100',
            'descripcion'=>'required|string|max:80000',
            'links'=>'required|string|max:200',

        ];

        $mensaje=[
            'required'=>'El :attribute es requerido',
            'descripcion.required'=>'La descripcion es requerida',

        ];

        if($request->hasFile('imagen')){
            $campos=[
                'imagen'=>'required|max:1000000000|mimes:jpeg,png,jpg,gif',
            ];
            $mensaje=[
                'imagen.required'=>'La imagen es requerida'
            ];

        }

        $this->validate($request,$campos,$mensaje);


        $datotips= request()->except(['_token','_method']);
        if($request->hasFile('imagen')){
            $tips=Tips::findOrFail($id);
            Storage::delete('public/'.$tips->imagen);

            $datotips['imagen']=$request->file('imagen')->store('tips','public');
        }

        
        Tips::where('id','=',$id)->update($datotips);
        $tips=Tips::findOrFail($id);

        //return view('registros.tips.edit',compact('tips'));
        return redirect('home/tips')->with('mensaje','El tip seleccionado fue editado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tips  $tips
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $tips=Tips::findOrFail($id);
        if(Storage::delete('public/'.$tips->imagen)){
            Tips::destroy($id);
        }
        return redirect('home/tips')->with('mensaje','Se elimino un tip');
    }
}
