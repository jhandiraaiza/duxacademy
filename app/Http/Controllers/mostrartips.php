<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tips;

class mostrartips extends Controller
{
    //
    public function index()
    {
        //
        
        $datostips['tips']=Tips::all();
        $tip=Tips::paginate(1);


        return view('vistas.tips.tip',compact('tip'),$datostips);
    }
}
