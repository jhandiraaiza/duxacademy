<?php

namespace App\Http\Controllers;

use App\Models\Biblioteca;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class BibliotecaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $datosbiblioteca['bibliotecas']=Biblioteca::paginate(15);
        return view('registros.biblioteca.index', $datosbiblioteca);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('registros.biblioteca.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //titulo	autor	editorial	edicion	categoria	imagen	archivo
        $campos=[
            'titulo'=>'required|string|max:200',
            'autor'=>'required|string|max:200',
            'editorial'=>'required|string|max:200',
            'edicion'=>'required|string|max:200',
            'categoria'=>'required|string|max:200',
            'sinopsis'=>'required|string|max:8000000000',
            'imagen'=>'required|max:1000000000|mimes:jpeg,png,jpg,gif',
            'archivo'=>'required|max:1000000000|mimes:pdf',
        ];

        $mensaje=[
            'required'=>'El :attribute es requerido',
            'archivo.required'=>'El archivo es requerido',
            'editorial.required'=>'La editorial es requerida',
            'edicion.required'=>'La edicion es requerida',
            'categoria.required'=>'La categoria es requerida',
            'imagen.required'=>'La imagen es requerida',

        ];

        $this->validate($request,$campos,$mensaje);

        $datobiblioteca= request()->except('_token');

        if($request->hasFile('imagen')){

            $datobiblioteca['imagen']=$request->file('imagen')->store('imabilioteca','public');
        }
        if($request->hasFile('archivo')){

            $datobiblioteca['archivo']=$request->file('archivo')->store('libbilioteca','public');
        }
        Biblioteca::insert($datobiblioteca);
        //return response()->json($datocurso);
        return redirect('home/biblioteca')->with('mensaje','Se agrego un libro en biblioteca');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Biblioteca  $biblioteca
     * @return \Illuminate\Http\Response
     */
    public function show(Biblioteca $biblioteca)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Biblioteca  $biblioteca
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $biblioteca=Biblioteca::findOrFail($id);
        return view('registros.biblioteca.edit',compact('biblioteca'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Biblioteca  $biblioteca
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $campos=[
            'titulo'=>'required|string|max:200',
            'autor'=>'required|string|max:200',
            'editorial'=>'required|string|max:200',
            'edicion'=>'required|string|max:200',
            'categoria'=>'required|string|max:200',
            

        ];

        $mensaje=[
            'required'=>'El :attribute es requerido',
            'archivo.required'=>'El archivo es requerido',
            'editorial.required'=>'La editorial es requerida',
            'edicion.required'=>'La edicion es requerida',
            'categoria.required'=>'La categoria es requerida',
            
       
        ];
        if($request->hasFile('imagen')){

            $campos=[

                'imagen'=>'required|max:1000000000|mimes:jpeg,png,jpg,gif',
            ];
    
            $mensaje=[
  
                'imagen.required'=>'la imagen es requerida',
            ];

        }
        if($request->hasFile('archivo')){
            $campos=[

                'archivo'=>'required|max:1000000000|mimes:pdf',
            ];
    
            $mensaje=[
  
                'archivo.required'=>'El archivo es requerido',
            ];

        }
        $this->validate($request,$campos,$mensaje);



        $datobiblioteca= request()->except(['_token','_method']);
        if($request->hasFile('imagen')){
            $biblioteca=Biblioteca::findOrFail($id);
            Storage::delete('public/'.$biblioteca->imagen);
            $datobiblioteca['imagen']=$request->file('imagen')->store('imabiblioteca','public');
        }

        if($request->hasFile('archivo')){
            $biblioteca=Biblioteca::findOrFail($id);
            Storage::delete('public/'.$biblioteca->archivo);
            $datobiblioteca['archivo']=$request->file('archivo')->store('libbilioteca','public');
        }

        
        Biblioteca::where('id','=',$id)->update($datobiblioteca);
        $biblioteca=Biblioteca::findOrFail($id);

        //return view('registros.curso.edit',compact('curso'));
        return redirect('home/biblioteca')->with('mensaje','Se edito el libro ');
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Biblioteca  $biblioteca
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $biblioteca=Biblioteca::findOrFail($id);
        if(Storage::delete('public/'.$biblioteca->imagen)){
            if(Storage::delete('public/'.$biblioteca->archivo)){
                Biblioteca::destroy($id);
            }
           
        }

        return redirect('home/biblioteca')->with('mensaje','Se elimino el libro');
    }
}
