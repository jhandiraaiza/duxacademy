<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\personal;
use App\Models\rol;
use Illuminate\Support\Facades\DB;
use App\Models\Premios;

class mostrarperson extends Controller
{
    //
    public function index()
    {
        //
        $datospersonal['personal']=personal::paginate(5);
        $datospremio['premios']=Premios::paginate(5);
        $rolg['rol_registrado']= rol::all();
        return view('vistas.sobrenosotros.sn',$datospersonal)->with($datospremio);
    }
    public function show($id)
        {
            //
            $persona=personal::findOrFail($id);
            $rolgv['rol_registrado']= rol::all();

            $clients = DB::table('personals')->where('personals.id','=',$id)->get()->pluck('rol_id')->first();
            $rolg= rol::findOrFail($clients);
            //return response()->json($rolg);
            return view('vistas.sobrenosotros.showsn',compact('rolg','persona'), $rolgv);
        }
}
    
