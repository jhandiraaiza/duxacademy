<?php

namespace App\Http\Controllers;

use App\Models\Premios;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PremiosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $datospremio['premios']=Premios::paginate(20);
        
        return view('registros.premios.index', $datospremio);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('registros.premios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //		


        $campos=[
            'nombre'=>'required|string|max:80000',
            'fecha'=>'required|string|max:200',
            'institucion'=>'required|string|max:80000',
            'descripcion'=>'required|string|max:80000',
            'pais'=>'required|string|max:80000',
            'imagen'=>'required|max:1000000000|mimes:jpeg,png,jpg,gif',
            
            
        ];

        $mensaje=[
            'required'=>'La :attribute es requerida',
            'imagen.required'=>'La imagen es requerida',
        ];

        $this->validate($request,$campos,$mensaje);



        $datopremio= request()->except('_token');

        if($request->hasFile('imagen')){

            $datopremio['imagen']=$request->file('imagen')->store('premios','public');
        }
        Premios::insert($datopremio);
        return redirect('home/premios')->with('mensaje','Se agrego un nuevo reconocimiento');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Premios  $premios
     * @return \Illuminate\Http\Response
     */
    public function show(Premios $premios)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Premios  $premios
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $premio=Premios::findOrFail($id);
        return view('registros.premios.edit',compact('premio'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Premios  $premios
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $campos=[
            'nombre'=>'required|string|max:80000',
            'fecha'=>'required|string|max:200',
            'institucion'=>'required|string|max:80000',
            'descripcion'=>'required|string|max:80000',
            'pais'=>'required|string|max:80000',

        ];

        $mensaje=[
            'required'=>'La :attribute es requerida',
            
       
        ];
        if($request->hasFile('imagen')){

            $campos=[

                'imagen'=>'required|max:1000000000|mimes:jpeg,png,jpg,gif',
            ];
    
            $mensaje=[
  
                'imagen.required'=>'la imagen es requerida',
            ];

        }

        $this->validate($request,$campos,$mensaje);


        $datopremio= request()->except(['_token','_method']);
        if($request->hasFile('imagen')){
            $premio=Premios::findOrFail($id);
            Storage::delete('public/'.$premio->imagen);

            $datopremio['imagen']=$request->file('imagen')->store('premios','public');
        }

        
        Premios::where('id','=',$id)->update($datopremio);
        $premio=Premios::findOrFail($id);

        return redirect('home/premios')->with('mensaje','Se edito exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Premios  $premios
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $premio=Premios::findOrFail($id);
        if(Storage::delete('public/'.$premio->imagen)){
            Premios::destroy($id);
        }
        return redirect('home/premios')->with('mensaje','Se elimino la noticia exitosamente');
    }
}
