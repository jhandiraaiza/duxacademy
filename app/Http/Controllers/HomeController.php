<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\personal;
use App\Models\Biblioteca;
use App\Models\Noticias;
use App\Models\cursos;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $datospersonal['personal']=personal::all();
        $datosbiblioteca['biblioteca']=Biblioteca::all();
        $datosnoticias['noticia']=Noticias::all();
        $datoscursos['curso']=cursos::all();
        $datosuser['usuarios']=User::paginate(15);
        return view('home', $datosnoticias,$datosbiblioteca)->with($datospersonal)->with($datoscursos)->with($datosuser);
    }

    public function create()
    {
        //
        return view('registros.usuario.create');
    }

    public function store(Request $request)
    {
        //
        $campos=[
            'name'=>'required|string|max:200',
            'email'=>'required|string|max:80000',
            'password'=>'required|string|max:200',
            'rol_asignado'=>'required|string|max:200',
        ];

        $mensaje=[
            'required'=>'El :attribute es requerido',
        ];

        $this->validate($request,$campos,$mensaje);

        User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
            'rol_asignado' => $request['rol_asignado'],
        ]);
        //return response()->json($datocurso);
        return redirect('home')->with('mensaje','Se agrego un nuevo usuario');
    }
    public function edit($id)
    {
        //
        $usuario=User::findOrFail($id);
        return view('registros.usuario.edit',compact('usuario'));
    }

    public function update(Request $request,$id)
    {
        //
        $campos=[
            'name'=>'required|string|max:200',
            'email'=>'required|string|max:80000',
            'password'=>'required|string|max:200',
            'rol_asignado'=>'required|string|max:200',

        ];

        $mensaje=[
            'required'=>'El :attribute es requerido',
       
        ];

        $this->validate($request,$campos,$mensaje);

        //$datousuario= request()->except(['_token','_method']);

        User::where('id','=',$id)->update(
            [
                'name' => $request['name'],
                'email' => $request['email'],
                'password' => Hash::make($request['password']),
                'rol_asignado' => $request['rol_asignado'],
            ]
        );
        $usuario=User::findOrFail($id);

        //return view('registros.curso.edit',compact('curso'));
        return redirect('home')->with('mensaje','Se edito el usuario exitosamente');
    }
    public function destroy($id)
    {
        //
        $usuario=User::findOrFail($id);
        User::destroy($id);
    
        return redirect('home')->with('mensaje','Se elimino al usuario exitosamente');
    }
}
