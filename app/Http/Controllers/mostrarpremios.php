<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Premios;

class mostrarpremios extends Controller
{
    //
    public function index()
    {
        //
        $datospremio['premios']=Premios::all();
        
        return view('vistas.premios.premios', $datospremio);
    }
    public function show($id)
    {
        $premio=Premios::findOrFail($id);
        return view('vistas.premios.show',compact('premio'));
    }
}
