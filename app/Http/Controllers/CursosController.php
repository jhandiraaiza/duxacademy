<?php

namespace App\Http\Controllers;

use App\Models\cursos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CursosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $datoscurso['cursos']=cursos::paginate(15);
        return view('registros.curso.index', $datoscurso);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('registros.curso.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //	





        $campos=[
            'nombrecurso'=>'required|string|max:200',
            'descripcion'=>'required|string|max:80000',
            'categoria'=>'required|string|max:200',
            'tipo'=>'required|string|max:200',
            'institucion'=>'required|string|max:200',
            'logo_ins'=>'required|max:1000000000|mimes:jpeg,png,jpg,gif',
            'instructor'=>'required|string|max:200',
            'foto'=>'required|max:1000000000|mimes:jpeg,png,jpg,gif',
            'Modalidad'=>'required|string|max:200',
            'fecha'=>'required|string|max:200',
            'publico'=>'required|string|max:200',
            'certificado'=>'required|string|max:200',
            'temario'=>'required|max:1000000000|mimes:pdf',
            'Precio'=>'required|string|max:800000000',
            'imagen'=>'required|max:1000000000|mimes:jpeg,png,jpg,gif',
        ];

        $mensaje=[
            'required'=>'El :attribute es requerido',
            'nombrecurso.required'=>'El nombre del curso es requerido',
            'descripcion.required'=>'La descripcion es requerida',
            'categoria.required'=>'La categoria es requerida',
            'logo_ins.required'=>'El logo institucional es requerida',
            'foto.required'=>'La foto es requerida',
            'Modalidad.required'=>'La Modalidad es requerida',
            'empresa.required'=>'La empresa es requerida',
            'temario.required'=>'El temario es requerida',
            'imagen.required'=>'La imagen es requerida',
        ];

        $this->validate($request,$campos,$mensaje);

        $datocurso= request()->except('_token');

        if($request->hasFile('logo_ins') and $request->hasFile('foto') and $request->hasFile('temario') and $request->hasFile('imagen')){

            $datocurso['logo_ins']=$request->file('logo_ins')->store('logos','public');
            $datocurso['foto']=$request->file('foto')->store('fotos','public');
            $datocurso['temario']=$request->file('temario')->store('temarios','public');
            $datocurso['imagen']=$request->file('imagen')->store('cursos','public');
        }
        cursos::insert($datocurso);
        //return response()->json($datocurso);
        return redirect('home/cursos')->with('mensaje','Se agrego un nuevo curso');
    }
    

    /**
     * Display the specified resource.logo_ins	foto	imagen			
     *
     * @param  \App\Models\cursos  $cursos
     * @return \Illuminate\Http\Response
     */
    public function show(cursos $cursos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\cursos  $cursos
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $curso=cursos::findOrFail($id);
        return view('registros.curso.edit',compact('curso'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\cursos  $cursos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validacion
        $campos=[
            'nombrecurso'=>'required|string|max:200',
            'descripcion'=>'required|string|max:80000',
            'categoria'=>'required|string|max:200',
            'tipo'=>'required|string|max:200',
            'institucion'=>'required|string|max:200',
            'instructor'=>'required|string|max:200',
            'Modalidad'=>'required|string|max:200',
            'fecha'=>'required|string|max:200',
            'publico'=>'required|string|max:200',
            'certificado'=>'required|string|max:200',
            'Precio'=>'required|string|max:800000000',
            //'Precio'=>'numeric|required|string|max:800000000',

        ];

        $mensaje=[
            'required'=>'El :attribute es requerido',
            'nombrecurso.required'=>'El nombre del curso es requerido',
            'descripcion.required'=>'La descripcion es requerida',
            'categoria.required'=>'La categoria es requerida',
            'Modalidad.required'=>'La Modalidad es requerida',
            'empresa.required'=>'La empresa es requerida',
            
       
        ];
        if($request->hasFile('logo_ins') and $request->hasFile('foto') and $request->hasFile('temario') and $request->hasFile('imagen')){

            $campos=[
                'logo_ins'=>'required|max:1000000000|mimes:jpeg,png,jpg,gif',
                'foto'=>'required|max:1000000000|mimes:jpeg,png,jpg,gif',
                'temario'=>'required|max:1000000000|mimes:pdf',
                'imagen'=>'required|max:1000000000|mimes:jpeg,png,jpg,gif',
            ];
    
            $mensaje=[
                'logo_ins.required'=>'El logo institucional es requerida',
                'foto.required'=>'La foto es requerida',
                'temario.required'=>'El temario es requerida',
                'imagen.required'=>'La image es requerida',
            ];

        }

        $this->validate($request,$campos,$mensaje);



        $datocurso= request()->except(['_token','_method']);
        if($request->hasFile('logo_ins')){
            $curso=cursos::findOrFail($id);
            Storage::delete('public/'.$curso->logo_ins);
            $datocurso['logo_ins']=$request->file('logo_ins')->store('logos','public');
        }

        if($request->hasFile('foto')){
            $curso=cursos::findOrFail($id);
            Storage::delete('public/'.$curso->foto);
            $datocurso['foto']=$request->file('foto')->store('fotos','public');
        }

        if($request->hasFile('temario')){
            $curso=cursos::findOrFail($id);
            Storage::delete('public/'.$curso->temario);
            $datocurso['temario']=$request->file('temario')->store('temarios','public');
        }

        if($request->hasFile('imagen')){
            $curso=cursos::findOrFail($id);
            Storage::delete('public/'.$curso->imagen);
            $datocurso['imagen']=$request->file('imagen')->store('cursos','public');
        }

        
        cursos::where('id','=',$id)->update($datocurso);
        $curso=cursos::findOrFail($id);

        //return view('registros.curso.edit',compact('curso'));
        return redirect('home/cursos')->with('mensaje','Se edito el curso exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\cursos  $cursos
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $curso=cursos::findOrFail($id);
        if(Storage::delete('public/'.$curso->logo_ins) and Storage::delete('public/'.$curso->foto) and Storage::delete('public/'.$curso->temario) and Storage::delete('public/'.$curso->imagen)){
            cursos::destroy($id);
        }
        return redirect('home/cursos')->with('mensaje','Se elimino el curso exitosamente');
    }
}
