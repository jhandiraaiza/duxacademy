<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\personal;
use App\Models\rol;


class ControlPasante extends Controller
{
    //
    public function login()
    {
        //
        return view('pasante.ventana');
    }

    public function verificar(Request $request)
    {
        //
        $datoperson= request()->except('_token');
        $login = $datoperson['login'];
        $password = $datoperson['password'];

        $val=personal::where('correo','=',$login)->where('ci','=',$password)->first();

        if($val == null)
        {
            return redirect('control')->with('mensaje','Datos inexistentes vuelva a intentarlo');
        }
        else{
            //return redirect('control')->with('mensaje','Datos inexistentes');
            //return response()->json($password);
            $pas=personal::findOrFail($val->id);
            return view('pasante.control',compact('pas'));
            //return response()->json($val->id);

        }

        //if($request->hasFile('imagen')){alejandrovera769@gmail.com

          //  $datonoticia['imagen']=$request->file('imagen')->store('noticia','public');
        //}
        //Noticias::insert($datonoticia);
        //return response()->json($val->id);
       // return redirect('home/noticias')->with('mensaje','Se agrego un nueva noticia');
    }
    public function marcarentrada(Request $request)
    {
        $datocontrol= request()->except('_token');

        $id = $datocontrol['id'];
        $fechai = $datocontrol['fecha'];

        personal::where('id','=',$id)->update(["inicio" => $fechai,"estado"=>"Activo",]);

       
    
        //return response()->json($id)link_imagenes;actividades
        $pas=personal::findOrFail($id);
        return view('pasante.control',compact('pas'));
    }
    public function marcarsalida(Request $request)
    {
        $datocontrol= request()->except('_token');

        $id = $datocontrol['id'];
        $fechas = $datocontrol['fecha'];
        $proyecto = $datocontrol['proyecto'];
        $imagenlink = $datocontrol['link_imagenes'];
        $actividad = $datocontrol['actividades'];

        personal::where('id','=',$id)->update(["fin" => $fechas,
        "estado"=>"Desactivar",
        "proyecto" => $proyecto,
        "link_imagenes" => $imagenlink,
        "actividades" => $actividad,
    ]);

    
        $pas=personal::findOrFail($id);
        return view('pasante.control',compact('pas'));
    }

    public function MostrarPasante()
    {
        //
        $datospersonal['personal']=personal::paginate(20);
        $rolg['rol_registrado']= rol::all();
        
        return view('registros.pasante.pasante', $datospersonal,$rolg);
    }
    public function generarpdf()
    
    {
    
        $datospersonal['personal']=personal::all();
        

        $pdf = \PDF::loadView('pasante.hojac',$datospersonal)/*->setPaper( [0, 0, 141.732,783,965])*/;



        return $pdf->stream();
        //return response()->json($nombre);
    }
}

