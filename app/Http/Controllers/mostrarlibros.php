<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Biblioteca;

class mostrarlibros extends Controller
{
    //
    public function index()
    {
        //
        $datoslibros['libros']=Biblioteca::all();
        $libros['l']=Biblioteca::paginate(15);
        return view('vistas.biblioteca.libros',$datoslibros,$libros);
    }
    public function show($id)
    {
        $libros=Biblioteca::findOrFail($id);
        return view('vistas.biblioteca.librosshow',compact('libros'));
    }
}
