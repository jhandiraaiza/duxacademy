<?php

namespace App\Http\Controllers;

use App\Models\personal;
use App\Models\rol;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class PersonalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $datospersonal['personal']=personal::paginate(20);
        $rolg['rol_registrado']= rol::all();
        
        return view('registros.personal.index', $datospersonal,$rolg);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $rolgv['rol_registrado']= rol::all();
        $rolg='';
        return view('registros.personal.create',compact('rolg'),$rolgv);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //									foto	rol_id	
        $campos=[
            'nombre'=>'required|string|max:200',
            'apellidos'=>'required|string|max:200',
            'ci'=>'required|string|max:200',
            'fecha_nac'=>'required|string|max:200',
            'area'=>'required|string|max:200',
            'ciudad'=>'required|string|max:200',
            'pais'=>'required|string|max:200',
            'celular'=>'required|string|max:200',
            'correo'=>'required|string|max:200',
            'descripcion'=>'required|string|max:80000',
            'rol_id'=>'required|string|max:200',
            'foto'=>'required|max:1000000000|mimes:jpeg,png,jpg,gif',
        ];

        $mensaje=[
            'required'=>'El :attribute es requerido',
            'apellidos.required'=>'Los apellidos son requeridos',
            'ci.required'=>'La cedula es requerida',
            'fecha_nac.required'=>'La fecha de nacimiento requerida',
            'ciudad.required'=>'La ciudad es requerida',
            'descripcion.required'=>'La descripcion es requerida',
            'foto.required'=>'La foto es requerida',
        ];

        $this->validate($request,$campos,$mensaje);

        $datorol= request()->except('_token');

        if($request->hasFile('foto')){

            $datorol['foto']=$request->file('foto')->store('uploads','public');
        }
        personal::insert($datorol);
        //return response()->json($datorol);
        return redirect('home/personal')->with('mensaje','Se agrego una persona');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\personal  $personal
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\personal  $personal
     * @return \Illuminate\Http\Response
     */
    public function edit( $id)
    {
        $persona=personal::findOrFail($id);
        $rolgv['rol_registrado']= rol::all();

        $clients = DB::table('personals')->where('personals.id','=',$id)->get()->pluck('rol_id')->first();
        $rolg= rol::findOrFail($clients);
        //return response()->json($rolg);
        return view('registros.personal.edit',compact('rolg','persona'), $rolgv);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\personal  $personal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $campos=[
            'nombre'=>'required|string|max:200',
            'apellidos'=>'required|string|max:200',
            'ci'=>'required|string|max:200',
            'fecha_nac'=>'required|string|max:200',
            'area'=>'required|string|max:200',
            'ciudad'=>'required|string|max:200',
            'pais'=>'required|string|max:200',
            'celular'=>'required|string|max:200',
            'correo'=>'required|string|max:200',
            'descripcion'=>'required|string|max:80000',
            'rol_id'=>'required|string|max:200',
        ];

        $mensaje=[
            'required'=>'El :attribute es requerido',
            'apellidos.required'=>'Los apellidos son requeridos',
            'ci.required'=>'La cedula es requerida',
            'fecha_nac.required'=>'La fecha de nacimiento requerida',
            'ciudad.required'=>'La ciudad es requerida',
            'descripcion.required'=>'La descripcion es requerida',
        ];

        if($request->hasFile('foto')){

            $campos=[

                'foto'=>'required|max:1000000000|mimes:jpeg,png,jpg,gif',

            ];
    
            $mensaje=[
  
                'foto.required'=>'la foto es requerida',

            ];

        }

        $this->validate($request,$campos,$mensaje);

        $datorol= request()->except(['_token','_method']);
        if($request->hasFile('foto')){
            $persona=personal::findOrFail($id);
            Storage::delete('public/'.$persona->foto);

            $datorol['foto']=$request->file('foto')->store('uploads','public');
        }

        
        personal::where('id','=',$id)->update($datorol);

        $persona=personal::findOrFail($id);
        $rolgv['rol_registrado']= rol::all();
        
        $clients = DB::table('personals')->where('personals.id','=',$id)->get()->pluck('rol_id')->first();
        $rolg= rol::findOrFail($clients);
        //return response()->json($rolg);
        //return view('registros.personal.edit',compact('rolg','persona'), $rolgv);
        return redirect('home/personal')->with('mensaje','Se edito a la persona');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\personal  $personal
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $persona=personal::findOrFail($id);

        if(Storage::delete('public/'.$persona->foto)){
            personal::destroy($id);
        }
        


        
        return redirect('home/personal')->with('mensaje','Se elimino a la persona');

    }
}
