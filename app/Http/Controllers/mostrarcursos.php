<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\cursos;

class mostrarcursos extends Controller
{
    //
    public function index()
    {
        //
        $datoscurso['curso']=cursos::all();
        return view('vistas.curso.cursos', $datoscurso);
    }
    public function show($id)
    {
        $cursos=cursos::findOrFail($id);
        return view('vistas.curso.show',compact('cursos'));
    }
    
}
