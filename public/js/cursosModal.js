$(document).ready(function() {
  var current = 1,
    current_step, next_step, steps;
  steps = $("fieldset").length;
  $(".next").click(function(e) {
    let first_input;
    let second_input;
    switch (current) {
      case 1:
        first_input = document.getElementById('email');
        second_input = document.getElementById('password');
        break;
      case 2:
        first_input = document.getElementById('fName');
        second_input = document.getElementById('lName');
        break;
      case 3:
        first_input = document.getElementById('mob');
        second_input = document.getElementById('address');
      default:
        break;
    }
    if(first_input.validity.valid && second_input.validity.valid) {
      current_step = $(this).parent();
      next_step = $(this).parent().next();
      next_step.show();
      current_step.hide();
      setProgressBar(++current);
    }
  });

  $(".previous").click(function() {
    current_step = $(this).parent();
    next_step = $(this).parent().prev();
    next_step.show();
    current_step.hide();
    setProgressBar(--current);
  });


  $('.submit').click(function(e) {
    const formulario = document.querySelector('form');
    e.preventDefault();
    if(formulario.reportValidity()) {
      formulario.requestSubmit();
    }
  });

  setProgressBar(current);
  // Change progress bar action
  function setProgressBar(curStep) {
    var percent = parseFloat(100 / steps) * curStep;
    percent = percent.toFixed();
    $(".progress-bar")
      .css("width", percent + "%")
      .html(percent + "%");
  }
});