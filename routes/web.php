<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RolController;
use App\Http\Controllers\PersonalController;
use App\Http\Controllers\PremiosController;
use App\Http\Controllers\CursosController;
use App\Http\Controllers\MostrarController;
use App\Http\Controllers\TipsController;
use App\Http\Controllers\NoticiasController;
use App\Http\Controllers\BibliotecaController;
use App\Http\Controllers\mostrarperson;
use App\Http\Controllers\mostrarcurso;
use App\Http\Controllers\mostrarnoticia;
use App\Http\Controllers\mostrarlibros;
use App\Http\Controllers\mostrarcursos;
use App\Http\Controllers\mostrartips;
use App\Http\Controllers\mostrarpremios;
use App\Http\Controllers\User;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\InscripcionController;
use App\Http\Controllers\ComunidadController;
use App\Http\Controllers\TestimoniosController;
use App\Http\Controllers\ControlPasante;
use App\Models\inscripcion;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/home/rol', function () {
    return view('registros.roles.index');
});
if($user->esdios())
    {
        echo "eres usuario dios ";

    }else{
        echo "eres otra persona";
}


Route::get('/home/rol/create',[RolController::class,'create']);*/

//autenticacion de rutas para el funcionamiento 


/*Route::get('/home', function () {
    $user = Auth::user();
    if($user->esadmin())
    {
        echo "eres usuario admin ";

    }else{
        echo "eres otra persona";
}
});*/




Route::resource('/',MostrarController::class);


//routas de la pagina de inicio 
Route::resource('/sobrenosotros',mostrarperson::class);
Route::resource('/noticias',mostrarnoticia::class);
Route::resource('/biblioteca',mostrarlibros::class);
Route::resource('/cursos',mostrarcursos::class);
Route::resource('/tips',mostrartips::class);
Route::resource('/premios', mostrarpremios::class);
Route::resource('/inscripcion', InscripcionController::class);
Route::get('/control',[ControlPasante::class,'login']);
Route::get('/control/verificar',[ControlPasante::class,'verificar']);
Route::get('/control/marcarentrada',[ControlPasante::class,'marcarentrada']);
Route::get('/control/marcarsalida',[ControlPasante::class,'marcarsalida']);
//Route::get('/control/pdf',[ControlPasante::class,'imprimir']);

//roles rutas

//$dato = Auth::user()->id; Route::get('/home/portada/imprimir/{id}',[PortadaController::class,'imprimir']);
Route::get('/home/pasante',[ControlPasante::class,'MostrarPasante'])->middleware('auth');
Route::get('/home/pasante/pdf',[ControlPasante::class,'generarpdf'])->middleware('auth');
    
Route::resource('/home/rol',RolController::class)->middleware('auth');
Route::resource('/home/personal',PersonalController::class)->middleware('auth');

Route::resource('/home/premios',PremiosController::class)->middleware('auth');
Route::resource('/home/cursos',CursosController::class)->middleware('auth');

Route::resource('/home/noticias',NoticiasController::class)->middleware('auth');


Route::resource('/home/tips',TipsController::class)->middleware('auth');

Route::resource('/home/biblioteca',BibliotecaController::class)->middleware('auth');

Route::resource('/home/inscripcion', InscripcionController::class)->middleware('auth');

Route::resource('/home/comunidad', ComunidadController::class)->middleware('auth');

Route::resource('/home/testimonio', TestimoniosController::class)->middleware('auth');

Route::get('/home/enviar/{id}',[InscripcionController::class,'enviar'])->middleware('auth');

   
Route::resource('/home',HomeController::class)->middleware('auth');



//inscripciones




//routa de login y pasword 

Auth::routes(['register'=>false,'reset'=>false]);


