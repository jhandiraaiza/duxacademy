
@include('layouts.mainv',['biblioteca'=>'active'])
<html>
    <head>
        <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
        <link rel="stylesheet" href="{{ asset('css/styleTest.css') }}">
        <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

    </head>
    <script>
	AOS.init({
  duration: 1200,
})
</script>
    <body>
        <section class="biblioteca-encabezado" data-aos="fade-down">
            <div class="row rowCarrusel" style="display: flex;align-items: center;">
                <div class="col-xs-12 col-sm-5" >
                    <div class="box">
                        <div class="contenido-seccion">
                            <p class="titulo-biblioteca">BIBLIOTECA</p>
                            <div class="linea"></div>
                            <p class="descripcion-biblioteca">Biblioteca pública digital DUX, con mas de 100 libros gratuitos en diversas categorias, para coadyuvar los procesos de enseñanza, aprendizaje, 
                                investigación y otras formas del saber humano.</P>
                            
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-7">
                    <div class="box" >
                        <!-- Swiper -->
                        <div class="swiper mySwiper">
                        
                            <div class="swiper-wrapper">
                            @foreach($l as $l)
                            <div class="swiper-slide"><img src="{{asset('storage').'/'.$l->imagen}}" alt=""></div>  
                            @endforeach    
                            </div>
                        
                           <!-- <div class="swiper-wrapper">
                            <div class="swiper-slide"><img src="img/lib7.jpg" alt=""></div>
                            <div class="swiper-slide"><img src="img/lib8.jpg" alt=""></div>
                            <div class="swiper-slide"><img src="img/lib9.jpg" alt=""></div>
                            <div class="swiper-slide"><img src="img/lib6.jpg" alt=""></div>
                            <div class="swiper-slide"><img src="img/lib5.jpg" alt=""></div>  
                            <div class="swiper-slide"><img src="" alt=""></div> --> 
                            </div>
                            <div class="swiper-pagination"></div>
                        </div>

                        <!-- Swiper JS -->
                        <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>

                        <!-- Initialize Swiper -->
                        <script>
                            var swiper = new Swiper(".mySwiper", {
                            effect: "coverflow",
                            grabCursor: true,
                            centeredSlides: true,
                            slidesPerView: "auto",
                            coverflowEffect: {
                                rotate: 20,
                                stretch: 0,
                                depth: 200,
                                modifier: 1,
                                slideShadows: true,
                            },
                            loop:true,
                                autoplay:{
                                    delay:1200,
                                    disableOnInteraction:false,
                                },
                            });
                        </script>
                    </div>
                </div>
            </div>
        </section>

        <section class="listas">
            
            <div class="container-listas">
                
                <p class="titulosCategLibrosDer">Desarrollo personal<div class="lineaDer"></div></p>
                <div class="row" style="margin:5% 0 5% 0">
                    <div class="col-md-3 colLib">
                        <div class="card-listas">
                            <div class="img-box">
                                <img src="https://images.cdn3.buscalibre.com/fit-in/360x360/46/6b/466b0b47e932561b186c56358acbe55e.jpg" width="100%">
                            </div>
                            <div class="details">
                                <h2 class="tituloSinopsis">
                                    SINOPSIS
                                </h2>
                                <p class="descSinopsis">
                                    Orgullo y prejuicio es la obra maestra de la escritora inglesa Jane Austen, cuyo trasfondo es la vida de la burguesía inglesa de comienzos de siglo XIX.
                                    La novela muestra cómo las relaciones motivadas por el amor y el dinero pueden ser promiscuas y mezquinas, encubiertas por el velo de la sociedad burguesa.
                                </p>
                            </div>
                            <div style="padding: 5% 0 5% 0">
                                <h2 class="tituloLibro">Orgullo y prejuicio</h2>
                                <p class="autor">Jane Austen</p>
                                <a href="#" class="btnLibros"><i class="fas fa-file-download"></i>Descargar</a>
                            </div>
                            
                        </div>
                    </div>
                    <div class="col-md-3 colLib">
                        <div class="card-listas">
                            <div class="img-box">
                                <img src="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1420395129l/20419450.jpg" width="100%">
                            </div>
                            <div class="details">
                                <h2 class="tituloSinopsis">
                                    SINOPSIS
                                </h2>
                                <p class="descSinopsis">
                                    Orgullo y prejuicio es la obra maestra de la escritora inglesa Jane Austen, cuyo trasfondo es la vida de la burguesía inglesa de comienzos de siglo XIX.
                                    La novela muestra cómo las relaciones motivadas por el amor y el dinero pueden ser promiscuas y mezquinas, encubiertas por el velo de la sociedad burguesa.
                                </p>
                            </div>
                            <div style="padding: 5% 0 5% 0">
                                <h2 class="tituloLibro">Orgullo y prejuicio</h2>
                                <p class="autor">Jane Austen</p>
                                <a href="#" class="btnLibros"><i class="fas fa-file-download"></i>Descargar</a>
                            </div>
                        </div>
                    </div>
                @foreach($libros as $libro)
                    <div class="col-md-3 colLib">
                        <div class="card-listas">
                            <div class="img-box">
                                <img src="{{asset('storage').'/'.$libro->imagen}}" width="100%">
                            </div>
                            <div class="details">
                                <h2 class="tituloSinopsis">
                                    SINOPSIS
                                </h2>
                                <p class="descSinopsis">
                                {{$libro->sinopsis}}.
                                </p>
                            </div>
                            <div style="padding: 5% 0 5% 0">
                                <h2 class="tituloLibro">{{$libro->titulo}}</h2>
                                <p class="autor">{{$libro->autor}}</p>
                                <a href="{{asset('storage').'/'.$libro->archivo}}" class="btnLibros" download="documento.pdf"><i class="fas fa-file-download"></i>Descargar</a>
                            </div>
                        </div>
                    </div>
                
                @endforeach
                    
                    <div class="col-md-3 colLib">
                        <div class="card-listas">
                            <div class="img-box">
                                <img src="https://images.cdn3.buscalibre.com/fit-in/360x360/46/6b/466b0b47e932561b186c56358acbe55e.jpg" width="100%">
                            </div>
                            <div class="details">
                                <h2 class="tituloSinopsis">
                                    SINOPSIS
                                </h2>
                                <p class="descSinopsis">
                                    Orgullo y prejuicio es la obra maestra de la escritora inglesa Jane Austen, cuyo trasfondo es la vida de la burguesía inglesa de comienzos de siglo XIX.
                                    La novela muestra cómo las relaciones motivadas por el amor y el dinero pueden ser promiscuas y mezquinas, encubiertas por el velo de la sociedad burguesa.
                                </p>
                            </div>
                            <div style="padding: 5% 0 5% 0">
                                <h2 class="tituloLibro">Orgullo y prejuicio</h2>
                                <p class="autor">Jane Austen</p>
                                <a href="#" class="btnLibros"><i class="fas fa-file-download"></i>Descargar</a>
                            </div>
                        </div>
                    </div>
                </div>

                <p class="titulosCategLibrosIzq">La Informática, el internet y sus redes sociales<div class="lineaIzq"></div></p>
                
                <div class="row" style="margin:5% 0 5% 0">
                    <div class="col-md-3 colLib">
                        <div class="card-listas">
                            <div class="img-box">
                                <img src="https://images.cdn3.buscalibre.com/fit-in/360x360/46/6b/466b0b47e932561b186c56358acbe55e.jpg" width="100%">
                            </div>
                            <div class="details">
                                <h2 class="tituloSinopsis">
                                    SINOPSIS
                                </h2>
                                <p class="descSinopsis">
                                    Orgullo y prejuicio es la obra maestra de la escritora inglesa Jane Austen, cuyo trasfondo es la vida de la burguesía inglesa de comienzos de siglo XIX.
                                    La novela muestra cómo las relaciones motivadas por el amor y el dinero pueden ser promiscuas y mezquinas, encubiertas por el velo de la sociedad burguesa.
                                </p>
                            </div>
                            <div style="padding: 5% 0 5% 0">
                                <h2 class="tituloLibro">Orgullo y prejuicio</h2>
                                <p class="autor">Jane Austen</p>
                                <a href="#" class="btnLibros"><i class="fas fa-file-download"></i>Descargar</a>
                            </div>
                            
                        </div>
                    </div>
                    <div class="col-md-3 colLib">
                        <div class="card-listas">
                            <div class="img-box">
                                <img src="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1420395129l/20419450.jpg" width="100%">
                            </div>
                            <div class="details">
                                <h2 class="tituloSinopsis">
                                    SINOPSIS
                                </h2>
                                <p class="descSinopsis">
                                    Orgullo y prejuicio es la obra maestra de la escritora inglesa Jane Austen, cuyo trasfondo es la vida de la burguesía inglesa de comienzos de siglo XIX.
                                    La novela muestra cómo las relaciones motivadas por el amor y el dinero pueden ser promiscuas y mezquinas, encubiertas por el velo de la sociedad burguesa.
                                </p>
                            </div>
                            <div style="padding: 5% 0 5% 0">
                                <h2 class="tituloLibro">Orgullo y prejuicio</h2>
                                <p class="autor">Jane Austen</p>
                                <a href="#" class="btnLibros"><i class="fas fa-file-download"></i>Descargar</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 colLib">
                        <div class="card-listas">
                            <div class="img-box">
                                <img src="https://www.aboutespanol.com/thmb/HORNUW8kOEo3BJyUKuBG5qdphZ0=/768x0/filters:no_upscale():max_bytes(150000):strip_icc()/ciudades-de-papel-nube-de-tinta-56a5a5515f9b58b7d0ddd015.JPG" width="100%">
                            </div>
                            <div class="details">
                                <h2 class="tituloSinopsis">
                                    SINOPSIS
                                </h2>
                                <p class="descSinopsis">
                                    Orgullo y prejuicio es la obra maestra de la escritora inglesa Jane Austen, cuyo trasfondo es la vida de la burguesía inglesa de comienzos de siglo XIX.
                                    La novela muestra cómo las relaciones motivadas por el amor y el dinero pueden ser promiscuas y mezquinas, encubiertas por el velo de la sociedad burguesa.
                                </p>
                            </div>
                            <div style="padding: 5% 0 5% 0">
                                <h2 class="tituloLibro">Orgullo y prejuicio</h2>
                                <p class="autor">Jane Austen</p>
                                <a href="#" class="btnLibros"><i class="fas fa-file-download"></i>Descargar</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 colLib">
                        <div class="card-listas">
                            <div class="img-box">
                                <img src="https://images.cdn3.buscalibre.com/fit-in/360x360/46/6b/466b0b47e932561b186c56358acbe55e.jpg" width="100%">
                            </div>
                            <div class="details">
                                <h2 class="tituloSinopsis">
                                    SINOPSIS
                                </h2>
                                <p class="descSinopsis">
                                    Orgullo y prejuicio es la obra maestra de la escritora inglesa Jane Austen, cuyo trasfondo es la vida de la burguesía inglesa de comienzos de siglo XIX.
                                    La novela muestra cómo las relaciones motivadas por el amor y el dinero pueden ser promiscuas y mezquinas, encubiertas por el velo de la sociedad burguesa.
                                </p>
                            </div>
                            <div style="padding: 5% 0 5% 0">
                                <h2 class="tituloLibro">Orgullo y prejuicio</h2>
                                <p class="autor">Jane Austen</p>
                                <a href="#" class="btnLibros"><i class="fas fa-file-download"></i>Descargar</a>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            
        </section>
        <div class="bibliotecaFooter" data-aos="fade-up">
                <p class="textoBibliotecaFooter">¿Tuviste problemas con la descarga?</p>
                <p class="textoBibliotecaFooter2">Comunícate con nostros.</p>
            </div>
        
    </body>
</html>
@include('layouts.footer')
<img src="{{ asset('img\figuras\trianguloBordeBlanco.png') }}" alt="" class="trianguloBordeBlanco">
<img src="{{ asset('img\figuras\trianguloBordeAmarillo.png') }}" alt="" class="trianguloBordeAmarillo">
<img src="{{ asset('img\figuras\trianguloBordeGray.png') }}" alt="" class="trianguloBordePlomo">
<img src="{{ asset('img\figuras\puntosWhite.png') }}" alt="" class="puntosBlanco">



