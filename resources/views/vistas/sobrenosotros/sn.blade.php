@include('layouts.mainv',['nosotros'=>'active'])

<script>
	AOS.init({
  duration: 1200,
})
</script>
<link href="{{asset('css/styleNosotros.css')}}" rel="stylesheet" type="text/css">
<section class="cabeceraNosotros">
  <div class="row">
    <div class="col-md-5 colUno" >
      <p class="tituloNosotros"></p>
    </div>
    <div class="col-md-3 colDos">
      <div class="descNosotros">
        <!--<img src="img/fondoCursos.jpeg" alt="" class="imgNosotrosDiv" style="width:45rem;margin-left:-60%">-->
        <div class="imgNosotros">

        </div>
      </div>
    </div>
    <div class="col-md-4 colTres " >
      <div class="descNosotrosDux" data-aos="fade-left">
       DUX<br><span class="cambioColor">AC</span>ADEMY
        <p class="significado"> El significado del nombre de la institución proviene del término utilizado 
        en latín «DUX» que significa <strong>líder</strong>
          y «ACADEMY» proviene del latín que significa <strong>academia</strong>.</p>
      </div>
      
    </div>
  </div> 
</section>





<section class="misionVision" >
  <div class="mision" data-aos="fade-left">
    <p class="titulo"> MISIÓN</p>
    <p class="descripcion">
      Desarrollar las habilidades de cada joven alcanzando su <br>
        máximo potencial y desarrollar un liderazgo integral en <br>
        su respectivo entorno.
    </p>
  </div>
      
  <div class="vision" data-aos="fade-right">
    <p class="titulo">VISIÓN</p>
    <p class="descripcion">
      Ser una academia de formación de jóvenes líderes con <br>
        calidad humana e intelectual reconocida a nivel internacional, <br>
        desarrollando la red más grande, diversa y de excelencia en <br>
        oportunidades académica y extracurriculares
    </p>
  </div>

</section>


<section class="logros" id="logros" style="" data-aos="fade-up">
  <p class="tituloSeccionLogros">NUESTROS LOGROS</p>
</section> 

<section class="logros2" data-aos="fade-up">
<div class="rowInicio">
    
      @foreach($premios as $p)
      <div class="cardLogro">
        <p class="logroIcono">
          <span class="fa-stack fa-2x">
            <i class="fas fa-circle fa-stack-2x iconoCirculo"></i>
            <i class="fas fa-trophy fa-stack-1x fa-inverse iconoLogro"></i>
          </span>
        </p>
				<p class="tituloLogro">{{$p->titulo}}</p>
        <p class="instLogro"><span class="rellenoTitulo"> {{$p->institucion}}, {{$p->pais}}</span></p>
        <p class="fechaLogro">{{$p->fecha}}</p>
        <p class="descLogro">{{$p->descripcion}}</p>
      </div>
      @endforeach
      
      <!--<div class="cardLogro">
        <p class="logroIcono">
          <span class="fa-stack fa-2x">
            <i class="fas fa-circle fa-stack-2x iconoCirculo"></i>
            <i class="fas fa-award  fa-stack-1x fa-inverse iconoLogro"></i>
          </span>
          
        </p>
        <p class="tituloLogro">RECONOCIMIENTO</p>
        <p class="instLogro"><span class="rellenoTitulo">UPSA, Bolivia</span></p>
        <p class="fechaLogro">Abril 2021</p>
        <p class="descLogro">Reconocimiento otorgado a Vanesa Michelle Ortega por el proyecto DUX ACADEMY.</p>
      </div>
      <div class="cardLogro">
        <p class="logroIcono">
          <span class="fa-stack fa-2x">
            <i class="fas fa-circle fa-stack-2x iconoCirculo"></i>
            <i class="fas fa-trophy fa-stack-1x fa-inverse iconoLogro"></i>
          </span>
        </p>
				<p class="tituloLogro">#NOSOTRASINNOVAMOS</p>
        <p class="instLogro"><span class="rellenoTitulo">Mujeres TIC, Bolivia</span></p>
        <p class="fechaLogro">Octubre 2021</p>
        <p class="descLogro">Reconocimiento otorgado a Vanesa Michelle Ortega por el proyecto DUX ACADEMY.</p>
      </div>
      <div class="cardLogro">
        <p class="logroIcono">
          <span class="fa-stack fa-2x">
            <i class="fas fa-circle fa-stack-2x iconoCirculo"></i>
            <i class="fas fa-award  fa-stack-1x fa-inverse iconoLogro"></i>
          </span>
        </p>
				<p class="tituloLogro">PROYECTO SOLIDARIO</p>
        <p class="instLogro"><span class="rellenoTitulo">FCEE-UAGRM, Bolivia</span></p>
        <p class="fechaLogro">Octubre 2021</p>
        <p class="descLogro">Reconocimiento otorgado A DUX ACADEMY.</p>
      </div>-->
    </div>
</section>

<section class="equipo" id="equipo">
<p style="font-size: 8rem;font-weight: bold;color: var(--plomoTrans);text-align:right; padding-right:8%"><strong>EQUIPO DUX</strong></p>


  <div class="rowPersonTeam" >
 
    @foreach($personal as $person)
    <div class="colPersona">
      <div class="imgBoxEquipo">
          <div class="contenidoImgEquipo">
            <img src="{{asset('storage').'/'.$person->foto}}" width="100%" class="imgPersonaEquipo">
           <div class="overlayimgPersonaEquipo">
              <div class="textoFrase">{{$person->descripcion}}</div>
            </div>
          </div>
          
          <p class="nombrePersona" >{{$person->nombre}}&nbsp;{{$person->apellidos}}</p>
      </div>
      <div class="cargoPersona">{{$person->rol->nombre_rol}}</div>
    </div>
    @endforeach
    
    <div class="colPersona2">
      <div class="imgBoxEquipo">
          <div class="contenidoImgEquipo">
            <img src="img/team/imgTeam8.jfif" width="100%" class="imgPersonaEquipo">
            <div class="overlayimgPersonaEquipo">
              <div class="textoFrase">Cree en ti y en lo que puedas lograr</div>
            </div>
          </div>
          
          <p>John Green</p>
      </div>
      <div class="cargoPersona">CEO/FOUNDER</div>
    </div>
    <div class="colPersona2">
      <div class="imgBoxEquipo">
          <div class="contenidoImgEquipo">
            <img src="img/team/imgTeam8.jfif" width="100%" class="imgPersonaEquipo">
            <div class="overlayimgPersonaEquipo">
              <div class="textoFrase">Cree en ti y en lo que puedas lograr</div>
            </div>
          </div>
          
          <p>John Green</p>
      </div>
      <div class="cargoPersona">CEO/FOUNDER</div>
    </div>

    <div class="colPersona2">
      <div class="imgBoxEquipo">
          <div class="contenidoImgEquipo">
            <img src="img/team/imgTeam8.jfif" width="100%" class="imgPersonaEquipo">
            <div class="overlayimgPersonaEquipo">
              <div class="textoFrase">Cree en ti y en lo que puedas lograr</div>
            </div>
          </div>
          
          <p>John Green</p>
      </div>
      <div class="cargoPersona">CEO/FOUNDER</div>
    </div>
    <div class="colPersona3">
      <div class="imgBoxEquipo">
          <div class="contenidoImgEquipo">
            <img src="img/team/imgTeam2.jfif" width="100%" class="imgPersonaEquipo">
            <div class="overlayimgPersonaEquipo">
              <div class="textoFrase">Cree en ti y en lo que puedas lograr</div>
            </div>
          </div>
          
          <p>Jane Austen</p>
      </div>
      <div class="cargoPersona">CEO/FOUNDER</div>
    </div>
    <div class="colPersona4">
      <div class="imgBoxEquipo">
          <div class="contenidoImgEquipo">
            <img src="img/team/imgTeam7.jpg" width="100%" class="imgPersonaEquipo">
            <div class="overlayimgPersonaEquipo">
              <div class="textoFrase">Cree en ti y en lo que puedas lograr</div>
            </div>
          </div>
          
          <p>John Green</p>
      </div>
      <div class="cargoPersona">CEO/FOUNDER</div>
    </div>   
  </div>
  
  <div class="areasContenido" data-aos="zoom-in">
      <p style="font-size: 3.5rem;font-weight: bold;color: var(--turquesaOpacidad);text-align:left; margin:0"><strong>ÁREAS DE</strong></p>
      <div class="areasDux">
        <div class="rowCardAreas">
          <div class="cardAreas">
            
            <p class="contArea">ACADÉMICA</p>
            <p class="contArea">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            
          </div>
          

          <div class="cardAreas">
            <p class="contArea">RELACIONES PÚBLICAS</p> 
            <p class="contArea">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
          </div>
          
          <div class="cardAreas">
            <p class="contArea">RECURSOS HUMANOS</p>
            <p class="contArea">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
          </div>
          
          <div class="cardAreas">
            <p class="contArea">COMUNICACIÓN</p>
            <p class="contArea">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
          </div>
        </div>
      </div>
      <p style="font-size: 3.5rem;font-weight: bold;color: var(--turquesaOpacidad); margin:0;"><strong>DESARROLLO</strong></p>
  </div>


  <div class="rowPersonTeam">
    <div class="colPersona">
      <div class="imgBoxEquipo">
          <div class="contenidoImgEquipo">
            <img src="img/team/imgTeam1.jpg" width="100%" class="imgPersonaEquipo">
           <div class="overlayimgPersonaEquipo">
              <div class="textoFrase">""Cree en ti y en lo que puedas lograr.""</div>
            </div>
          </div>
          
          <p class="nombrePersona" >Jane Austen</p>
      </div>
      <div class="cargoPersona">CEO/FOUNDER</div>
    </div>
    <div class="colPersona2">
      <div class="imgBoxEquipo">
          <div class="contenidoImgEquipo">
            <img src="img/team/imgTeam8.jfif" width="100%" class="imgPersonaEquipo">
            <div class="overlayimgPersonaEquipo">
              <div class="textoFrase">Cree en ti y en lo que puedas lograr</div>
            </div>
          </div>
          
          <p>John Green</p>
      </div>
      <div class="cargoPersona">CEO/FOUNDER</div>
    </div>
    <div class="colPersona3">
      <div class="imgBoxEquipo">
          <div class="contenidoImgEquipo">
            <img src="img/team/imgTeam2.jfif" width="100%" class="imgPersonaEquipo">
            <div class="overlayimgPersonaEquipo">
              <div class="textoFrase">Cree en ti y en lo que puedas lograr</div>
            </div>
          </div>
          
          <p>Jane Austen</p>
      </div>
      <div class="cargoPersona">CEO/FOUNDER</div>
    </div>
    <div class="colPersona4">
      <div class="imgBoxEquipo">
          <div class="contenidoImgEquipo">
            <img src="img/team/imgTeam7.jpg" width="100%" class="imgPersonaEquipo">
            <div class="overlayimgPersonaEquipo">
              <div class="textoFrase">Cree en ti y en lo que puedas lograr</div>
            </div>
          </div>
          
          <p>John Green</p>
      </div>
      <div class="cargoPersona">CEO/FOUNDER</div>
    </div>   
  </div>
</section>


<div class="boxAlianzasInicio" data-aos="zoom-in">
		<p class="titAlianzasInicio"><strong>ALIANZAS</strong></p>
		<p class="descAlianzasInicio">Instituciones que confían en nosotros. </p>
	</div>

<section class="alianzas" >
    <!-- Swiper -->
    <div class="swiper mySwiper">
        <div class="swiper-wrapper" style="display: flex;align-items: center;">
        <div class="swiper-slide"><img class="img-fluid" src="{{asset('img/alianzas/iconoTest.jpg')}}" alt=""></div>
          <div class="swiper-slide"><img class="img-fluid" src="{{asset('img/alianzas/iconoTest2.png')}}" alt=""></div>
          <div class="swiper-slide"><img class="img-fluid" src="{{asset('img/alianzas/iconoTest3.jpg')}}" alt=""></div>
          <div class="swiper-slide"><img class="img-fluid" src="{{asset('img/alianzas/iconoTest4.png')}}" alt=""></div>
          <div class="swiper-slide"><img class="img-fluid" src="{{asset('img/alianzas/iconoTest5.png')}}" alt=""></div>
          <div class="swiper-slide"><img class="img-fluid" src="{{asset('img/alianzas/iconoTest6.png')}}" alt=""></div>
        </div>
        <div class="swiper-pagination"></div>
      </div>
  
      <!-- Swiper JS -->
      <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
  
      <!-- Initialize Swiper -->
      <script>
        var swiper = new Swiper(".mySwiper", {
          slidesPerView: 5,
          spaceBetween: 0,
         
          /*pagination: {
            el: ".swiper-pagination",
            clickable: true,
          },*/
          loop:true,
            autoplay:{
                delay:1000,
                disableOnInteraction:false,
            },
        });

      </script>
</section>
<section class="sumateDux" data-aos="fade-up">
  <div class="row">
    <div class="col-md-4">
      <div class="descIzq">
        <p class="tituloSumDux">SÚMATE A <br> DUX ACADEMY</p>
        <div class="divlineaSumDux"></div>
        <p class="descSumDux">¿Eres un apasionado de tu área y quieres ganar experiencia?</p>
      
        <a href=""><button class="btnSumDux">ENVÍANOS TU CV</button></a>
      </div>
      
    </div>
    <div class="col-md-4 divimgSumDux"><img class="imgSum"src="img/areaAcad.jpg" alt="" ></div>
    <div class="col-md-4 contSumDux" >Si eres un graduado o estudiante universitario que está buscando 
      dominar sus habilidades y ganar experiencia, envíanos tu solicitud al correo eléctrónico y 
      aplica a nuestras oportunidades de pasantía.</div>
    </div>
    
    
  </div>
  
</section>


@include('layouts.footer')
<img src="img\figuras\trianguloBordeBlanco.png" alt="" class="trianguloBordeBlanco">
<img src="img\figuras\cuadradoDoble.png" alt="" class="cuadradoDoble">
<img src="img\figuras\zigZagBlack.png" alt="" class="zigZagNegro">
<img src="img\figuras\puntosWhiteLargo.png" alt="" class="puntosBlancoLargo">
<img src="img\figuras\circulo.png" alt="" class="circuloAmarillo">
<img src="img\figuras\trianguloBordeGray.png" alt="" class="trianguloBordePlomo">
<img src="img\figuras\puntosTurquesaLargo.png" alt="" class="puntosTurquesaLargo">
<!--<img src="img\figuras\circulo.png" alt="" class="circuloAmarillo2">-->
<img src="img\figuras\trianguloBorde.png" alt="" class="trianguloBordeTurquesa">
<img src="img\figuras\puntosAmarillo.png" alt="" class="puntosAmarillo">
<img src="img\figuras\trianguloBordeAmarillo.png" alt="" class="trianguloBordeAmarillo">
<img src="img\figuras\trianguloBorde.png" alt="" class="trianguloBordeTurquesa2">
<img src="img\figuras\circuloBordePqno.png" alt="" class="circuloBordeAmarillo">
<img src="img\figuras\puntosWhiteLargo.png" alt="" class="puntosBlancoLargo2">
<img src="img\figuras\zigZagBlack.png" alt="" class="zigZagNegro">
<img src="img\figuras\trianguloBordeBlanco.png" alt="" class="trianguloBordeBlanco2">

