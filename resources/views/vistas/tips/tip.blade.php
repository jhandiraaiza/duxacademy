@include('layouts.mainv',['tips'=>'active'])


    <script>
  AOS.init({
  duration: 1200,
})
</script>
<link href="{{ asset('css/tips.css') }}" rel="stylesheet" type="text/css"> 
     

    <section class="tipsInicio">
      <div class="row">
        <div class="col blancoTips" >
          <div class="cuadradoTips" data-aos="fade-right"><p class="tituloTipsInicio">5 TIPS PARA ORGANIZAR MEJOR TU TIEMPO</p> </div>
          <div class="cuadradoTips2" data-aos="fade-right">
          @foreach($tip as $tip)
            <p class="nombreTipsInicio">{{$tip->autor}}<</p>
            <p class="descTipsInicio">{{$tip->descripcion}}</p>
            <a target="_blank"  href="{{$tip->links}}" style="text-align:right;"><button class="btnPlayTipsInicio"> EXPLORAR</button></a>
          @endforeach
          </div>
        </div>
        <div class="col tipsImagen"></div>
      </div>
    </section>
    <section class="carrusel-tips" data-aos="fade-up">
      <p class="titulo-tips-otros">OTROS</p>

      <div class="swiper mySwiper">
        <div class="swiper-wrapper">

        @foreach($tips as $t)
          <div class="swiper-slide">
              <a target="_blank" href="{{$t->links}}" class="url-box" >
                <figure class='newsCard news-Slide-up '>
                  <img src="{{asset('storage').'/'.$t->imagen}}"/>
                  <div class='newsCaption px-4'>
                      <div class="d-flex align-items-center justify-content-between cnt-title">
                          <h5 class='newsCaption-title' style=" font-weight: bold; font-size: 1.2rem;">{{$t->titulo}}</h5> <i class="fas fa-play-circle"></i>
                      </div>
                      <div class='newsCaption-content d-flex '>
                          <p >{{$t->descripcion}}</p>
                      </div>
                  </div>
                  <span class="overlay"></span>
                </figure>
              </a>
            </div>
          

          @endforeach
         <!-- <div class="swiper-slide">
            
            
              <figure class='newsCard news-Slide-up '>
                <img src="img/imgPrincipal.jpg"/>
                <div class='newsCaption px-4'>
                    <div class="d-flex align-items-center justify-content-between cnt-title">
                        <h5 class='newsCaption-title' style=" font-weight: bold; font-size: 1.2rem;">5 TIPS PARA ORGANIZAR MEJOR TU TIEMPO</h5> <i class="fas fa-play-circle"></i>
                    </div>
                    
                    <div class='newsCaption-content d-flex '>
                        <p >Hoy te traigo 5 cursos completamente gratis, para aprender 
                            o mejorar tu ingles. Lo mejor de todo es que algunos de los sitios ofrecen CERTIFICACION GRATUITA</p>
                    </div>
                </div>
                <span class="overlay"></span>
              </figure>
            </a>
            
          </div>
          <div class="swiper-slide">
            
            <a href='https://youtu.be/ELsgRAGIgw0' class="url-box" >
              <figure class='newsCard news-Slide-up '>
                <img src="img/imgPrincipal.jpg"/>
                <div class='newsCaption px-4'>
                    <div class="d-flex align-items-center justify-content-between cnt-title">
                        <h5 class='newsCaption-title' style=" font-weight: bold; font-size: 1.2rem;">CURSOS GRATUITOS DE INGLES</h5> <i class="fas fa-play-circle"></i>
                    </div>

                    <div class='newsCaption-content d-flex '>
                        
                        <p >Hoy te traigo 5 cursos completamente gratis, para aprender 
                            o mejorar tu ingles. Lo mejor de todo es que algunos de los sitios ofrecen CERTIFICACION GRATUITA</p>
                    </div>
                </div>
                <span class="overlay"></span>
              </figure>
            </a>
            
          </div>
          <div class="swiper-slide">
            
            <a href='https://youtu.be/ELsgRAGIgw0' class="url-box" >
              <figure class='newsCard news-Slide-up '>
                <img src="img/imgPrincipal.jpg"/>
                <div class='newsCaption px-4'>
                    <div class="d-flex align-items-center justify-content-between cnt-title">
                        <h5 class='newsCaption-title' style=" font-weight: bold; font-size: 1.2rem;">CURSOS GRATUITOS DE INGLES</h5> <i class="fas fa-play-circle"></i>
                    </div>
                    <div class='newsCaption-content d-flex '>
                        <p >Hoy te traigo 5 cursos completamente gratis, para aprender 
                            o mejorar tu ingles. Lo mejor de todo es que algunos de los sitios ofrecen CERTIFICACION GRATUITA</p>
                    </div>
                </div>
                <span class="overlay"></span>
              </figure>
            </a>
           
          </div>
          <div class="swiper-slide">
            
            
              <a href='https://youtu.be/ELsgRAGIgw0' class="url-box" >
                <figure class='newsCard news-Slide-up '>
                  <img src="img/imgPrincipal.jpg"/>
                  <div class='newsCaption px-4'>
                      <div class="d-flex align-items-center justify-content-between cnt-title">
                          <h5 class='newsCaption-title' style=" font-weight: bold; font-size: 1.2rem;">CURSOS GRATUITOS DE INGLES</h5> <i class="fas fa-play-circle"></i>
                      </div>
                      <div class='newsCaption-content d-flex '>
                          <p >Hoy te traigo 5 cursos completamente gratis, para aprender 
                              o mejorar tu ingles. Lo mejor de todo es que algunos de los sitios ofrecen CERTIFICACION GRATUITA</p>
                      </div>
                  </div>
                  <span class="overlay"></span>
                </figure>
              </a>
            
          </div>
          <div class="swiper-slide">Slide 5</div>
          <div class="swiper-slide">Slide 6</div>
          <div class="swiper-slide">Slide 7</div>
          <div class="swiper-slide">Slide 8</div>
          <div class="swiper-slide">Slide 9</div>-->
        </div>
        <div class="swiper-button-next" style="color: #9B9A9A;"></div>
        <div class="swiper-button-prev" style="color: #9B9A9A;"></div>
          
      </div>

      
      <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>

      
      <script>
        var swiper = new Swiper(".mySwiper", {
          slidesPerView: 4,
          spaceBetween: 0,
          navigation: {
              nextEl: ".swiper-button-next",
              prevEl: ".swiper-button-prev",
            },
          
        });
      </script>
    </section>


    @include('layouts.footer')
    <img src="{{ asset('img\figuras\trianguloBordeAmarillo.png') }}" alt="" class="trianguloBordeAmarillo">
    <img src="{{ asset('img\figuras\puntosWhiteLargo.png') }}" alt="" class="puntosBlancoLargo">
    <img src="{{ asset('img\figuras\zigZagBlack.png') }}" alt="" class="zigZagNegro">
    <img src="{{ asset('img\figuras\circuloBorde.png') }}" alt="" class="circuloBordeAmarillo">
    <img src="{{ asset('img\figuras\trianguloBorde.png') }}" alt="" class="trianguloBordeTurquesa">
    <img src="{{ asset('img\figuras\trianguloBordeAmarillo.png') }}" alt="" class="trianguloBordeAmarillo2">
