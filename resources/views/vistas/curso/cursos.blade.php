<!--biblioteca -->

@include('layouts.mainv',['curso'=>'active'])

@if(Session::has('mensaje'))
    <div class="alert alert-success alert-dismissible" role="alert">
    
    {{Session::get('mensaje')}}

    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
        
    </div>
    @endif
<html>
  <head>
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
      <link rel="stylesheet" href="{{asset('css/styleCursos.css')}}">

  </head>
  <script>
	AOS.init({
  duration: 1200,
})
</script>
  <body>
    <section class="cardsCursos">
    <div class="css-filter--links">
      <a href="#tag:all" class="css-filter--link  css-filter--link--reset">Todas las categorias</a>
      <a href="#tag:filter1" class="css-filter--link">Emprendimientos</a>
      <a href="#tag:filter2" class="css-filter--link" >Finanzas</a>
      <a href="#tag:filter3" class="css-filter--link">Informatica</a>
    </div>
    <main data-aos="zoom-in">
      <div class="content-container">
        <a class="css-filter--target" id="tag:filter1"></a>
        <a class="css-filter--target" id="tag:filter2"></a>
        <a class="css-filter--target" id="tag:filter3"></a>
        
       <!-- <div class="css-filter--item" data-tag="filter1">
          <div class="c-card">
            <div class="card">
              <div class="card__banner">
                <div class="background">
                  <div class="c-background">
                    <div class="card__poster">
                      <img src="img/fondoCursoLogo.jpg" alt="">
                    </div>
                    <div class="card_banner_content">
                      <div class="__content">
                        <p class="_content-subtitle">Project Management Institute</p>
                        <h3 class="_content-title">GESTIÓN DE PROYECTOS</h3>
                        <p class="_content-subtitle">Lic. Carlos Cuauhtemoc Sanchez</p>
                        <p class="_content-subtitle"><strong>Bs. 0,00</strong></p>
                        <p class="_content-subtitle" >Inicio: 26 de agosto de 2021</p>
                        <a class="_content-btn" href="cursosDetalles.php">MAS INFORMACIÓN</a>
                      </div>
                    </div>
                  </div>   
                </div>
                <img src="img/fondoCursos.jpeg" alt="" style="width:100%;">
                
              </div>
              
            </div>
          </div>
           Curso Emprendimiento 1
        </div>-->
        @foreach($curso as $c)
        <div class="css-filter--item" data-tag="filter1">
          <div class="c-card">
            <div class="card">
              <div class="card__banner">
                <div class="background">
                  <div class="c-background">
                    <div class="card__poster">
                      <img src="{{asset('img/fondoCursoLogo.jpg')}}" alt="">
                    </div>
                    <div class="card_banner_content">
                      <div class="__content">
                        <p class="_content-subtitle">{{$c->nombrecurso}}</p>
                        <h3 class="_content-title">{{$c->categoria}}</h3>
                        <p class="_content-subtitle">{{$c->instructor}}</p>
                        <p class="_content-subtitle"><strong>Bs. {{$c->Precio}}</strong></p>
                        <p class="_content-subtitle" >Inicio: {{$c->fecha}}</p>
                        <a class="_content-btn" href="{{url('/cursos/'.$c->id)}}">MAS INFORMACIÓN</a>
                      </div>
                    </div>
                  </div>   
                </div>
                <img src="{{asset('img/fondoCursos.jpeg')}}" alt="" style="width:100%;">
                
              </div>
              
            </div>
          </div>
           Curso Emprendimiento 1
        </div>
        @endforeach
        <!--<div class="css-filter--item" data-tag="filter2">
          <div class="c-card">
            <div class="card">
              <div class="card__banner">
                <div class="background">
                  <div class="c-background">
                    <div class="card__poster">
                      <img src="img/fondoCursoLogo.jpg" alt="">
                    </div>
                    <div class="card_banner_content">
                      <div class="__content">
                        <p class="_content-subtitle">Project Management Institute</p>
                        <h3 class="_content-title">GESTIÓN DE PROYECTOS</h3>
                        <p class="_content-subtitle">Lic. Carlos Cuauhtemoc Sanchez</p>
                        <p class="_content-subtitle"><strong>Bs. 0,00</strong></p>
                        <p class="_content-subtitle" >Inicio: 26 de agosto de 2021</p>
                        <a class="_content-btn" href="cursosDetalles.php">MAS INFORMACIÓN</a>
                      </div>
                    </div>
                  </div>   
                </div>
                <img src="img/fondoCursos.jpeg" alt="" style="width:100%;">
                
              </div>
              
            </div>
          </div>
           Curso Emprendimiento 1
        </div>
        <div class="css-filter--item" data-tag="filter2">
        <div class="c-card">
            <div class="card">
              <div class="card__banner">
                <div class="background">
                  <div class="c-background">
                    <div class="card__poster">
                      <img src="img/fondoCursoLogo.jpg" alt="">
                    </div>
                    <div class="card_banner_content">
                      <div class="__content">
                        <p class="_content-subtitle">Project Management Institute</p>
                        <h3 class="_content-title">GESTIÓN DE PROYECTOS</h3>
                        <p class="_content-subtitle">Lic. Carlos Cuauhtemoc Sanchez</p>
                        <p class="_content-subtitle"><strong>Bs. 0,00</strong></p>
                        <p class="_content-subtitle" >Inicio: 26 de agosto de 2021</p>
                        <a class="_content-btn" href="cursosDetalles.php">MAS INFORMACIÓN</a>
                      </div>
                    </div>
                  </div>   
                </div>
                <img src="img/fondoCursos.jpeg" alt="" style="width:100%;">
                
              </div>
              
            </div>
          </div>
          Curso Finanzas 2
        </div>
        <div class="css-filter--item" data-tag="filter1">
        <div class="c-card">
            <div class="card">
              <div class="card__banner">
                <div class="background">
                  <div class="c-background">
                    <div class="card__poster">
                      <img src="img/fondoCursoLogo.jpg" alt="">
                    </div>
                    <div class="card_banner_content">
                      <div class="__content">
                        <p class="_content-subtitle">Project Management Institute</p>
                        <h3 class="_content-title">GESTIÓN DE PROYECTOS</h3>
                        <p class="_content-subtitle">Lic. Carlos Cuauhtemoc Sanchez</p>
                        <p class="_content-subtitle"><strong>Bs. 0,00</strong></p>
                        <p class="_content-subtitle" >Inicio: 26 de agosto de 2021</p>
                        <a class="_content-btn" href="cursosDetalles.php">MAS INFORMACIÓN</a>
                      </div>
                    </div>
                  </div>   
                </div>
                <img src="img/fondoCursos.jpeg" alt="" style="width:100%;">
                
              </div>
              
            </div>
          </div>
          Curso Emprendimiento 2
        </div>
        <div class="css-filter--item" data-tag="filter3">Curso Informatica 1</div>
        <div class="css-filter--item" data-tag="filter3">Curso Informatica 2</div>
        <div class="css-filter--item" data-tag="filter1">Curso Emprendimiento 3</div>
        <div class="css-filter--item" data-tag="filter1">Curso Emprendimiento 4</div>
        <div class="css-filter--item" data-tag="filter2">Curso Finanzas 3</div>
        <div class="css-filter--item" data-tag="filter3">Curso Informatica 3</div>
        <div class="css-filter--item" data-tag="filter3">Curso Informatica 4</div>
        <div class="css-filter--item" data-tag="filter3">Curso Informatica 5</div>-->
      </div> 
    </main>
    </section>
    <section class="trabajamos" data-aos="fade-up">
      <p class="tituloTrabajamos">
        Capacitaciones corporativas
      </p>
    <p class="descTrabajo">También te ofrecemos diferentes planes de capacitación en diversas áreas para que puedas 
          elegir el que mejor se adapte a las necesidades de tu organizacion, colegio o universidad,
          ya sean públicas o privadas.</p>
      <!--<div class="row my-auto">
        <div class="col-md-8">
          <p class="descTrabajo my-auto">En Dux Academy te ofrecemos diferentes planes de capacitación en diversas áreas para que puedas 
          elegir el que mejor se adapte a las necesidades de tu organizacion, institución, colegio o universidad,
          ya sean publicas o privadas.</p>
        </div>
        <div class="col-md-4 my-auto">
            <span class="fa-stack fa-lg iconoTrabajo ">
              <i class="fa fa-circle fa-stack-2x"></i>
              <i class="fa fa-handshake fa-stack-1x fa-inverse"></i>
            </span> 
        </div>
        
      </div>-->
      
    </section> 
  </body>
  <script>
    $(document).ready(function(){
        $("#boton-drop").click(function(){
            $("#boton-drop").toggleClass("drop-rotate");
            $("#drop-descripcion").toggleClass("drop-active")
        })
    })
  </script>
  <script>
    $(document).ready(function(){
        $("#boton-drop2").click(function(){
            $("#boton-drop2").toggleClass("drop-rotate");
            $("#drop-descripcion2").toggleClass("drop-active")
        })
    })
  </script>
</html>


@include('layouts.footer')
<img src="{{asset('img\figuras\trianguloBordeGray.png')}}" alt="" class="trianguloBordePlomo">
<img src="{{asset('img\figuras\puntosTurquesa.png')}}" alt="" class="puntosTurquesa">
<img src="{{asset('img\figuras\circuloBorde.png')}}" alt="" class="circuloBordeAmarillo">
<img src="{{asset('img\figuras\trianguloBordeBlanco.png')}}" alt="" class="trianguloBordeBlanco">

