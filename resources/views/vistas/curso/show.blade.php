

@include('layouts.mainv')
        
        <!--<form action="{{url('/inscripcion')}}" method="post" enctype="multipart/form-data">
        @csrf
            <div class="container">

            <div class="form-group">
            <label for="curso">Curso</label>
            <input type="text" class="form-control" name="curso" id="curso" readonly onmousedown="return false;" value="{{$cursos->nombrecurso}}">
            </div>

            <div class="form-group">
            <label for="nombre">Nombre</label>
            <input type="text" class="form-control" name="nombre" id="nombre" value="">
            </div>

            <div class="form-group">
            <label for="apellidopaterno">Apellido Paterno</label>
            <input type="text" class="form-control" name="apellidopaterno" id="apellidopaterno" value="">
            </div>

            <div class="form-group">	
            <label for="apellidomaterno">Apellido Materno</label>
            <input type="text" class="form-control" name="apellidomaterno" id="apellidomaterno" value="">
            </div>

            <div class="form-group">
            <label for="ci">Cedula</label>
            <input type="text" class="form-control" name="ci" id="ci" value="">
            </div>

            <div class="form-group">
            <label for="correo">Correo</label>
            <input type="text" class="form-control" name="correo" id="correo" value="">
            </div>

            <div class="form-group">
            <label for="num_cuenta">Num. cuenta</label>
            <input type="text" class="form-control" name="num_cuenta" id="num_cuenta" value="">
            </div>

            <div class="form-group">
            <label for="imagen">Foto del baucher</label>
            <input type="file" class="form-control" name="imagen" id="imagen" value="">
            </div>

            <input type="submit" class="btn btn-success" value="Registrarse" id="Enviar">
            <a class="btn btn-primary" href="{{url('/cursos')}}">Regresar</a>
                </div>
        
        </form>-->


<html>
    <head>
        <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
        <link rel="stylesheet" href="{{asset('css/styleCursosDetalles.css')}}">
        
    </head>
    <body>
        <div class="desCursos">
            <div class="row" >
                <div class="col-md-7" style="text-align:center;font-size:2.2rem;padding:5%" >
                    <p style="color:white">WEBINAR DE</p>
                    <p style="color:white"><strong>{{$cursos->nombrecurso}}</strong></p>
                    <div class="lineaDivCursos"></div>
                    <p style="font-size:1.5rem" > <span style="color:white;">Inicio: {{$cursos->fecha}}</span></p>
                    <!--<p style="background-color: #aaaaaa;color:white;font-size:1.1rem;padding:5%">
                    ¿Sabías que las Empresas de Software necesitan cada vez más Testers para garantizar la calidad de sus desarrollos? Un Tester QA (Quality Assurance) 
                    puede formarse de forma rápida, sencilla y sin conocimientos previos. 
                    Descubre por qué convertirte en Tester puede ser una gran oportunidad de entrada al mundo del Desarrollo del Software.
                    </p>-->
                    <div class="row">
                        <div class="col-md-2 iconosTargetCursos" >
                            
                            <i class="fas fa-users "></i>
                        </div>
                        <div class="col-md-4 iconosTarget" >
                            <p><strong>PÚBLICO</strong></p>
                            <p>{{$cursos->publico}} </p>
                        </div>

                        <div class="col-md-2 iconosTargetCursos" >
                            <p><i class="fas fa-award "></i>
                        </div>
                        <div class="col-md-4 iconosTarget" >
                            <p><strong>CERTIFICACIÓN</strong></p>
                            <p>{{$cursos->certificado}}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 iconosTargetCursos" >
                            <p><i class="fas fa-chalkboard-teacher "></i>
                        </div>
                        <div class="col-md-4 iconosTarget" >
                            <p><strong>MODALIDAD</strong> </p>
                            <p>{{$cursos->Modalidad}} </p>
                        </div>
                        <div class="col-md-2 iconosTargetCursos">
                            <p><i class="fas fa-download "></i>
                        </div>
                        <div class="col-md-4 iconosTarget"  >
                            <p><strong>TEMARIO</strong> </p>
                            <a href="{{asset('storage').'/'.$cursos->temario}}" class="btn" download="documento.pdf">Descargar</a>para mayor información.</p>
                        </div>

                    </div>
                    
                    <div class="row " style="font-size:.8rem;text-align:left;padding:5%;background:rgba(99,189,169,0.65);">
                        <div class="col-md-2">
                            <img style="border-radius: 50%;background:white;"class="imgIns img-fluid" src="{{asset('storage').'/'.$cursos->logo_ins}}" width="50" alt="">
                        </div>
                        <div class="col-md-4 my-auto" style="color:white;font-weight:bold;">
                        {{$cursos->institucion}} 
                        </div>
                        <div class="col-md-2 ">
                            <img style="border-radius: 50%;background:white;"class="imgIns img-fluid" src="{{asset('storage').'/'.$cursos->foto}}" width="50" alt="">
                        </div>
                        <div class="col-md-4 my-auto" style="color:white;font-weight:bold;">
                        {{$cursos->instructor}}. 
                        </div>
                    </div>
                    
                </div>
                
                <form class="col-md-5" id="regForm"  action="{{url('/inscripcion')}}" method="post" enctype="multipart/form-data">
                @csrf
                    <h1>FORMULARIO DE INSCRIPCIÓN</h1>
                    
                    <!-- One "tab" for each step in the form: -->
                    <div class="tab">
                        <p>Por favor, introduzca los datos correspondientes para registrar su inscripción.</p>
                        <div class="form-row">
                            <div class="col">
                                <label>Nombre(s):</label>
                                <p><input type="text" placeholder="Jane" oninput="this.className = ''"  name="nombre" id="nombre" required></p>
                            </div> 
                            <div class="col">   
                                <label>Apellido(s):</label>
                                <p><input type="text" placeholder="Austen" oninput="this.className = ''"  name="apellido" id="apellido" required></p>
                            </div>
                        </div>
                        
                        <div class="form-row">
                            <div class="col">
                                <label>Correo electrónico:</label>
                                <p><input type="email" placeholder="ejemplo@mail.com" oninput="this.className = ''"  name="correo" id="correo" required></p>
                            </div> 
                        </div>
                        <div class="form-row">
                            <div class="col"> 
                                <label>Curso:</label>
                                <p><input type="text"  oninput="this.className = ''" name="capacitacion" id="capacitacion" readonly onmousedown="return false;" value="{{$cursos->nombrecurso}}"></p>
                                
                            </div>
                        </div>
                        
                        
                    </div>
                    
                    <div class="tab">Agregar datos personales

                        <div class="form-row">
                            <div class="col">
                                <label>País:</label>
                                <p><input type="text" placeholder="Ingrese país de residencia" oninput="this.className = ''"name="pais" id="pais" required></p>
                            
                            </div> 
                            <div class="col"> 
                                <label>Ciudad:</label>
                                <p><input type="text" placeholder="Ingrese ciudad de residencia" oninput="this.className = ''" name="ciudad" id="ciudad" required></p>
                            </div> 
                        </div> 

                        <div class="form-row">
                            <div class="col">
                                <label>Código de país:</label>
                                <p><input type="number" placeholder="Cod." oninput="this.className = ''" name="cod" id="cod" required></p>
                            </div> 
                            <div class="col"> 
                                <label>Número de celular:</label>
                                <p><input type="tel" placeholder="Celular" oninput="this.className = ''" name="celular" id="celular" required></p>
                            </div> 
                        </div> 
                        
                        <div class="form-row">
                            <div class="col">
                                <label>Género</label>
                                
                                <p><select oninput="this.className = ''"name="genero" id="genero" required>
                                    <option selected>Seleccionar...</option>
                                    <option>Masculino</option>
                                    <option>Femenino</option>
                                    <option>Otro</option>
                                </select></p>
                            </div> 
                            <div class="col"> 
                                <label>Fecha de nacimiento:</label>
                                <p><input type="date" oninput="this.className = ''" name="fec_na" id="fec_na" min="1990-01-01" max="2011-12-30"  required ></p>
                            </div> 
                        </div> 
                        <div class="form-row">
                            <div class="col">
                                <label>Colegio/Universidad:</label>
                                <p><input type="text" placeholder="Ingrese nombre de la institución" oninput="this.className = ''" name="colegio" id="colegio" required></p>
                            </div> 
                            <div class="col"> 
                                <label>Curso/Semestre:</label>
                                <p><select oninput="this.className = ''"name="curso" id="curso" required>
                                    <option selected>Seleccionar...</option>
                                    <option>1RO SECUNDARIA</option>
                                    <option>2DO SECUNDARIA</option>
                                    <option>3RO SECUNDARIA</option>
                                    <option>4TO SECUNDARIA</option>
                                    <option>5TO SECUNDARIA</option> 
                                    <option>6TO SECUNDARIA</option>
                                    <option>1ER SEMESTRE</option>
                                    <option>2DO SEMESTRE</option> 
                                    <option>3ER SEMESTRE</option> 
                                    <option>4TO SEMESTRE</option> 
                                    <option>5TO SEMESTRE</option> 
                                    <option>6TO SEMESTRE</option> 
                                    <option>7MO SEMESTRE</option> 
                                    <option>8VO SEMESTRE</option>                      
                                    <option>9NO SEMESTRE</option> 
                                    <option>10MO SEMESTRE</option>
                                    <option>OTRO</option>
                                </select></p>
                            </div> 
                        </div> 
                        
                        <div class="form-row">
                            <div class="col">
                                <label>Carrera:</label>
                                <p><input type="text" placeholder="Ingrese nombre de la carrera" oninput="this.className = ''" name="carrera" id="carrera" required></p>
                            </div> 
                        </div> 
                        
                        <div class="form-group" >
                            <input type="checkbox" name="permiso" id="permiso" required>
                            <small >Autorizo a Dux Academy a incluir mis datos a su lista de suscritos.</small>
                        </div>   
                    </div>
                    
                    <div class="tab">Contact Info:
                        <p><input placeholder="dd" oninput="this.className = ''" ></p>
                        <div class="form-row">
                            <!--INPUT CODIGO DE DESCUENTO-->
                            <div class="col">
                            
                                <label id="labelform1">¿Tiene código de descuento?</label>
                                <p><input id="texbox" type="text" placeholder="Ingrese código" maxlength="10" oninput="this.className = ''" name="codigoname"></p>
                            </div>
                            <!--FIN INPUT CODIGO DE DESCUENTO-->
                        </div>
                        <div class="form-row">
                            
                                <!--AJAX CODIGO DE DESCUENTO-->
                                <div class="col" align="center" >              
                                    <div id="div-0" style="display:block;" class="regulartable">
                                        <div class="cabPrecio">
                                        <p class="tituloPrecio">COSTO DEL CURSO</p>
                                        <p class="tipoPrecio">REGULAR</p>
                                        </div>
                                        <div class="row text-white ">
                                        <div class="col-6">
                                            <div class="precio" >
                                            <p class="moneda"> US$</p>
                                            <p class="monto"> 14,51</p>
                                            </div>
                                        </div>
                                        
                                        <div class="col-6" style="border-left: 2px solid #aaaaaa;">
                                            <div class="precio" >
                                            <p class="moneda"> Bs</p>
                                            <p class="monto"> 100,00</p>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    
                                    <div id="div-20" style="display:none;" class="regulartable">
                                        <div class="cabPrecio">
                                        <p class="tituloPrecio">COSTO DEL CURSO</p>
                                        <p class="tipoPrecio">-20%</p>
                                        </div>
                                        <div class="row text-white text-center justify-content-center">
                                        <div class="col-6">
                                            <div class="precio" >
                                            <p class="moneda"> US$</p>
                                            <p class="monto"> 11,61</p>
                                            </div>
                                        </div>
                                        
                                        <div class="col-6" style="border-left: 3px solid white;">
                                            <div class="precio" >
                                            <p class="moneda"> Bs</p>
                                            <p class="monto"> 80,00</p>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    <div id="div-50" style="display:none;" class="regulartable">
                                        <div class="cabPrecio">
                                        <p class="tituloPrecio">COSTO DEL CURSO</p>
                                        <p class="tipoPrecio">-50%</p>
                                        </div>
                                        <div class="row text-white text-center justify-content-center">
                                        <div class="col-6">
                                            <div class="precio" >
                                            <p class="moneda"> US$</p>
                                            <p class="monto"> 7,23</p>
                                            </div>
                                        </div>
                                        
                                        <div class="col-6" style="border-left: 3px solid white;">
                                            <div class="precio" >
                                            <p class="moneda"> Bs</p>
                                            <p class="monto"> 50,00</p>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    <div id="div-100" style="display:none;" class="regulartable">
                                        <div class="cabPrecio">
                                        <p class="tituloPrecio">COSTO DEL CURSO</p>
                                        <p class="tipoPrecio">FREE</p>
                                        </div>
                                        <div class="row text-white text-center justify-content-center">
                                        <div class="col-6">
                                            <div class="precio" >
                                            <p class="moneda"> US$</p>
                                            <p class="monto"> 0,00</p>
                                            </div>
                                        </div>
                                        
                                        <div class="col-6" style="border-left: 3px solid white;">
                                            <div class="precio" >
                                            <p class="moneda"> Bs</p>
                                            <p class="monto"> 0,00</p>
                                            </div>
                                        </div>
                                        </div> 
                                    </div>
                                    <div id="errorCodigo" style="display:none;">¡ERROR! CÓDIGO INVALIDO</div>
                                    <script>
                                        
                                        jQuery("input").change(function(){
                                        let texbox = jQuery('#texbox').val();
                                        if ( texbox == '' ) {
                                            jQuery('#div-0').attr("style","display:block;");
                                            jQuery('#div-20').attr("style","display:none;");
                                            jQuery('#div-50').attr("style","display:none;");
                                            jQuery('#div-100').attr("style","display:none;");
                                            jQuery('#errorCodigo').attr("style","display:none;");
                                            jQuery('#msjFile').attr("style","display:none;");
                                                
                                        } else{
                                            
                                            if ( texbox == 'DuX20%wOLf' ) {
                                            jQuery('#div-20').attr("style","display:block;");
                                            jQuery('#div-0').attr("style","display:none;");
                                            jQuery('#div-50').attr("style","display:none;");
                                            jQuery('#div-100').attr("style","display:none;");
                                            jQuery('#errorCodigo').attr("style","display:none;");
                                            jQuery('#msjFile').attr("style","display:none;");
                                            
                                            } else {
                                                
                                            if ( texbox == 'WolF%50dUX' ) {
                                                jQuery('#div-50').attr("style","display:block;");
                                                jQuery('#div-0').attr("style","display:none;");
                                                jQuery('#div-20').attr("style","display:none;");
                                                jQuery('#div-100').attr("style","display:none;");
                                                jQuery('#errorCodigo').attr("style","display:none;");
                                                jQuery('#msjFile').attr("style","display:none;");
                                            }else{
                                                if ( texbox == 'DuxWoLF100' ) {
                                                    jQuery('#div-100').attr("style","display:block;");
                                                    jQuery('#div-0').attr("style","display:none;"); 
                                                    jQuery('#div-20').attr("style","display:none;");
                                                jQuery('#div-50').attr("style","display:none;");
                                                jQuery('#errorCodigo').attr("style","display:none;");
                                                jQuery('#msjFile').attr("style","display:block;");
                                                jQuery('#fileHelp').attr("style","display:none;");
                                                
                                                } else {        
                                                jQuery('#errorCodigo').attr("style","display:block;");
                                                }
                                            }   
                                            }
                                        } 
                                        });
                                    </script> 
                                </div> 
                                <!--FIN AJAX CODIGO DE DESCUENTO-->
                            
                        </div>
                        <div class="form-row">
                            <!--SECCION FORMA DE PAGO-->
                            <div class="col" style="margin: 5% 0 5% 0 ;">
                                <div class="comboSelector">
                                <select name="tipo" id="tipo" onchange="elegir_opcion(this);">
                                    <option value="">Seleccione forma de pago...</option>
                                    <option value="op1">Depósito/transferencia (Solo Bolivia)</option>
                                    <option value="op2">Paypal</option>
                                </select>
                                </div>
                                <div class="opcionesWrapper" style="background: white; border-radius: 30px; padding: 2%; text-align: center;margin-top: 5%;">
                                <!--Aqui irán los campos según la opción elegida-->
                                </div>
                            </div>
                            <div class="opciones">
                                <div id="op1">
                                    
                                    <p style="margin: 5%;color:black;" >
                                        •Entidad financiera: Banco Mercantil Santa Cruz<br>
                                        •Tipo de cuenta: Caja de ahorro - Cuenta corriente<br>•Numero de cuenta: 4070286436<br>•Titular: Vanesa Michelle Ortega Montaño <br>•CI: 6149616 LP</p>
                                    
                                </div>
                                <div id="op2" >
                                
                                    <a class="btn" style="color: black; font-weight: bold;color:  #9492fc !important; background: #f9e4b7 !important;" href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=KE4QV63F6BAN6"><i class="fab fa-paypal"></i>AGAR</a>
                                
                                    
                                </div>
                                
                            </div>
                            <script type="text/javascript">
                                function elegir_opcion(combo) {
                                $tipo = jQuery(combo).val();
                                $campos = jQuery("#"+$tipo).html();
                                jQuery(".opcionesWrapper").html($campos);
                                }
                            </script>
                            <!--FIN SECCION FORMA DE PAGO-->

                        </div>
                    </div>

                    <div class="tab">Birthday:
                        <p><input placeholder="dd" oninput="this.className = ''" name="dd"></p>
                        <!--DATOS DEPOSITO/TRANSFERENCIA-->
                        <p>DATOS DEL DEPÓSITO/TRANSFERENCIA BANCARIA</p>
                        <div class="row"> 
                            
                            <div class="col">
                                <label>Nro. del Depósito/Transferencia Bancaria:</label>
                                <p><input type="number" class="form-control" name="num_cuenta" id="num_cuenta" placeholder="Nro. del Deposito/Transferencia Bancaria" required></p>
                            </div>  
                        </div> 
                        <div class="row"> 
                            <div class="col">   
                                 
                                <label>Monto depositado:</label>
                                <input type="number" min="0" max="500" step="0.01" value="0.00" placeholder="Monto (Bs)" name="monto" id="monto"  required/>
                                
                            </div>
                        </div>
                        <div class="row"> 
                        
                                <p id="fileHelp" class="form-text" align="center">Subir un archivo en formato .JPG o .PNG 
                                del comprobante del deposito/transferencia bancaria
                                (Tamaño máximo 1 MB) </p>

                            
                            <div class="col">
                                
                                <input class="text-white" type="button" id="click-input" value="Buscar"  onclick="document.getElementById('file').click();" />
                            </div>
                            <div class="col">
                                <label for="click-input" id="file-name" class="text-black" >Ningun archivo seleccionado </label>
                                <input type="file" style="display:none;" id="file" name="imagen" required>
                                <script>
                                    inputElement = document.getElementById('file')
                                    labelElement = document.getElementById('file-name')
                                    inputElement.onchange = function(event) {
                                        var path = inputElement.value;
                                        if (path) {
                                            labelElement.innerHTML = path.split(/(\\|\/)/g).pop()
                                        } else {
                                            labelElement.innerHTML = 'Bla bla'
                                        } 
                                    }
                                    
                                </script>
                                
                                
                                
                            </div>
                        </div> 
                    
                        <!--FIN DATOS DEPOSITO/TRANSFERENCIA--> 
                    </div>
                    <div class="tab">
                        <p>Una vez verificada la información, le enviaremos un email con las instrucciones y accesos correspondientes al curso.</p> 
                        <p>NOTA: No olvide revisar su bandeja de spam, correos no deseados o la sección de promociones.</p> 
                        
                        
                    </div>
                    <div style="text-align:center;margin-top:40px;">
                        <span class="step"></span>
                        <span class="step"></span>
                        <span class="step"></span>
                        <span class="step"></span>
                    </div>
                    <div style="overflow:auto;">
                        <div style="float:right;">
                        <button type="submit" id="prevBtn" onclick="nextPrev(-1)">Anterior</button>
                        <button type="submit" id="nextBtn" onclick="nextPrev(1)">Siguiente</button>
                        </div>
                       
                        
                    </div>
                    
                    <!-- Circles which indicates the steps of the form: -->
                    
                    
                </form>
                <script>
                    var currentTab = 0; // Current tab is set to be the first tab (0)
                    showTab(currentTab); // Display the current tab

                    function showTab(n) {
                    // This function will display the specified tab of the form...
                    var x = document.getElementsByClassName("tab");
                    x[n].style.display = "block";
                    //... and fix the Previous/Next buttons:
                    if (n == 0) {
                        document.getElementById("prevBtn").style.display = "none";
                    } else {
                        document.getElementById("prevBtn").style.display = "inline";
                    }
                    if (n == (x.length - 1)) {
                        document.getElementById("nextBtn").innerHTML = "Enviar";
                    } else {
                        document.getElementById("nextBtn").innerHTML = "Siguiente";
                    }
                    //... and run a function that will display the correct step indicator:
                    fixStepIndicator(n)
                    }

                    function nextPrev(n) {
                    // This function will figure out which tab to display
                    var x = document.getElementsByClassName("tab");
                    // Exit the function if any field in the current tab is invalid:
                    if (n == 1 && !validateForm()) return false;
                    // Hide the current tab:
                    x[currentTab].style.display = "none";
                    // Increase or decrease the current tab by 1:
                    currentTab = currentTab + n;
                    // if you have reached the end of the form...
                    if (currentTab >= x.length) {
                        // ... the form gets submitted:
                        document.getElementById("regForm").submit();
                        return false;
                    }
                    // Otherwise, display the correct tab:
                    showTab(currentTab);
                    }

                    function validateForm() {
                    // This function deals with validation of the form fields
                    var x, y, i, valid = true;
                    x = document.getElementsByClassName("tab");
                    y = x[currentTab].getElementsByTagName("input");
                    // A loop that checks every input field in the current tab:
                    for (i = 0; i < y.length; i++) {
                        // If a field is empty...
                        if (y[i].value == "") {
                        // add an "invalid" class to the field:
                        y[i].className += " invalid";
                        // and set the current valid status to false
                        valid = false;
                        }
                    }
                    // If the valid status is true, mark the step as finished and valid:
                    if (valid) {
                        document.getElementsByClassName("step")[currentTab].className += " finish";
                    }
                    return valid; // return the valid status
                    }

                    function fixStepIndicator(n) {
                    // This function removes the "active" class of all steps...
                    var i, x = document.getElementsByClassName("step");
                    for (i = 0; i < x.length; i++) {
                        x[i].className = x[i].className.replace(" active", "");
                    }
                    //... and adds the "active" class on the current step:
                    x[n].className += " active";
                    }
                </script>
            </div>
        </div>
        
        <div class="otrosEventos">
            <div class="row">
                <div class="col-md-6" style="font-size:1.6rem;text-align:left;">
                    <p><strong>Otros eventos</strong> <br>que pueden interesarte</p>
                </div>
                <div class="col-md-6" style="font-size:1.2rem;text-align:right;">
                    <a href="cursos.php"><button class="btnCursosMore">Ver mas</button></a>
                </div>

            </div>
        </div>
        
    </body>
</html>


        
@include('layouts.footer')



  