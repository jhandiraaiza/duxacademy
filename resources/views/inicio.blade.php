
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!- Fonts ->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
       

      
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/tips/tip.css') }}" rel="stylesheet">
        <link
      rel="stylesheet"
      href="https://unpkg.com/swiper/swiper-bundle.min.css"
    />
        <style>
            
            /*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */html{line-height:1.15;-webkit-text-size-adjust:100%}body{margin:0}a{background-color:transparent}[hidden]{display:none}html{font-family:system-ui,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,Noto Sans,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol,Noto Color Emoji;line-height:1.5}*,:after,:before{box-sizing:border-box;border:0 solid #e2e8f0}a{color:inherit;text-decoration:inherit}svg,video{display:block;vertical-align:middle}video{max-width:100%;height:auto}.bg-white{--bg-opacity:1;background-color:#fff;background-color:rgba(255,255,255,var(--bg-opacity))}.bg-gray-100{--bg-opacity:1;background-color:#f7fafc;background-color:rgba(247,250,252,var(--bg-opacity))}.border-gray-200{--border-opacity:1;border-color:#edf2f7;border-color:rgba(237,242,247,var(--border-opacity))}.border-t{border-top-width:1px}.flex{display:flex}.grid{display:grid}.hidden{display:none}.items-center{align-items:center}.justify-center{justify-content:center}.font-semibold{font-weight:600}.h-5{height:1.25rem}.h-8{height:2rem}.h-16{height:4rem}.text-sm{font-size:.875rem}.text-lg{font-size:1.125rem}.leading-7{line-height:1.75rem}.mx-auto{margin-left:auto;margin-right:auto}.ml-1{margin-left:.25rem}.mt-2{margin-top:.5rem}.mr-2{margin-right:.5rem}.ml-2{margin-left:.5rem}.mt-4{margin-top:1rem}.ml-4{margin-left:1rem}.mt-8{margin-top:2rem}.ml-12{margin-left:3rem}.-mt-px{margin-top:-1px}.max-w-6xl{max-width:72rem}.min-h-screen{min-height:100vh}.overflow-hidden{overflow:hidden}.p-6{padding:1.5rem}.py-4{padding-top:1rem;padding-bottom:1rem}.px-6{padding-left:1.5rem;padding-right:1.5rem}.pt-8{padding-top:2rem}.fixed{position:fixed}.relative{position:relative}.top-0{top:0}.right-0{right:0}.shadow{box-shadow:0 1px 3px 0 rgba(0,0,0,.1),0 1px 2px 0 rgba(0,0,0,.06)}.text-center{text-align:center}.text-gray-200{--text-opacity:1;color:#edf2f7;color:rgba(237,242,247,var(--text-opacity))}.text-gray-300{--text-opacity:1;color:#e2e8f0;color:rgba(226,232,240,var(--text-opacity))}.text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}.text-gray-500{--text-opacity:1;color:#a0aec0;color:rgba(160,174,192,var(--text-opacity))}.text-gray-600{--text-opacity:1;color:#718096;color:rgba(113,128,150,var(--text-opacity))}.text-gray-700{--text-opacity:1;color:#4a5568;color:rgba(74,85,104,var(--text-opacity))}.text-gray-900{--text-opacity:1;color:#1a202c;color:rgba(26,32,44,var(--text-opacity))}.underline{text-decoration:underline}.antialiased{-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.w-5{width:1.25rem}.w-8{width:2rem}.w-auto{width:auto}.grid-cols-1{grid-template-columns:repeat(1,minmax(0,1fr))}@media (min-width:640px){.sm\:rounded-lg{border-radius:.5rem}.sm\:block{display:block}.sm\:items-center{align-items:center}.sm\:justify-start{justify-content:flex-start}.sm\:justify-between{justify-content:space-between}.sm\:h-20{height:5rem}.sm\:ml-0{margin-left:0}.sm\:px-6{padding-left:1.5rem;padding-right:1.5rem}.sm\:pt-0{padding-top:0}.sm\:text-left{text-align:left}.sm\:text-right{text-align:right}}@media (min-width:768px){.md\:border-t-0{border-top-width:0}.md\:border-l{border-left-width:1px}.md\:grid-cols-2{grid-template-columns:repeat(2,minmax(0,1fr))}}@media (min-width:1024px){.lg\:px-8{padding-left:2rem;padding-right:2rem}}@media (prefers-color-scheme:dark){.dark\:bg-gray-800{--bg-opacity:1;background-color:#2d3748;background-color:rgba(45,55,72,var(--bg-opacity))}.dark\:bg-gray-900{--bg-opacity:1;background-color:#1a202c;background-color:rgba(26,32,44,var(--bg-opacity))}.dark\:border-gray-700{--border-opacity:1;border-color:#4a5568;border-color:rgba(74,85,104,var(--border-opacity))}.dark\:text-white{--text-opacity:1;color:#fff;color:rgba(255,255,255,var(--text-opacity))}.dark\:text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}}
        </style>


        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>
        <script src="https://code.jquery.com/jquery-3.2.1.js"></script>
<script>
var current = 0;
var imagenes = new Array();

$(document).ready(function() {
    var numImages = 6;
    if (numImages <= 3) {
        $('.right-arrow').css('display', 'none');
        $('.left-arrow').css('display', 'none');
    }
	 
    $('.left-arrow').on('click',function() {
        if (current > 0) {
            current = current - 1;
        } else {
            current = numImages - 3;
        }
        
        $(".carrusel").animate({"left": -($('#product_'+current).position().left)}, 600);
        
        return false;
    });

    $('.left-arrow').on('hover', function() {
        $(this).css('opacity','0.5');
    }, function() {
        $(this).css('opacity','1');
    });

    $('.right-arrow').on('hover', function() {
        $(this).css('opacity','0.5');
    }, function() {
        $(this).css('opacity','1');
    });

    $('.right-arrow').on('click', function() {
        if (numImages > current + 3) {
            current = current+1;
        } else {
            current = 0;
        }
        
        $(".carrusel").animate({"left": -($('#product_'+current).position().left)}, 600);
        
        return false;
    }); 
 });
</script>
    </head>
<body>
<div class="">
            @if (Route::has('login'))
            @extends('layouts.mainv')
            @endif
        </div>
        <br><br><br><br>
    <form >
    
            
        
        

      @foreach($tip as $tip)
      


      <?php
            $string = $tip->links;
            $array = explode('=',$string); // explode string with '@' delimiter
            array_shift($array);           // shift array to remove first element which has no value
            
            $output = [];                  // Output array
            foreach ($array as $string) {  // Loop thru each array as string
                $key = strstr($string, ' ', true);  // take the string before first space
                $value = str_replace($key . ' ', "", $string); // removes the string up to first space
                $output[$key] = $value;    // Set output key and value
            }
        ?>
    <div class="container_mob">
        <section class="container_mobile">
            <div class="box_up">
                <span class="camara"></span>
                <span class="circles"></span>
                <span class="audio_up"></span>
            
            </div>
            <div class="screen">
                <iframe  src="https://www.youtube.com/embed/<?php echo $output[0]?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <div class="buttons_down">
                    <span class="btn_home"></span>
                </div>
            </div>
        </section>
        <div id="parrafo">
            <h1>{{$tip->titulo}}</h1>
            <h2>{{$tip->descripcion}}</h2>
            <a id="boton" href="{{$tip->links}}"  target="_blank"> VER TIP
            </a>
        </div>
        
    </div>
    <div id="imagen_abajo"><img src="{{asset('img/TIPS.png')}}"/></div>

    
       
        
      @endforeach

      
      
      
      
      
    </form>

    <h1 id="otrostips">OTROS TIPS</h1>

    <div class="container">
<!-- Swiper -->
<div class="swiper mySwiper">
      <div class="swiper-wrapper">
      @foreach($tips as $t)
        <div class="swiper-slide"> <img src="{{asset('storage').'/'.$t->imagen}}" width="195" height="100" /> <h5>{{$t->titulo}} <a href="{{$t->links}}"  target="_blank" class="btn btn-success">Ver tip</a> </h5>  </div>
        <!--<div class="swiper-slide">Slide 1</div>
        <div class="swiper-slide">Slide 2</div>
        <div class="swiper-slide">Slide 3</div>
        <div class="swiper-slide">Slide 4</div>
        <div class="swiper-slide">Slide 5</div>
        <div class="swiper-slide">Slide 6</div>
        <div class="swiper-slide">Slide 7</div>
        <div class="swiper-slide">Slide 8</div>
        <div class="swiper-slide">Slide 9</div>-->
        @endforeach
      </div>
      <div class="swiper-pagination"></div>
    </div>

    <!-- Swiper JS -->
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>

    <!-- Initialize Swiper -->
    <script>
      var swiper = new Swiper(".mySwiper", {
        slidesPerView: 3,
        spaceBetween: 30,
        pagination: {
          el: ".swiper-pagination",
          clickable: true,
        },
      });
    </script>

    
</div>





<script src="https://cdnjs.cloudflare.com/ajax/libs/web-animations/2.2.2/web-animations.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/just-animate/1.1.0/just-animate-all.min.js"></script>

<!--<h2 id="score">Click the circles</h2>-->

<div class="bubble-modal">
   <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="bubbleModalTitle">
           Title goes here
         </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        lorem
      </div>

      </div>
    </div>
</div>
<script>
    const $pause = false;
// this drives the number of bubbles occurring
const bubbleImages = [
    '{{asset("img/emogis/like.png")}}',
    '{{asset("img/emogis/encanta.png")}}',
    '{{asset("img/emogis/risa.png")}}',
    '{{asset("img/emogis/sorprendido.png")}}',
    '{{asset("img/emogis/triste.png")}}'
];

// x, y cordinates could be proven useful for animating the modal from this location <img src="/graficos/Image1.png" width=140 height=210 alt="Producto1">
function animateBubbleModal(x, y, bubble) {
   
   const $modal = document.getElementById('bubble-modal');
   $modal.classList.add('active');
   
   // modal background
   const $modalbg = document.createElement('div');
   $modalbg.classList.add('bubble-modal-bg');
   document.body.appendChild($modalbg);
}

function createBubble(imageIndex) {
   // create bubble graphic
   const $bubble = document.createElement('div');
   
   // add class and background image
   $bubble.classList.add('bubble');
   $bubble.style.backgroundImage = 'url(' + bubbleImages[imageIndex] + ')';
   
   // wrap in a larger div so bubbles are easy to pop while moving
   const $boundingBox = document.createElement('div');
   $boundingBox.classList.add('bubble-wrap');
   
   // start at 10vw and max of 90vw
   $boundingBox.style.left = (10 + (Math.random() * 80))  + 'vw';

   // add bubble to bounding box and event listener
   $boundingBox.appendChild($bubble);
   $boundingBox.addEventListener('click', destroyBubble($boundingBox));
   
   // attach to doc and return
   document.body.appendChild($boundingBox);
   return $boundingBox;
}

function createExplosion(x, y) {
    // create explosion at the coordinates
    const $explosion = document.createElement('div');
    $explosion.classList.add('explosion');
    
    // set the cordinates of the "explosion" useful for generating location for graphic
    $explosion.style.left = x + 'px';
    $explosion.style.top = y + 'px';
   
    // set text - again could put anything in here like the start of an animation 
    $explosion.innerHTML = "Clicked";
    
    // add element to dom
    document.body.appendChild($explosion);
   
    // animate cartoon pop on words
    just.animate({
       targets: $explosion,
       to: 600,
       fill: 'forwards',
       easing: 'ease-out',
       css: [
          { scale: 1 },
          { offset: 0.2, scale: 1.4, opacity: 1 },
          { scale: .7, opacity: 0 }
       ]
    })
    .on('finish', () => document.body.removeChild($explosion));
}

function destroyBubble($bubble) {
   return () => {
      // get the bubble bounding box and location
      const rect = $bubble.getBoundingClientRect();
      const centerX = (rect.right - rect.left) * .45 + rect.left;
      const centerY = (rect.bottom - rect.top) * .45 + rect.top;
      
      // add animation for deleting bubble
      createExplosion(centerX, centerY);
      
      // create modal here
      //animateBubbleModal(centerX, centerY, $bubble);

      // remove bubble
      $bubble.style.display = 'none';
   }
}

function generateBubbles(length) {
   const targets = []
   for (let i = 0; i < length; i++) {
      targets.push(createBubble(i));
   }
   return targets;
}

function animateBubbles() {
   const bubbles = generateBubbles(bubbleImages.length);
   const player = just.animate({
      targets: bubbles,
      to: '5s',
      easing: 'ease-in',
      css: {
         transform() {
            const endTranslateY = just.random(100, 110, 'vh', true);
            const startScale = just.random(80, 100, null, true);
            const endScale = just.random(40, 80, null, true);
            
            return [
               'translateY(0) scale(0.' + startScale + ')', 
               'translatey(-' + endTranslateY + ') scale(0.' + endScale + ')'
            ];
         }
      },
      delay() {
         return just.random(0, 10000);
      }
   })
   .on('finish', () => {
      bubbles.forEach(bubble => {
         document.body.removeChild(bubble);
      });
   });
   
   return player;
}

function startBubbles() {
   animateBubbles().on('finish', startBubbles);
}



startBubbles();

</script>

  
        </body>
</html>-->



