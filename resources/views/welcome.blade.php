<!--<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!- Fonts --
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        
        <!- Styles ->
    
        <style>
            /*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */html{line-height:1.15;-webkit-text-size-adjust:100%}body{margin:0}a{background-color:transparent}[hidden]{display:none}html{font-family:system-ui,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,Noto Sans,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol,Noto Color Emoji;line-height:1.5}*,:after,:before{box-sizing:border-box;border:0 solid #e2e8f0}a{color:inherit;text-decoration:inherit}svg,video{display:block;vertical-align:middle}video{max-width:100%;height:auto}.bg-white{--bg-opacity:1;background-color:#fff;background-color:rgba(255,255,255,var(--bg-opacity))}.bg-gray-100{--bg-opacity:1;background-color:#f7fafc;background-color:rgba(247,250,252,var(--bg-opacity))}.border-gray-200{--border-opacity:1;border-color:#edf2f7;border-color:rgba(237,242,247,var(--border-opacity))}.border-t{border-top-width:1px}.flex{display:flex}.grid{display:grid}.hidden{display:none}.items-center{align-items:center}.justify-center{justify-content:center}.font-semibold{font-weight:600}.h-5{height:1.25rem}.h-8{height:2rem}.h-16{height:4rem}.text-sm{font-size:.875rem}.text-lg{font-size:1.125rem}.leading-7{line-height:1.75rem}.mx-auto{margin-left:auto;margin-right:auto}.ml-1{margin-left:.25rem}.mt-2{margin-top:.5rem}.mr-2{margin-right:.5rem}.ml-2{margin-left:.5rem}.mt-4{margin-top:1rem}.ml-4{margin-left:1rem}.mt-8{margin-top:2rem}.ml-12{margin-left:3rem}.-mt-px{margin-top:-1px}.max-w-6xl{max-width:72rem}.min-h-screen{min-height:100vh}.overflow-hidden{overflow:hidden}.p-6{padding:1.5rem}.py-4{padding-top:1rem;padding-bottom:1rem}.px-6{padding-left:1.5rem;padding-right:1.5rem}.pt-8{padding-top:2rem}.fixed{position:fixed}.relative{position:relative}.top-0{top:0}.right-0{right:0}.shadow{box-shadow:0 1px 3px 0 rgba(0,0,0,.1),0 1px 2px 0 rgba(0,0,0,.06)}.text-center{text-align:center}.text-gray-200{--text-opacity:1;color:#edf2f7;color:rgba(237,242,247,var(--text-opacity))}.text-gray-300{--text-opacity:1;color:#e2e8f0;color:rgba(226,232,240,var(--text-opacity))}.text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}.text-gray-500{--text-opacity:1;color:#a0aec0;color:rgba(160,174,192,var(--text-opacity))}.text-gray-600{--text-opacity:1;color:#718096;color:rgba(113,128,150,var(--text-opacity))}.text-gray-700{--text-opacity:1;color:#4a5568;color:rgba(74,85,104,var(--text-opacity))}.text-gray-900{--text-opacity:1;color:#1a202c;color:rgba(26,32,44,var(--text-opacity))}.underline{text-decoration:underline}.antialiased{-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.w-5{width:1.25rem}.w-8{width:2rem}.w-auto{width:auto}.grid-cols-1{grid-template-columns:repeat(1,minmax(0,1fr))}@media (min-width:640px){.sm\:rounded-lg{border-radius:.5rem}.sm\:block{display:block}.sm\:items-center{align-items:center}.sm\:justify-start{justify-content:flex-start}.sm\:justify-between{justify-content:space-between}.sm\:h-20{height:5rem}.sm\:ml-0{margin-left:0}.sm\:px-6{padding-left:1.5rem;padding-right:1.5rem}.sm\:pt-0{padding-top:0}.sm\:text-left{text-align:left}.sm\:text-right{text-align:right}}@media (min-width:768px){.md\:border-t-0{border-top-width:0}.md\:border-l{border-left-width:1px}.md\:grid-cols-2{grid-template-columns:repeat(2,minmax(0,1fr))}}@media (min-width:1024px){.lg\:px-8{padding-left:2rem;padding-right:2rem}}@media (prefers-color-scheme:dark){.dark\:bg-gray-800{--bg-opacity:1;background-color:#2d3748;background-color:rgba(45,55,72,var(--bg-opacity))}.dark\:bg-gray-900{--bg-opacity:1;background-color:#1a202c;background-color:rgba(26,32,44,var(--bg-opacity))}.dark\:border-gray-700{--border-opacity:1;border-color:#4a5568;border-color:rgba(74,85,104,var(--border-opacity))}.dark\:text-white{--text-opacity:1;color:#fff;color:rgba(255,255,255,var(--text-opacity))}.dark\:text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}}
        </style>

        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>
    </head>
    <body class="antialiased">
    <div class="">
            @if (Route::has('login'))
            
            @endif
        </div>
<br><br><br><br><br>
<!- partial:index.partial.html ->

        
    
    </body>
</html>-->
@include('layouts.mainv')

	<script>
	AOS.init({
  duration: 1200,
})
</script>

	<!--SECCION INICIO-->
	<section class="inicioCabecera">
		<div class="row">
			<div class="col" data-aos="fade-down">
				<img src="{{ asset('img/duxLogo1.png') }}" alt="" style="width:70%">
		
		
				<p class="desDux">Somos la red más diversa de <br>
				oportunidades académicas <br>
				para jóvenes.</p>
				<a href="#masDux"><i class="fas fa-sort-down fa-3x iconArrow"></i></a>
			</div>
			<div class="col" style="text-align:center;" >
				<div class="cardInfo" tabindex="0" >
					<span class="card__infoicon" style="">
						<i class="fas fa-info"></i>
					</span>
					
					<p class="card__description"> <span class="resaltado">
						Ofrecemos programas integrales de crecimiento personal y liderazgo, 
						ademas de herramientas y oportunidades.</span></p> 
					<br>
					<a href="nosotros.php" ><button class="btnInicioMore">¿QUIERES SABER MAS?</button></a>
					
					
				</div>
			</div>
		</div>
		
		
				
	</section>
	<!--FIN SECCION INICIO-->

	<!--SECCION MAS DUX-->
	<section id="masDux" class="masDux" style="padding: 8% 20% 0% 20%;margin-top:-5%" data-aos="zoom-in">
		<div class="row" style="text-align:center;font-size:1.1rem; padding: 0 15% 0 15%">
			<p><strong>Buscamos desarrollar y acompañar el proceso de generación de líderes</strong> <br>
			a través de actividades y metodologías de impacto motivacional 
					enfocados en el desarrollo de aptitudes y habilidades que les permita: </p>
		</div>
		<div class="row" style="text-align:center;font-size:.8rem;padding-top: 2%;">
			
			<div class="col-md-4">
				<p><span id="icono" class="fa-stack fa-3x">
					<i class="fa fa-circle fa-stack-2x fa-inverse" style="color: #B8E1D8"></i>
					<i class="fas fa-book-reader fa-stack-1x"></i>
					
				</span></p>
				<p>Experimentar el aprendizaje.</p>
			</div>
			<div class="col-md-4">
				<p><span id="icono" class="fa-stack fa-3x">
					<i class="fa fa-circle fa-stack-2x fa-inverse" style="color: #B8E1D8"></i>
					<i class="fas fa-fist-raised fa-stack-1x"></i>
					
				</span></p>
				<p>Adquirir el carácter.</p>
					
			
			</div>
			<div class="col-md-4">
				<p><span id="icono" class="fa-stack fa-3x">
					<i class="fa fa-circle fa-stack-2x fa-inverse" style="color: #B8E1D8"></i>
					<i class="fas fa-chart-line fa-stack-1x"></i>
				</span></p>
				<p>Empoderar al estudiante entorno al crecimiento personal, social, académico y humano.</p> 
			
			</div>
		</div>
	</section>
	<!--FIN SECCION MAS DUX-->
	
	<!--SECCION NOTICIAS-->
	<section class="noticias" id="noticias" data-aos="slide-up" data-aos-duration="3500">
		<!-- Swiper -->
		<div class="swiper mySwiper">
			<div class="swiper-wrapper">
				@foreach($noticia as $n)
				<div class="swiper-slide">
					<div class="row" style="display: flex;align-items: center;">
						<div class="col-xs-12 col-sm-6">
							<div class="box" style=" padding-top: 15%;padding-bottom: 15%;">
								<div class="content">
									<p class="fecha">{{$n->fecha}}</p>
									<p class="titulo">{{$n->titulo}}</p>
									<P class="descripcion">{{$n->descripcion}}</P>
									<a target="_blank" href="{{$n->link}}" class="btn-noticias">SABER MÁS</a>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6">
							<div class="box">
								<div class="imagen-noticias">
									<img class="img-fluid" src="{{asset('storage').'/'.$n->imagen}}" alt="" >
								</div>
							</div>
						</div>
					</div>
				</div>
				@endforeach
			
			</div>
			<div class="swiper-button-next" style="color:#000011;"></div>
			<div class="swiper-button-prev" style="color:#000011;"></div>
		</div>
	
		<!-- Swiper JS -->
		<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
	
		<!-- Initialize Swiper -->
		<script>
			var swiper = new Swiper(".mySwiper", {
			navigation: {
				nextEl: ".swiper-button-next",
				prevEl: ".swiper-button-prev",
			},
			});
		</script>
		
	</section>
	<!--FIN SECCION NOTICIAS-->
	
	<!--MARTES DE DUX-->
	<section class="comunidad" id="comunidad"style="padding-top: 5%; padding-bottom:5%" data-aos="slide-up">
		<p class="titulo-com1">NUESTRA COMUNIDAD</p>
		<div class="row">
			<div class="col-md-8">
				<p class="titulo-com3">#MartesDeDux</p>
			</div>
			<div class="col-md-4" style="text-align:right;">
				<a href="https://www.facebook.com/duxacademybo"><button class="verMasCom">VER MAS</button></a>
			</div>
		</div>
		
		<div class="divCardPersonCom">
			<div class="row">
				<!--<div class="col cardPersonCom">
					<img src="img/img.png" alt="" class="imgPersonCom">
					<p class="descPersonCom">Es asesor en comunicación, acreditado internacionalmente por la C.I.O. Su propósito es provocar la 
						transformación en un millón de personas y tiene como pasatiempo explorar la grandeza del mundo. 
						Creador de Sánchez Academy.
					</p>
					<div class="row">
						<div class="col-md-9">

						</div>
						<div class="col-md-3 lineaCardPersonCom">
						
						</div>
					</div>
					
					<p class="nombrePersonCom">
						Ulises <br> Cordova </p>
					<a href="https://fb.watch/awSeeDkpNi/" class="iconoPlayCom"><i class="fas fa-play-circle " style="background:white;border-radius:50%"></i></a>
				</div>-->
				@foreach($comunidad as $com)
					<div class="col cardPersonCom">
						<img src="{{asset('storage').'/'.$com->foto}}" alt="" class="imgPersonCom">
						<p class="descPersonCom">{{$com->descripcion}}
						</p>
						<div class="row">
							<div class="col-md-9">

							</div>
							<div class="col-md-3 lineaCardPersonCom">
							
							</div>
						</div>
						
						<p class="nombrePersonCom">
						{{$com->expositor}} <br> {{$com->apel_expositor}} </p>
						<a href="{{$com->link}}" class="iconoPlayCom"><i class="fas fa-play-circle " style="background:white;border-radius:50%"></i></a>
					</div>
				@endforeach
				
			</div>
		</div>
	</section>
	<!--MARTES DE DUX-->
	<!--CURSOS INICIO-->
	<div class="tituloCursosInicio" style="padding-right: 10%;text-align:right">
		<p style="font-size: 3.5rem;font-weight: bold;color: var(--turquesaOpacidad);"><strong>EDUCACIÓN</strong></p>
		<p>Cursos, conferencias, talleres y más, en diversas categorías. </p>
	</div>
	
	<section class="cursosInicio" >
		<div class="divCardPersonCom" data-aos="zoom-in">
			<div class="row cursosGrid">
				<div class="col cuadrosColor">
					<p class="tituloCatCurso">Negocios y Emprendimiento</p> 
					<p  class="desCatCurso">Potencia tu negocio y destaca en el mercado.</p>
					<a href="{{url('/cursos')}}"><button class="btnVer">VER</button></a>
				</div>
				<div class="col cuadrosColor" >
					<p class="tituloCatCurso">Habilidades Blandas</p> 
					<p  class="desCatCurso">Desarrolla tus habilidades blandas. Conviértete en un líder y crea equipos de alto rendimiento.7</p>
					<a href="{{url('/cursos')}}"><button class="btnVer">VER</button></a>
				</div>
				<div class="col cuadrosColor" >
					<p class="tituloCatCurso">Contenido Digital</p> 
					<p  class="desCatCurso">Crea contenidos digitales para hacer crecer tu empresa o tu marca personal.</p>
					<a href="{{url('/cursos')}}"><button class="btnVer">VER</button></a>
				</div>
			</div>
			<div class="row cursosGrid">
				<div class="col cuadrosColor">
					<p class="tituloCatCurso">Negocios y Emprendimiento</p> 
					<p  class="desCatCurso">Potencia tu negocio y destaca en el mercado.</p>
					<a href="{{url('/cursos')}}"><button class="btnVer">VER</button></a>
				</div>
				<div class="col cuadrosColor" >
					<p class="tituloCatCurso">Habilidades Blandas</p> 
					<p  class="desCatCurso">Desarrolla tus habilidades blandas. Conviértete en un líder y crea equipos de alto rendimiento.7</p>
					<a href="{{url('/cursos')}}"><button class="btnVer">VER</button></a>
				</div>
				<div class="col cuadrosColor" >
					<p class="tituloCatCurso">Contenido Digital</p> 
					<p  class="desCatCurso">Crea contenidos digitales para hacer crecer tu empresa o tu marca personal.</p>
					<a href="{{url('/cursos')}}"><button class="btnVer">VER</button></a>
				</div>
			</div>
				
		</div>
	</section>

	<!--FIN CURSOS INICIO-->

	<!--SECCION TIPS-->
	<section class="tips" id="tips" data-aos="slide-up" data-aos-duration="3500">
		<div class="row" style="display: flex;align-items: center;">
			<div class="col-xs-12 col-sm-6">
				<div class="box" >
					<div class="titulo-seccion">
						<p>DUX<br> TIPS &<br> TRICKS</p>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6">
				<div class="box" >
					<div class="contenido-seccion">
						<p class="titulo-tips">CURSOS GRATUITOS CERTIFICADOS</p>
						<div class="linea"></div>
						<p class="descripcion-tips">Aprende habilidades en oficios para obtener un mejor empleo o comenzar tu propio negocio. Los cursos son gratuitos. </P>
						<a href="{{url('/tips')}}" class="btn-tips"> EXPLORAR</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--FIN SECCION TIPS-->

	
	<!--SECCION PREGUNTA FINAL-->
	<section class="preguntas" id="preguntas" data-aos="slide-up" data-aos-duration="3500">
		<div class="descripcion-pregunta">
			<div class="row my-auto" >
				<div class="col-md-7" style="margin-top:12%">
					<p class="tituloBibliotecaInicio">BIBLIOTECA DIGITAL DUX</p>
					<p class="desBibliotecaInicio">Descubre miles de libros gratis...</p>
				</div>
				<div class="col-md-5">
					<div class="composition">
						<a href="{{url('/biblioteca')}}"><img src="{{ asset('img/lib1.jpg') }}" class="composition__photo composition__photo--p1"></a>
						<a href="{{url('/biblioteca')}}"><img src="{{ asset('img/lib5.jpg') }}"  class="composition__photo composition__photo--p2"></a>
						<a href="{{url('/biblioteca')}}"><img src="{{ asset('img/lib6.jpg') }}" class="composition__photo composition__photo--p3"></a>
					</div>
				</div>
				
			</div>
		</div>
	</section>
	<!--FIN SECCION PREGUNTA FINAL-->
	
	

    @include('layouts.footer')

	<img src="img\figuras\circulo.png" alt="" class="circuloAmarillo">
		<img src="{{ asset('img\figuras\puntosWhite1Fila.png') }}" alt="" class="puntosLineaBlanco">
		<img src="{{ asset('img\figuras\puntosWhiteLargo.png') }}" alt="" class="puntosBlancoLargo">
		<img src="{{ asset('iimg\figuras\trianguloBorde.png') }}" alt="" class="trianguloBordeTurquesa">


		<img src="{{ asset('img\figuras\circulo.png') }}" alt="" class="circuloAmarillo2">
		<img src="{{ asset('img\figuras\circuloRellenoBlack.png') }}" alt="" class="circuloRellenoNegro">
		<img src="{{ asset('img\figuras\trianguloBordeGray.png') }}" alt="" class="trianguloBordePlomo">
		<img src="{{ asset('img\figuras\puntosWhite.png') }}" alt="" class="puntosBlanco">

		<img src="{{ asset('img\figuras\circuloBorde.png') }}" alt="" class="circuloBordeAmarillo">
		<img src="{{ asset('img\figuras\puntosTurquesa.png') }}" alt="" class="puntosTurquesa">
		<img src="{{ asset('img\figuras\trianguloBorde.png') }}" alt="" class="trianguloBordeTurquesa2">
		<img src="{{ asset('img\figuras\lineaVerticalYellow.png') }}" alt="" class="lineaVerticalAmarilla">
		<img src="{{ asset('img\figuras\lineaVerticalYellow.png') }}" alt="" class="lineaVerticalAmarilla2">
		<img src="{{ asset('img\figuras\trianguloBorde.png') }}" alt="" class="trianguloBordeTurquesa3">
		<img src="{{ asset('img\figuras\trianguloBordeGray.png') }}" alt="" class="trianguloBordeTurquesa4">

		<img src="{{ asset('img\figuras\circuloBordeBlancoPqno.png') }}" alt="" class="circuloBordeTurquesa">