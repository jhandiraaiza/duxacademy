@extends('layouts.app')

@section('content')
<form action="{{url('home/')}}" method="post" enctype="multipart/form-data">
@csrf
@include('registros.usuario.form',['modo'=>'Crear'])
</form>
@endsection