<h1>{{$modo}} Usuario </h1>   

@if(count($errors)>0)

    <div class="alert alert-danger" role="alert">

    <ul>
    @foreach($errors->all() as $error)
        <li>{{$error}}</li>

    @endforeach
    </ul>
    </div>


@endif

<div class="container">

<div class="form-group">
<label for="name">Nombre</label>
<input type="text" class="form-control" name="name" id="name" value="{{isset($usuario->name)?$usuario->name:old('name')}}">
</div>

<div class="form-group">
<label for="email">Correo:</label>
<input type="email" class="form-control" name="email" id="email" value="{{isset($usuario->email)?$usuario->email:old('email')}}">
</div>

<div class="form-group">
<label for="password">Contraseña:</label>
<input type="password" class="form-control" name="password" id="password" value="{{isset($usuario->password)?$usuario->password:old('password')}}">
</div>

<div class="form-group">
<label for="rol_asignado">Rol:</label>
<input type="text" class="form-control" name="rol_asignado" id="rol_asignado" value="{{isset($usuario->rol_asignado)?$usuario->rol_asignado:old('rol_asignado')}}">
</div>


<input type="submit" class="btn btn-success" value="{{$modo}} Usuario" id="Enviar">
<a class="btn btn-primary" href="{{url('home/')}}">Regresar</a>
    </div>