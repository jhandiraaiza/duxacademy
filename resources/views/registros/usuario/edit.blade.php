@extends('layouts.app')

@section('content')
<form action="{{url('/home/'.$usuario->id)}}" method="post" enctype="multipart/form-data">
@csrf
{{method_field('PATCH')}}
@include('registros.usuario.form',['modo'=>'Editar'])




</form>
@endsection