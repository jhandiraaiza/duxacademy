
@extends('layouts.app')

@section('content')


<div class="container">

    @if(Session::has('mensaje'))
    <div class="alert alert-success alert-dismissible" role="alert">
    
    {{Session::get('mensaje')}}

    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
        
    </div>
    @endif

    <a target="_blank" href="{{url('home/pasante/pdf')}}" class="btn btn-success">Generar informe</a>
<br><br>
<table class="table table-light">
@if(count($personal)>0)
    <thead class="thead-light">
        <tr>
            <th>id</th>
            <th>foto</th>
            <th>Nombre</th>
            <th>Apellido Completo</th>
            <th>Cedula</th>
            <th>Correo</th>
            <th>Area</th>
            <th>Proyecto</th>
            <th>Actividades</th>
            <th>Link imagenes</th>
            <th>Hora entrada</th>
            <th>Hora salida</th>
            <th>Actividad</th>
        </tr>
    </thead>
    <tbody>
    @foreach($personal as $person)

        @if($person->estado != '')
    
            <tr>									 
                <td>{{$person->id}}</td>
                
                <td>
                    <img src="{{asset('storage').'/'.$person->foto}}" width="100" alt="">
                </td>
                <td>{{$person->nombre}}</td>
                <td>{{$person->apellidos}}</td>
                <td>{{$person->ci}}</td>
                <td>{{$person->correo}}</td>
                <td>{{$person->area}}</td>
                <td>{{$person->proyecto}}</td>
                <td>{{$person->actividades}}</td>
                <td>{{$person->link_imagenes}}</td>
                <td>{{$person->inicio}}</td>
                <td>{{$person->fin}}</td>
                <td>
                <a target="_blank" href="{{$person->link_imagenes}}">
                    <input type="button"  class="btn btn-danger" value="Enlace imagenes">
                </a>
                </td>
            </tr>
        @endif
    @endforeach
        
    </tbody>
    @else
<div class="alert alert-dismissable alert-warning">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <h4>Mensaje del sistema!</h4>
  <p>No se encuentran registros.</p>
</div>

    @endif
</table>
{{$personal->links()}}
</div>
@endsection
