


@extends('layouts.app')

@section('content')
<form action="{{url('/home/personal/'.$persona->id)}}" method="post" enctype="multipart/form-data">
@csrf
{{method_field('PATCH')}}
@include('registros.personal.form',['modo'=>'Editar'])



</form>
@endsection
 