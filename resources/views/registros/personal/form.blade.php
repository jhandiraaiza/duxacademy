<h1>{{$modo}} persona </h1>   

@if(count($errors)>0)

    <div class="alert alert-danger" role="alert">

    <ul>
    @foreach($errors->all() as $error)
        <li>{{$error}}</li>

    @endforeach
    </ul>
    </div>


@endif

<div class="container">

    <div class="form-group">
    <label for="nombre">Nombre</label>
    <input type="text" class="form-control" name="nombre" id="nombre" value="{{isset($persona->nombre)?$persona->nombre:old('nombre')}}">
    </div>

    <div class="form-group">
    <label for="apellidos">Apellidos</label>
    <input type="text" class="form-control" name="apellidos" id="apellidos" value="{{isset($persona->apellidos)?$persona->apellidos:old('apellidos')}}">
    </div>

    <div class="form-group">
    <label for="ci">Cedula</label>
    <input type="text" class="form-control" name="ci" id="ci" value="{{isset($persona->ci)?$persona->ci:old('ci')}}">
    </div>

    <div class="form-group">
    <label for="fecha_nac">Fecha de nacimiento</label>
    <input type="date" class="form-control" name="fecha_nac" id="fecha_nac" value="{{isset($persona->fecha_nac)?$persona->fecha_nac:old('fecha_nac')}}">
    </div>

    <div class="form-group">
    <label for="area">Area</label>
    <input type="text" class="form-control" name="area" id="area" value="{{isset($persona->area)?$persona->area:old('area')}}">
    </div>

    <div class="form-group">
    <label for="ciudad">Ciudad</label>
    <input type="text" class="form-control" name="ciudad" id="ciudad" value="{{isset($persona->ciudad)?$persona->ciudad:old('ciudad')}}">
    </div>

    <div class="form-group">
    <label for="pais">Pais</label>
    <input type="text" class="form-control" name="pais" id="pais" value="{{isset($persona->pais)?$persona->pais:old('pais')}}">
    </div>

    <div class="form-group">
    <label for="genero">Genero</label>
    <select name="genero" id="genero" class="form-control" value="{{isset($persona->genero)?$persona->genero:old('genero')}}">
    @if(isset($persona->genero))
        <option value="{{$persona->genero}}">{{$persona->genero}}</option> 
    @endif
        <option value="Masculino">Masculino</option>
        <option value="Femenino">Femenino</option>
        <option value="Otros">Otros</option>
    </select>
    </div>

    <div class="form-group">
    <label for="celular">Celular</label>
    <input type="text" class="form-control" name="celular" id="celular" value="{{isset($persona->celular)?$persona->celular:old('celular')}}">
    </div>

    <div class="form-group">
    <label for="correo">Correo</label>
    <input type="email" class="form-control" name="correo" id="correo" value="{{isset($persona->correo)?$persona->correo:old('correo')}}">
    </div>

    <div class="form-group">
    <label for="descripcion">Lema</label>
    <textarea name="descripcion" class="form-control" id="descripcion" value="">{{isset($persona->descripcion)?$persona->descripcion:old('descripcion')}}</textarea>
    </div>

    <div class="form-group">
    <label for="foto">Foto</label>
    @if(isset($persona->foto))
    <img src="{{asset('storage').'/'.$persona->foto}}" width="100" alt="">
    @endif
    <input type="file" class="form-control" name="foto" id="foto" value="">
    </div>

    <div class="form-group">
    <label for="rol_id">Rol asignado</label>
    <select name="rol_id"  class="form-control">
    @if(isset($persona->rol->nombre_rol))
        <option value="{{$persona->rol_id}}">{{$persona->rol->nombre_rol}}</option> 
    @endif
        @foreach($rol_registrado as $rol)
        <option value="{{$rol->id}}">{{$rol->nombre_rol}}</option>   
        @endforeach
   
    </select>
    </div>
    <input type="submit" class="btn btn-success" value="{{$modo}} personal" id="Enviar">

    <a class="btn btn-primary" href="{{url('home/personal')}}">Regresar</a>
</div>

    
    

   
   
   
    