

@extends('layouts.app')

@section('content')


<div class="container">

    @if(Session::has('mensaje'))
    <div class="alert alert-success alert-dismissible" role="alert">
    
    {{Session::get('mensaje')}}

    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
        
    </div>
    @endif

<a href="{{url('home/personal/create')}}" class="btn btn-success" >Registrar Personal</a>
<br><br>
<table class="table table-light">
@if(count($personal)>0)
    <thead class="thead-light">
        <tr>
            <th>id</th>
            <th>foto</th>
            <th>Nombre</th>
            <th>Apellido Completo</th>
            <th>Cedula</th>
            <th>Fecha nacimiento</th>
            <th>Area</th>
            <th>Ciudad</th>
            <th>Pais</th>
            <th>Genero</th>
            <th>Celular</th>
            <th>Correo</th>
            <th>descripcion</th>
            <th>Rol persona</th>
            <th>Accion</th>
            
        </tr>
    </thead>
    <tbody>
    @foreach($personal as $person)
    
        <tr>									 
            <td>{{$person->id}}</td>
            
            <td>
                <img src="{{asset('storage').'/'.$person->foto}}" width="100" alt="">
            </td>
            <td>{{$person->nombre}}</td>
            <td>{{$person->apellidos}}</td>
            <td>{{$person->ci}}</td>
            <td>{{$person->fecha_nac}}</td>
            <td>{{$person->area}}</td>
            <td>{{$person->ciudad}}</td>
            <td>{{$person->pais}}</td>
            <td>{{$person->genero}}</td>
            <td>{{$person->celular}}</td>
            <td>{{$person->correo}}</td>
            <td>{{$person->descripcion}}</td>

            <!--@foreach($rol_registrado as $rol)
                @if($person->rol_id == $rol->id)
                <td>{{$rol->nombre_rol}}</td>
                @endif
            @endforeach-->
            <td>{{$person->rol->nombre_rol}}</td>
            <td>
            <a href="{{url('/home/personal/'.$person->id.'/edit')}}"  class="btn btn-warning">
            Editar
            
            </a>
                
             || 
                <form action="{{url('/home/personal/'.$person->id)}}"class="d-inline"  method="post">
                @csrf
                {{method_field('DELETE')}}
                <input type="submit" class="btn btn-danger" onclick="return confirm('Seguro que quieres eliminar')" value="Borrar">
                </form>
                
            </td>
            

        </tr>
        @endforeach
        
    </tbody>
    @else
<div class="alert alert-dismissable alert-warning">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <h4>Mensaje del sistema!</h4>
  <p>No se encuentran registros.</p>
</div>

    @endif
</table>
{{$personal->links()}}
</div>
@endsection

