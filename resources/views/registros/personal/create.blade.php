

@extends('layouts.app')

@section('content')
<form action="{{url('/home/personal')}}" method="post" enctype="multipart/form-data">
    
@csrf

@include('registros.personal.form',['modo'=>'Crear'])

</form>
@endsection