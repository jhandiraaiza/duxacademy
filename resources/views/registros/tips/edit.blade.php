@extends('layouts.app')

@section('content')
<form action="{{url('/home/tips/'.$tips->id)}}" method="post" enctype="multipart/form-data">
@csrf
{{method_field('PATCH')}}
@include('registros.tips.form',['modo'=>'Editar'])
</form>
@endsection
