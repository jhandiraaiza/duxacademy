


@extends('layouts.app')

@section('content')

<div class="container">



    
    @if(Session::has('mensaje'))
    <div class="alert alert-success alert-dismissible" role="alert">
    
    {{Session::get('mensaje')}}

    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
        
    </div>
    @endif

   

    

<a href="{{url('home/tips/create')}}" class="btn btn-success">Registrar tips  </a>
<br>
<br>



<table class="table table-light">
    @if(count($tips)>0)
    
    <thead class="thead-light">
        <tr>
            <th>id</th>
            <th>Imagen</th>
            <th>Autor</th>
            <th>Titulo</th>
            <th>Descripcion</th>
            <th>Link</th>
            <th>Acciones</th>
            
        </tr>
    </thead>
    <tbody>
        
        @foreach($tips as $tip)
        <tr>
            <td>{{$tip->id}}</td>
            <td>
                <img src="{{asset('storage').'/'.$tip->imagen}}" width="100" alt="">
            </td>
            <td>{{$tip->autor}}</td>
            <td>{{$tip->titulo}}</td>
            <td>{{$tip->descripcion}}</td>
            <td>{{$tip->links}}</td>
            <td>
            <a href="{{url('/home/tips/'.$tip->id.'/edit')}}" class="btn btn-warning">
            Editar
            
            </a>
                
             || 
                <form action="{{url('/home/tips/'.$tip->id)}}" class="d-inline" method="post">
                @csrf
                {{method_field('DELETE')}}
                <input class="btn btn-danger" type="submit" onclick="return confirm('Seguro que quieres eliminar')" value="Borrar">
                </form>
                
            </td>

        </tr>
        @endforeach
        
    </tbody>
    @else
<div class="alert alert-dismissable alert-warning">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <h4>Mensaje del sistema!</h4>
  <p>No se encuentran registros.</p>
</div>

    @endif
    
</table>
{{$tips->links()}}
</div>
@endsection





