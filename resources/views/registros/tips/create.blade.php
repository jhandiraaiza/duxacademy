
@extends('layouts.app')

@section('content')

<form action="{{url('/home/tips')}}" method="post"  enctype="multipart/form-data">
    @csrf
    @include('registros.tips.form',['modo'=>'Crear'])
</form>
@endsection