
<h1>{{$modo}} tip </h1>

@if(count($errors)>0)

    <div class="alert alert-danger" role="alert">

    <ul>
    @foreach($errors->all() as $error)
        <li>{{$error}}</li>

    @endforeach
    </ul>
    </div>


@endif
<div class= "container">

<div class="form-group">

<label for="autor">Autor</label>
<input type="text" class="form-control" name="autor" id="autor" value="{{isset($tips->autor)?$tips->autor:old('autor')}}">

</div>

<div class="form-group">

<label for="titulo">Titulo</label>
<input type="text" class="form-control" name="titulo" id="titulo" value="{{isset($tips->titulo)?$tips->titulo:old('titulo')}}">

</div>

<div class="form-group">
<label for="descripcion">Descripcion</label>
<textarea name="descripcion" class="form-control" id="descripcion" value="">{{isset($tips->descripcion)?$tips->descripcion:old('descripcion')}}</textarea>

</div>

<div class="form-group">
<label for="links">Link</label>
<input type="text" class="form-control" name="links" id="links" value="{{isset($tips->links)?$tips->links:old('links')}}">
</div>

<div class="form-group">
<label for="imagen">Imagen</label>

@if(isset($tips->imagen))
    <img src="{{asset('storage').'/'.$tips->imagen}}" width="100" alt="">
@endif


<input type="file" class="form-control" name="imagen" id="imagen" value="">


</div>

<input type="submit" class="btn btn-success" value="{{$modo}} tip" id="Enviar">
<a class="btn btn-primary" href="{{url('home/tips')}}">Regresar</a>

</div>