@extends('layouts.app')

@section('content')
<form action="{{url('home/testimonio')}}" method="post" enctype="multipart/form-data">
@csrf
@include('registros.testimonio.form',['modo'=>'Crear'])
</form>
@endsection