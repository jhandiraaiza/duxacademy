@extends('layouts.app')

@section('content')
<form action="{{url('/home/testimonio/'.$testimonio->id)}}" method="post" enctype="multipart/form-data">
@csrf
{{method_field('PATCH')}}
@include('registros.testimonio.form',['modo'=>'Editar'])




</form>
@endsection