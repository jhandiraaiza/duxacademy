@extends('layouts.app')

@section('content')


<div class="container">

    @if(Session::has('mensaje'))
    <div class="alert alert-success alert-dismissible" role="alert">
    
    {{Session::get('mensaje')}}

    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
        
    </div>
    @endif

<a href="{{url('home/testimonio/create')}}" class="btn btn-success">Registrar Testimonio</a>
<br>
<br>
<table class="table table-light">
@if(count($testimonios)>0)
    <thead class="thead-light">
        <tr>					
        	
            <th>id</th>
            <th>Nombre</th>
            <th>Pais</th>
            <th>Ciudad</th>
            <th>Testimonio</th>
            <th>Accion</th>
        </tr>
    </thead>
    <tbody>
    @foreach($testimonios as $testimonio)
	
        <tr>
            <td>{{$testimonio->id}}</td>
            <td>{{$testimonio->nombre}}</td>
            <td>{{$testimonio->pais}}</td>
            <td>{{$testimonio->ciudad}}</td>
            <td>{{$testimonio->testimonio}}</td>
            <td>
            <a href="{{url('/home/testimonio/'.$testimonio->id.'/edit')}}" class="btn btn-warning">
            Editar
            
            </a>
                
             || 
                <form action="{{url('/home/testimonio/'.$testimonio->id)}}" class="d-inline" method="post">
                @csrf
                {{method_field('DELETE')}}
                <input type="submit" class="btn btn-danger" onclick="return confirm('Seguro que quieres eliminar')" value="Borrar">
                </form>
                
            </td>
            

        </tr>
        @endforeach
        
    </tbody>
    @else
<div class="alert alert-dismissable alert-warning">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <h4>Mensaje del sistema!</h4>
  <p>No se encuentran registros.</p>
</div>

    @endif
</table>
{{$testimonios->links()}}
</div>
@endsection