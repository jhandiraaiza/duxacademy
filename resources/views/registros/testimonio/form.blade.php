<h1>{{$modo}} Testimonio </h1>   

@if(count($errors)>0)

    <div class="alert alert-danger" role="alert">

    <ul>
    @foreach($errors->all() as $error)
        <li>{{$error}}</li>

    @endforeach
    </ul>
    </div>


@endif

<div class="container">			

<div class="form-group">
<label for="nombre">Nombre:</label>
<input type="text" class="form-control" name="nombre" id="nombre" value="{{isset($testimonio->nombre)?$testimonio->nombre:old('nombre')}}">
</div>

<div class="form-group">
<label for="pais">Pais:</label>
<input type="text" class="form-control" name="pais" id="pais" value="{{isset($testimonio->pais)?$testimonio->pais:old('pais')}}">
</div>

<div class="form-group">
<label for="ciudad">Ciudad:</label>
<input type="text" class="form-control" name="ciudad" id="ciudad" value="{{isset($testimonio->ciudad)?$testimonio->ciudad:old('ciudad')}}">
</div>

<div class="form-group">
<label for="testimonio">testimonio:</label>
<textarea name="testimonio" class="form-control" id="testimonio" value="">{{isset($testimonio->testimonio)?$testimonio->testimonio:old('testimonio')}}</textarea>
</div>



<input type="submit" class="btn btn-success" value="{{$modo}} testimonio" id="Enviar">
<a class="btn btn-primary" href="{{url('home/testimonio')}}">Regresar</a>
    </div>