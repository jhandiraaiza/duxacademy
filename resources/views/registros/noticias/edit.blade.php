@extends('layouts.app')

@section('content')
<form action="{{url('/home/noticias/'.$noticia->id)}}" method="post" enctype="multipart/form-data">
@csrf
{{method_field('PATCH')}}
@include('registros.noticias.form',['modo'=>'Editar'])




</form>
@endsection