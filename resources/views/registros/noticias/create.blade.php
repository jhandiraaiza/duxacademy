@extends('layouts.app')

@section('content')
<form action="{{url('home/noticias')}}" method="post" enctype="multipart/form-data">
@csrf
@include('registros.noticias.form',['modo'=>'Crear'])
</form>
@endsection