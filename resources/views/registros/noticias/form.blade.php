<h1>{{$modo}} noticias </h1>   

@if(count($errors)>0)

    <div class="alert alert-danger" role="alert">

    <ul>
    @foreach($errors->all() as $error)
        <li>{{$error}}</li>

    @endforeach
    </ul>
    </div>


@endif

<div class="container">

<div class="form-group">
<label for="titulo">Titulo</label>
<input type="text" class="form-control" name="titulo" id="titulo" value="{{isset($noticia->titulo)?$noticia->titulo:old('titulo')}}">
</div>

<div class="form-group">
<label for="descripcion">Descripcion</label>
<textarea name="descripcion" class="form-control" id="descripcion" value="">{{isset($noticia->descripcion)?$noticia->descripcion:old('descripcion')}}</textarea>
</div>



<div class="form-group">
<label for="fecha">Fecha:</label>
<input type="date" class="form-control" name="fecha" id="fecha" value="{{isset($noticia->fecha)?$noticia->fecha:old('fecha')}}">
</div>

<div class="form-group">
<label for="link">link:</label>
<input type="text" class="form-control" name="link" id="link" value="{{isset($noticia->link)?$noticia->link:old('link')}}">
</div>


<div class="form-group">
<label for="imagen">Imagen</label>
    @if(isset($noticia->imagen))
    <img src="{{asset('storage').'/'.$noticia->imagen}}" width="100" alt="">
    @endif
    <input type="file" class="form-control" name="imagen" id="imagen" value="">
</div>

<input type="submit" class="btn btn-success" value="{{$modo}} noticia" id="Enviar">
<a class="btn btn-primary" href="{{url('home/noticias')}}">Regresar</a>
    </div>