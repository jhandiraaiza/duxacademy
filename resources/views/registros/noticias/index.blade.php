

@extends('layouts.app')

@section('content')


<div class="container">

    @if(Session::has('mensaje'))
    <div class="alert alert-success alert-dismissible" role="alert">
    
    {{Session::get('mensaje')}}

    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
        
    </div>
    @endif

<a href="{{url('home/noticias/create')}}" class="btn btn-success">Registrar Noticia</a>
<br>
<br>
<table class="table table-light">
@if(count($noticias)>0)
    <thead class="thead-light">
        <tr>
        	
            <th>id</th>
            <th>Imagen</th>
            <th>Titulo</th>
            <th>Descripcion</th>
            <th>Fecha</th>
            <th>Link</th>
            <th>Accion</th>
        </tr>
    </thead>
    <tbody>
    @foreach($noticias as $noticia)
	
        <tr>
            <td>{{$noticia->id}}</td>
            <td>
                <img src="{{asset('storage').'/'.$noticia->imagen}}" width="100" alt="">
            </td>
            <td>{{$noticia->titulo}}</td>
            <td>{{$noticia->descripcion}}</td>
            <td>{{$noticia->fecha}}</td>
            <td>{{$noticia->link}}</td>
            <td>
            <a href="{{url('/home/noticias/'.$noticia->id.'/edit')}}" class="btn btn-warning">
            Editar
            
            </a>
                
             || 
                <form action="{{url('/home/noticias/'.$noticia->id)}}" class="d-inline" method="post">
                @csrf
                {{method_field('DELETE')}}
                <input type="submit" class="btn btn-danger" onclick="return confirm('Seguro que quieres eliminar')" value="Borrar">
                </form>
                
            </td>
            

        </tr>
        @endforeach
        
    </tbody>
    @else
<div class="alert alert-dismissable alert-warning">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <h4>Mensaje del sistema!</h4>
  <p>No se encuentran registros.</p>
</div>

    @endif
</table>
{{$noticias->links()}}
</div>
@endsection

