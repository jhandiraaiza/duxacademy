

@extends('layouts.app')

@section('content')

<div class="container">

    @if(Session::has('mensaje'))
    <div class="alert alert-success alert-dismissible" role="alert">
    
    {{Session::get('mensaje')}}

    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
        
    </div>
    @endif


<a href="{{url('home/comunidad/create')}}" class="btn btn-success">Registrar comunidad</a>
<br>
<br>
<table class="table table-light">
@if(count($comunidad)>0)
    <thead class="thead-light">
        <tr>
        
            <th>id</th>
            <th>Foto</th>
            <th>Titulo</th>
            <th>Expositor</th>
            <th>Apellido</th>
            <th>Descripcion</th>
            <th>Link</th>
            
            <th>Accion</th>
        </tr>
    </thead>
    <tbody>
    @foreach($comunidad as $com)
   
        <tr>			
        	
            <td>{{$com->id}}</td>
            <td>
                <img src="{{asset('storage').'/'.$com->foto}}" width="100" alt="">
            </td>
            <td>{{$com->titulo}}</td>
            <td>{{$com->expositor}}</td>
            <td>{{$com->apel_expositor}}</td>
            <td>{{$com->descripcion}}</td>
            <td>{{$com->link}}</td>
  
            <td>
            <a href="{{url('/home/comunidad/'.$com->id.'/edit')}}"  class="btn btn-warning" >
            Editar
            
            </a>
                
             || 
                <form action="{{url('/home/comunidad/'.$com->id)}}" class="d-inline"  method="post">
                @csrf
                {{method_field('DELETE')}}
                <input type="submit" class="btn btn-danger" onclick="return confirm('Seguro que quieres eliminar')" value="Borrar">
                </form>
                
            </td>
            

        </tr>
        @endforeach
        
    </tbody>
    @else
<div class="alert alert-dismissable alert-warning">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <h4>Mensaje del sistema!</h4>
  <p>No se encuentran registros.</p>
</div>

    @endif
</table>
{{$comunidad->links()}}
</div>
@endsection

