
<h1>{{$modo}} premio </h1>   

@if(count($errors)>0)

    <div class="alert alert-danger" role="alert">

    <ul>
    @foreach($errors->all() as $error)
        <li>{{$error}}</li>

    @endforeach
    </ul>
    </div>


@endif

<div class="container">

<div class="form-group">
<label for="titulo">Titulo</label>
<input type="text" class="form-control" name="titulo" id="titulo" value="{{isset($comunidad->titulo)?$comunidad->titulo:old('titulo')}}">
</div>

<div class="form-group">
<label for="expositor">Expositor</label>
<input type="text" class="form-control" name="expositor" id="expositor" value="{{isset($comunidad->expositor)?$comunidad->expositor:old('expositor')}}">
</div>

<div class="form-group">
<label for="apel_expositor">Apellido expositor</label>
<input type="text" class="form-control" name="apel_expositor" id="apel_expositor" value="{{isset($comunidad->apel_expositor)?$comunidad->apel_expositor:old('apel_expositor')}}">
</div>

<div class="form-group">
<label for="descripcion">Descripcion</label>
<textarea name="descripcion"  class="form-control" id="descripcion" value="">{{isset($comunidad->descripcion)?$comunidad->descripcion:old('descripcion')}}</textarea>
</div>

<div class="form-group">
<label for="link">Link</label>
<input type="text" class="form-control" name="link" id="link" value="{{isset($comunidad->link)?$comunidad->link:old('link')}}">
</div>

<div class="form-group">
<label for="foto">Imagen</label>
@if(isset($comunidad->foto))
    <img src="{{asset('storage').'/'.$comunidad->foto}}" width="100" alt="">
    @endif
<input type="file"  class="form-control"  name="foto" id="foto" value="">
</div>


<input type="submit" class="btn btn-success" value="{{$modo}} comunidad" id="Enviar">
<a class="btn btn-primary" href="{{url('home/comunidad')}}">Regresar</a>
</div>
