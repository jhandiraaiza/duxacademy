
@extends('layouts.app')

@section('content')
<form action="{{url('/home/comunidad/'.$comunidad->id)}}" method="post" enctype="multipart/form-data">
@csrf
{{method_field('PATCH')}}
@include('registros.comunidad.form',['modo'=>'Editar'])



</form>
@endsection
