@extends('layouts.app')

@section('content')
<form action="{{url('home/comunidad')}}" method="post" enctype="multipart/form-data">
    
    @csrf
    @include('registros.comunidad.form',['modo'=>'Crear'])
 
</form>
@endsection
