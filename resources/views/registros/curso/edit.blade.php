@extends('layouts.app')

@section('content')
<form action="{{url('/home/cursos/'.$curso->id)}}" method="post" enctype="multipart/form-data">
@csrf
{{method_field('PATCH')}}
@include('registros.curso.form',['modo'=>'Editar'])




</form>
@endsection
