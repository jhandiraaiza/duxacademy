
@extends('layouts.app')

@section('content')


<div class="container">



    
    @if(Session::has('mensaje'))
    <div class="alert alert-success alert-dismissible" role="alert">
    
    {{Session::get('mensaje')}}

    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
        
    </div>
    @endif

<a href="{{url('home/cursos/create')}}" class="btn btn-success">Registrar Curso</a>
<br>
<br>
<table class="table table-light">
@if(count($cursos)>0)
    <thead class="thead-light">
        <tr>
            <th>id</th>
            <th>Imagen</th>
            <th>Nombre curso</th>
            <th>Descripcion</th>
            <th>Categoria</th>
            <th>Tipo</th>
            <th>Institucion</th>
            <th>Imagen Ins</th>
            <th>Instructor</th>
            <th>Foto</th>
            <th>Modalidad</th>
            <th>Fecha</th>
            <th>Publico</th>
            <th>Certificado</th>
            <th>Temario</th>
            <th>Precio</th>
            <th>Accion</th>
        </tr>
    </thead>
    <tbody>
    @foreach($cursos as $curso)





        <tr>
            <td>{{$curso->id}}</td>
            <td>
                <img src="{{asset('storage').'/'.$curso->imagen}}" width="100" alt="">
            </td>
            <td>{{$curso->nombrecurso}}</td>
            <td>{{$curso->descripcion}}</td>
            <td>{{$curso->categoria}}</td>
            <td>{{$curso->tipo}}</td>
            <td>{{$curso->institucion}}</td>
            <td>
                <img src="{{asset('storage').'/'.$curso->logo_ins}}" width="100" alt="">
            </td>
            <td>{{$curso->instructor}}</td>
            <td>
                <img src="{{asset('storage').'/'.$curso->foto}}" width="100" alt="">
            </td>
            <td>{{$curso->Modalidad}}</td>
            <td>{{$curso->fecha}}</td>
            <td>{{$curso->publico}}</td>
            <td>{{$curso->certificado}}</td>
            <td>
                <embed src="{{asset('storage').'/'.$curso->temario}}" width="100" alt="">
            </td>
            <td>{{$curso->Precio}}</td>
            <td>
            <a href="{{url('/home/cursos/'.$curso->id.'/edit')}}" class="btn btn-warning">
            Editar
            
            </a>
                
             || 
                <form action="{{url('/home/cursos/'.$curso->id)}}" class="d-inline" method="post">
                @csrf
                {{method_field('DELETE')}}
                <input type="submit" class="btn btn-danger" onclick="return confirm('Seguro que quieres eliminar')" value="Borrar">
                </form>
                
            </td>
            

        </tr>
        @endforeach
        
    </tbody>
    @else
<div class="alert alert-dismissable alert-warning">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <h4>Mensaje del sistema!</h4>
  <p>No se encuentran registros.</p>
</div>

    @endif
</table>
{{$cursos->links()}}
</div>
@endsection

