@extends('layouts.app')

@section('content')
<form action="{{url('home/cursos')}}" method="post" enctype="multipart/form-data">
@csrf
@include('registros.curso.form',['modo'=>'Crear'])
</form>
@endsection

