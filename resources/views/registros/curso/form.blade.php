<h1>{{$modo}} curso </h1>   

@if(count($errors)>0)

    <div class="alert alert-danger" role="alert">

    <ul>
    @foreach($errors->all() as $error)
        <li>{{$error}}</li>

    @endforeach
    </ul>
    </div>


@endif
				

<div class="container">

<div class="form-group">
<label for="nombrecurso">Nombre del curso</label>
<input type="text" class="form-control" name="nombrecurso" id="nombrecurso" value="{{isset($curso->nombrecurso)?$curso->nombrecurso:old('nombrecurso')}}">
</div>

<div class="form-group">
<label for="descripcion">Descripcion</label>
<textarea name="descripcion" class="form-control" id="descripcion" value="">{{isset($curso->descripcion)?$curso->descripcion:old('descripcion')}}</textarea>
</div>

<div class="form-group">
<label for="categoria">Categoria</label>
<input type="text" class="form-control" name="categoria" id="categoria" value="{{isset($curso->categoria)?$curso->categoria:old('categoria')}}">
</div>

<div class="form-group">
<label for="cupos">Tipo</label>
<input type="text" class="form-control" name="tipo" id="tipo" value="{{isset($curso->tipo)?$curso->tipo:old('tipo')}}">
</div>

<div class="form-group">
<label for="cupos">Institucion</label>
<input type="text" class="form-control" name="institucion" id="institucion" value="{{isset($curso->institucion)?$curso->institucion:old('institucion')}}">
</div>
		
<div class="form-group">
<label for="foto">Logo institucion</label>
    @if(isset($curso->logo_ins))
    <img src="{{asset('storage').'/'.$curso->logo_ins}}" width="100" alt="">
    @endif
    <input type="file" class="form-control" name="logo_ins" id="logo_ins" value="">
</div>

<div class="form-group">
<label for="instructor">Instructor</label>
<input type="text" class="form-control" name="instructor" id="instructor" value="{{isset($curso->instructor)?$curso->instructor:old('instructor')}}">
</div>

<div class="form-group">
<label for="foto">Foto instructor</label>
    @if(isset($curso->foto))
    <img src="{{asset('storage').'/'.$curso->foto}}" width="100" alt="">
    @endif
    <input type="file" class="form-control" name="foto" id="foto" value="">
</div>
				

<div class="form-group">
<label for="Modalidad">Modalidad</label>
<select  class="form-control" name="Modalidad" id="Modalidad" >
    @if(isset($curso->Modalidad))
        <option value="{{$curso->Modalidad}}">{{$curso->Modalidad}}</option> 
    @endif
    <option value="100% presencial">100% presencial</option>
    <option value="Semipresencial">Semipresencial</option>
    <option value="100% virtual">100% virtual</option>
    <option value="Presencial/virtua">Presencial/virtual</option>

</select>
</div>




<div class="form-group">
<label for="duracion">Fecha</label>
<input type="text" class="form-control" name="fecha" id="fecha" value="{{isset($curso->fecha)?$curso->fecha:old('fecha')}}">
</div>

<div class="form-group">
<label for="cupos">Publico</label>
<input type="text" class="form-control" name="publico" id="publico" value="{{isset($curso->publico)?$curso->publico:old('publico')}}">
</div>

<div class="form-group">
<label for="empresa">Certificado</label>
<input type="text" class="form-control" name="certificado" id="certificado" value="{{isset($curso->certificado)?$curso->certificado:old('certificado')}}">
</div>

<div class="form-group">
<label for="archivo">Temario</label>
    @if(isset($curso->temario))
    <embed src="{{asset('storage').'/'.$curso->temario}}" width="100" alt="">
    @endif
    <input type="file" class="form-control" name="temario" id="temario" value="">
</div>

<div class="form-group">
<label for="Precio">Precio</label>
    <input type="text" class="form-control" name="Precio" id="Precio" value="{{isset($curso->Precio)?$curso->Precio:old('Precio')}}">
</div>

<div class="form-group">
<label for="foto">Imagen</label>
    @if(isset($curso->imagen))
    <img src="{{asset('storage').'/'.$curso->imagen}}" width="100" alt="">
    @endif
    <input type="file" class="form-control" name="imagen" id="imagen" value="">
</div>

<input type="submit" class="btn btn-success" value="{{$modo}} curso" id="Enviar">
<a class="btn btn-primary" href="{{url('home/cursos')}}">Regresar</a>



    </div>