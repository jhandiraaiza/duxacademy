

@extends('layouts.app')

@section('content')
<form action="{{url('/home/rol/'.$rol->id)}}" method="post">
@csrf
{{method_field('PATCH')}}
@include('registros.roles.form',['modo'=>'Editar'])
</form>
@endsection