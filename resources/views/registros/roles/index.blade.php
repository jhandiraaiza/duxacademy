

@extends('layouts.app')

@section('content')

<div class="container">

    @if(Session::has('mensaje'))
    <div class="alert alert-success alert-dismissible" role="alert">
    
    {{Session::get('mensaje')}}

    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
        
    </div>
    @endif

<a href="{{url('home/rol/create')}}"  class="btn btn-success" >Registrar rol</a>
<br><br>
<table class="table table-light">
@if(count($rol_registrado)>0)
    <thead class="thead-light">
        <tr>
            <th>id</th>
            <th>rol</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach($rol_registrado as $rol)
        <tr>
            <td>{{$rol->id}}</td>
            <td>{{$rol->nombre_rol}}</td>
            <td>
            <a href="{{url('/home/rol/'.$rol->id.'/edit')}}" class="btn btn-warning" >
            Editar
    
            
            </a>
                
             || 
                <form action="{{url('/home/rol/'.$rol->id)}}" class="d-inline" method="post">
                @csrf
                {{method_field('DELETE')}}
                <input type="submit" class="btn btn-danger" onclick="return confirm('Seguro que quieres eliminar')" value="Borrar">
                </form>
                
            </td>

        </tr>
        @endforeach
    </tbody>
    @else
<div class="alert alert-dismissable alert-warning">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <h4>Mensaje del sistema!</h4>
  <p>No se encuentran registros.</p>
</div>

    @endif
</table>
{{$rol_registrado->links()}}
</div>
@endsection

