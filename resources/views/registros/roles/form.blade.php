
<h1>{{$modo}} rol </h1>   

@if(count($errors)>0)

    <div class="alert alert-danger" role="alert">

    <ul>
    @foreach($errors->all() as $error)
        <li>{{$error}}</li>

    @endforeach
    </ul>
    </div>


@endif

<div class="container">

<div class="form-group">
<label for="nombre_rol">Rol</label>
<input type="text" class="form-control" name="nombre_rol" value="{{isset($rol->nombre_rol)?$rol->nombre_rol:''}}">
</div>

<input type="submit" class="btn btn-success" value="{{ $modo }} rol">

<a  class="btn btn-primary"   href="{{url('home/rol')}}">Regresar</a>
</div>
