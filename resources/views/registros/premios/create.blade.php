@extends('layouts.app')

@section('content')
<form action="{{url('home/premios')}}" method="post" enctype="multipart/form-data">
    
    @csrf
    @include('registros.premios.form',['modo'=>'Crear'])
 
    
    

</form>
@endsection
