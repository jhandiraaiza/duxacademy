<h1>{{$modo}} premio </h1>   

@if(count($errors)>0)

    <div class="alert alert-danger" role="alert">

    <ul>
    @foreach($errors->all() as $error)
        <li>{{$error}}</li>

    @endforeach
    </ul>
    </div>


@endif

<div class="container">


<div class="form-group">
<label for="titulo">Nombre</label>
<input type="text" class="form-control" name="nombre" id="nombre" value="{{isset($premio->nombre)?$premio->nombre:old('nombre')}}">
</div>

<div class="form-group">
<label for="titulo">Fecha</label>
<input type="text" class="form-control" name="fecha" id="fecha" value="{{isset($premio->fecha)?$premio->fecha:old('fecha')}}">
</div>

<div class="form-group">
<label for="titulo">Institucion</label>
<input type="text" class="form-control" name="institucion" id="institucion" value="{{isset($premio->institucion)?$premio->institucion:old('institucion')}}">
</div>

<div class="form-group">
<label for="descripcion">Descripcion</label>
<textarea name="descripcion"  class="form-control" id="descripcion" value="">{{isset($premio->descripcion)?$premio->descripcion:old('descripcion')}}</textarea>
</div>

<div class="form-group">
<label for="titulo">Pais</label>
<input type="text" class="form-control" name="pais" id="pais" value="{{isset($premio->pais)?$premio->pais:old('pais')}}">
</div>

<div class="form-group">
<label for="imagen">Imagen</label>
@if(isset($premio->imagen))
    <img src="{{asset('storage').'/'.$premio->imagen}}" width="100" alt="">
    @endif
<input type="file"  class="form-control"  name="imagen" id="imagen" value="">
</div>


<input type="submit" class="btn btn-success" value="{{$modo}} premio" id="Enviar">
<a class="btn btn-primary" href="{{url('home/premios')}}">Regresar</a>
</div>
