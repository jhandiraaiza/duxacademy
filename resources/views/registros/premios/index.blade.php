

@extends('layouts.app')

@section('content')

<div class="container">

    @if(Session::has('mensaje'))
    <div class="alert alert-success alert-dismissible" role="alert">
    
    {{Session::get('mensaje')}}

    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
        
    </div>
    @endif


<a href="{{url('home/premios/create')}}" class="btn btn-success">Registrar premio</a>
<br>
<br>
<table class="table table-light">
@if(count($premios)>0)
    <thead class="thead-light">
        <tr>
        
            <th>id</th>
            <th>Imagen</th>
            <th>Nombre</th>
            <th>Fecha</th>
            <th>Institucion</th>
            <th>Descripcion</th>
            <th>Pais</th>
            
            <th>Accion</th>
        </tr>
    </thead>
    <tbody>
    @foreach($premios as $premio)
   
        <tr>			

            <td>{{$premio->id}}</td>
            <td>
                <img src="{{asset('storage').'/'.$premio->imagen}}" width="100" alt="">
            </td>
            <td>{{$premio->nombre}}</td>
            <td>{{$premio->fecha}}</td>
            <td>{{$premio->institucion}}</td>
            <td>{{$premio->descripcion}}</td>
            <td>{{$premio->pais}}</td>
  
            <td>
            <a href="{{url('/home/premios/'.$premio->id.'/edit')}}"  class="btn btn-warning" >
            Editar
            
            </a>
                
             || 
                <form action="{{url('/home/premios/'.$premio->id)}}" class="d-inline"  method="post">
                @csrf
                {{method_field('DELETE')}}
                <input type="submit" class="btn btn-danger" onclick="return confirm('Seguro que quieres eliminar')" value="Borrar">
                </form>
                
            </td>
            

        </tr>
        @endforeach
        
    </tbody>
    @else
<div class="alert alert-dismissable alert-warning">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <h4>Mensaje del sistema!</h4>
  <p>No se encuentran registros.</p>
</div>

    @endif
</table>
{{$premios->links()}}
</div>
@endsection

