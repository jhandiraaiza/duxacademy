
@extends('layouts.app')

@section('content')
<form action="{{url('/home/premios/'.$premio->id)}}" method="post" enctype="multipart/form-data">
@csrf
{{method_field('PATCH')}}
@include('registros.premios.form',['modo'=>'Editar'])



</form>
@endsection
