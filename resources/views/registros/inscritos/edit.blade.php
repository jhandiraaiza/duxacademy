@extends('layouts.app')

@section('content')
<form action="{{url('/home/inscripcion/'.$inscripcion->id)}}" method="post" enctype="multipart/form-data">
@csrf
{{method_field('PATCH')}}

<div class="container">

            <div class="form-group">
            <label for="curso">Curso</label>
            <input type="text" class="form-control" name="curso" id="curso" readonly onmousedown="return false;" value="{{$inscripcion->curso}}">
            </div>

            <div class="form-group">
            <label for="nombre">Nombre</label>
            <input type="text" class="form-control" name="nombre" id="nombre" value="{{isset($inscripcion->nombre)?$inscripcion->nombre:old('nombre')}}">
            </div>

            <div class="form-group">
            <label for="apellidopaterno">Apellido Paterno</label>
            <input type="text" class="form-control" name="apellidopaterno" id="apellidopaterno" value="{{isset($inscripcion->apellidopaterno)?$inscripcion->apellidopaterno:old('apellidopaterno')}}">
            </div>

            <div class="form-group">	
            <label for="apellidomaterno">Apellido Materno</label>
            <input type="text" class="form-control" name="apellidomaterno" id="apellidomaterno" value="{{isset($inscripcion->apellidomaterno)?$inscripcion->apellidomaterno:old('apellidomaterno')}}">
            </div>

            <div class="form-group">
            <label for="ci">Cedula</label>
            <input type="text" class="form-control" name="ci" id="ci" value="{{isset($inscripcion->ci)?$inscripcion->ci:old('ci')}}">
            </div>

            <div class="form-group">
            <label for="correo">Correo</label>
            <input type="text" class="form-control" name="correo" id="correo" value="{{isset($inscripcion->correo)?$inscripcion->correo:old('correo')}}">
            </div>

            <div class="form-group">
            <label for="num_cuenta">Num. cuenta</label>
            <input type="text" class="form-control" name="num_cuenta" id="num_cuenta"  readonly onmousedown="return false;" value="{{isset($inscripcion->num_cuenta)?$inscripcion->num_cuenta:old('num_cuenta')}}">
            </div>

            <div class="form-group">
            <label for="mensaje">Mensaje</label>
            <textarea name="mensaje" class="form-control" id="mensaje" value="">{{isset($inscripcion->mensaje)?$inscripcion->mensaje:old('mensaje')}}</textarea>
            </div>

            <div class="form-group">
            <label for="imagen">Foto del baucher</label><br>
            @if(isset($inscripcion->imagen))
            <img src="{{asset('storage').'/'.$inscripcion->imagen}}" width="500" alt="">
             @endif
            <!--<input type="file" class="form-control" name="imagen" id="imagen" readonly onmousedown="return false;" value=""  >-->
            </div>

            <input type="submit" class="btn btn-success" value="Editar" id="Enviar">
            <a class="btn btn-primary" href="{{url('/home/inscripcion')}}">Regresar</a>
                </div>




</form>
@endsection