@extends('layouts.app')

@section('content')


<div class="container">

    @if(Session::has('mensaje'))
    <div class="alert alert-success alert-dismissible" role="alert">
    
    {{Session::get('mensaje')}}

    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
        
    </div>
    @endif
<br>
<br>
<table class="table table-light">
@if(count($inscripcion)>0)
    <thead class="thead-light">
        <tr>
        	


            <th>id</th>
            <th>Imagen</th>
            <th>Curso</th>
            <th>Nombre</th>
            <th>Apellido paterno</th>
            <th>Apellido materno</th>
            <th>Cedula</th>
            <th>correo</th>
            <th>Numero C</th>
            <th>Mensaje</th>
            <th>Accion</th>
        </tr>
    </thead>
    <tbody>
    @foreach($inscripcion as $i)
        <tr>
            <td>{{$i->id}}</td>
            <td>
                <img src="{{asset('storage').'/'.$i->imagen}}" width="100" alt="">
            </td>
            <td>{{$i->curso}}</td>
            <td>{{$i->nombre}}</td>
            <td>{{$i->apellidopaterno}}</td>
            <td>{{$i->apellidomaterno}}</td>
            <td>{{$i->ci}}</td>
            <td>{{$i->correo}}</td>
            <td>{{$i->num_cuenta}}</td>
            <td>{{$i->mensaje}}</td>
            <td>

            <a href="{{url('/home/enviar/'.$i->id)}}" class="btn btn-primary">
            Enviar mensaje
            
            </a>



            <a href="{{url('/home/inscripcion/'.$i->id.'/edit')}}" class="btn btn-warning">
            Editar
            
            </a>
                
             || 
                <form action="{{url('/home/inscripcion/'.$i->id)}}" class="d-inline" method="post">
                @csrf
                {{method_field('DELETE')}}
                <input type="submit" class="btn btn-danger" onclick="return confirm('Seguro que quieres eliminar')" value="Borrar">
                </form>
                
            </td>
            

        </tr>
        @endforeach
        
    </tbody>
    @else
<div class="alert alert-dismissable alert-warning">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <h4>Mensaje del sistema!</h4>
  <p>No se encuentran registros.</p>
</div>

    @endif
</table>
{{$inscripcion->links()}}
</div>
@endsection
