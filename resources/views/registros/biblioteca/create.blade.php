@extends('layouts.app')

@section('content')
<form action="{{url('home/biblioteca')}}" method="post" enctype="multipart/form-data">
@csrf
@include('registros.biblioteca.form',['modo'=>'Crear'])
</form>
@endsection

