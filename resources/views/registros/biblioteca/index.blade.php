
@extends('layouts.app')

@section('content')


<div class="container">

    @if(Session::has('mensaje'))
    <div class="alert alert-success alert-dismissible" role="alert">
    
    {{Session::get('mensaje')}}

    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
        
    </div>
    @endif

<a href="{{url('home/biblioteca/create')}}" class="btn btn-success">Registrar Libro</a>
<br>
<br>
<table class="table table-light">
@if(count($bibliotecas)>0)
    <thead class="thead-light">
        <tr>
        	
            <th>id</th>
            <th>Imagen</th>
            <th>Archivo</th>
            <th>Titulo</th>
            <th>Autor</th>
            <th>Editorial</th>
            <th>Edicion</th>
            <th>Categoria</th>
            <th>Accion</th>
        </tr>
    </thead>
    <tbody>					
    @foreach($bibliotecas as $biblioteca)
	
        <tr>
            <td>{{$biblioteca->id}}</td>
            <td>
                <img src="{{asset('storage').'/'.$biblioteca->imagen}}" width="100" alt="">
            </td>
            <td>
                <embed src="{{asset('storage').'/'.$biblioteca->archivo}}" width="100" alt="">
            </td>
            <td>{{$biblioteca->titulo}}</td>
            <td>{{$biblioteca->autor}}</td>
            <td>{{$biblioteca->editorial}}</td>
            <td>{{$biblioteca->edicion}}</td>
            <td>{{$biblioteca->categoria}}</td>
            <td>
            <a href="{{url('/home/biblioteca/'.$biblioteca->id.'/edit')}}" class="btn btn-warning">
            Editar
            
            </a>
                
             || 
                <form action="{{url('/home/biblioteca/'.$biblioteca->id)}}" class="d-inline" method="post">
                @csrf
                {{method_field('DELETE')}}
                <input type="submit" class="btn btn-danger" onclick="return confirm('Seguro que quieres eliminar')" value="Borrar">
                </form>
                
            </td>
            

        </tr>
        @endforeach
        
    </tbody>
    @else
<div class="alert alert-dismissable alert-warning">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <h4>Mensaje del sistema!</h4>
  <p>No se encuentran registros.</p>
</div>

    @endif
</table>
{{$bibliotecas->links()}}
</div>
@endsection

