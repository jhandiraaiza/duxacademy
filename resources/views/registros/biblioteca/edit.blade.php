@extends('layouts.app')

@section('content')
<form action="{{url('/home/biblioteca/'.$biblioteca->id)}}" method="post" enctype="multipart/form-data">
@csrf
{{method_field('PATCH')}}
@include('registros.biblioteca.form',['modo'=>'Editar'])




</form>
@endsection