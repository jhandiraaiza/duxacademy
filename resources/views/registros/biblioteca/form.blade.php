<h1>{{$modo}} biblioteca </h1>   

@if(count($errors)>0)

    <div class="alert alert-danger" role="alert">

    <ul>
    @foreach($errors->all() as $error)
        <li>{{$error}}</li>

    @endforeach
    </ul>
    </div>


@endif

<div class="container">

<div class="form-group">
<label for="titulo">Titulo</label>
<input type="text" class="form-control" name="titulo" id="titulo" value="{{isset($biblioteca->titulo)?$biblioteca->titulo:old('titulo')}}">
</div>

<div class="form-group">
<label for="autor">Autor</label>
<input type="text" class="form-control" name="autor" id="autor" value="{{isset($biblioteca->autor)?$biblioteca->autor:old('autor')}}">
</div>

<div class="form-group">
<label for="editorial">Editorial</label>
<input type="text" class="form-control" name="editorial" id="editorial" value="{{isset($biblioteca->editorial)?$biblioteca->editorial:old('editorial')}}">
</div>

<div class="form-group">
<label for="edicion">Edicion</label>
<input type="text" class="form-control" name="edicion" id="edicion" value="{{isset($biblioteca->edicion)?$biblioteca->edicion:old('edicion')}}">
</div>

<div class="form-group">
<label for="categoria">Categoria</label>
<input type="text" class="form-control" name="categoria" id="categoria" value="{{isset($biblioteca->categoria)?$biblioteca->categoria:old('categoria')}}">
</div>

<div class="form-group">
<label for="sinopsis">Sinopsis</label>
<textarea name="sinopsis" class="form-control" id="sinopsis" value="">{{isset($biblioteca->sinopsis)?$biblioteca->sinopsis:old('sinopsis')}}</textarea>
</div>

<div class="form-group">
<label for="imagen">Imagen</label>
    @if(isset($biblioteca->imagen))
    <img src="{{asset('storage').'/'.$biblioteca->imagen}}" width="100" alt="">
    @endif
    <input type="file" class="form-control" name="imagen" id="imagen" value="">
</div>

<div class="form-group">
<label for="archivo">Archivo</label>
    @if(isset($biblioteca->imagen))
    <embed src="{{asset('storage').'/'.$biblioteca->archivo}}" width="100" alt="">
    @endif
    <input type="file" class="form-control" name="archivo" id="archivo" value="">
</div>

<input type="submit" class="btn btn-success" value="{{$modo}} biblioteca" id="Enviar">
<a class="btn btn-primary" href="{{url('home/biblioteca')}}">Regresar</a>
    </div>