<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'DUX') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
   
    
    
</head>
<body>
    <div id="app">
        
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand " href="{{ url('/') }}">
                    DUX
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto ">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            @if (Route::has('login'))
                                <li class="nav-item">
                                    <li class="nav-link"><a  href="{{ route('login') }}">{{ __('Login') }}</a></li>
                                    <li class="nav-link"><a  href="{{ url('/control') }}">Control</a></li>
                                    
                                    
                                </li>
                            @endif

                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                            <!-- style="display: none" /home/pasante-->
                        @else
                        
                            
                            <li class="nav-item dropdown">
                            <li class="nav-link"><a href="{{url('/home')}}" >Inicio</a></li>
                            <li class="nav-link"><a href="{{url('/home/rol')}}">Roles</a></li>
                            <li class="nav-link"><a href="{{url('/home/personal')}}">Personal</a></li>
                            <li class="nav-link"><a href="{{url('/home/cursos')}}">Cursos</a></li>
                            <li class="nav-link"><a href="{{url('/home/premios')}}">Premios</a></li>
                            <li class="nav-link"><a href="{{url('/home/noticias')}}">Noticias</a></li>
                            <li class="nav-link"><a href="{{url('/home/tips')}}">Tips</a></li>
                            <li class="nav-link"><a href="{{url('/home/biblioteca')}}">Biblioteca</a></li>
                            <li class="nav-link"><a href="{{url('/home/inscripcion')}}">Inscritos</a></li>
                            <li class="nav-link"><a href="{{url('/home/comunidad')}}">Comunidad</a></li>
                            <li class="nav-link"><a href="{{url('/home/testimonio')}}">Testimonios</a></li>
                            <li class="nav-link"><a href="{{url('/home/pasante')}}">Pasante</a></li>
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>

                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
            
            
        </nav>


        

        <main class="py-4">
            @yield('content')
        </main>
    </div>
    
</body>
</html>
