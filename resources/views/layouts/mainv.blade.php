


<!--<div class="hidden fixed top-0 right-0 px-6 py-4 sm:block">
<a href="{{URL::to('/')}}"><img src="{{asset('img/DUX.png')}}" width="75" alt="Logo"></a>
<a href="{{url('/')}}" class="ml-4 text-sm text-black-700 ">INICIO</a>
<a href="{{url('/cursos')}}" class="ml-4 text-sm text-black-700 ">CURSOS</a>
<a href="{{url('/premios')}}" class="ml-4 text-sm text-black-700 ">TROFEOS</a>
<a href="{{url('/noticias')}}" class="ml-4 text-sm text-black-700 ">NOTICIAS Y EVENTOS</a>
<a href="{{url('/biblioteca')}}" class="ml-4 text-sm text-black-700 " >BIBLIOTECA</a>

<a href="{{url('/tips')}}" class="ml-4 text-sm text-black-700 ">TIPS</a>
<a href="{{url('/sobrenosotros')}}" class="ml-4 text-sm text-black-700 ">SOBRE NOSOTROS</a>
                    
                    @auth
                        <a href="{{ url('/home') }}" class="ml-4 text-sm text-gray-700 ">HOME</a>
                    @else
                        <a href="{{ route('login') }}" class="ml-4 text-sm text-gray-700 ">LOG IN</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}" class="ml-4 text-sm text-gray-700 ">REGISTER</a>
                        @endif
                    @endauth
                    
                </div>-->


<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<link href="{{ asset('css/styles.css') }}" rel="stylesheet" type="text/css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<title>Dux Academy</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css">
<!-- Link Swiper's CSS -->
<link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css"/>
<!--AOS JS-->
<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

</head>

<body>
<!--AOS-->	
<script>
  AOS.init();
  
</script>

<!--FIN AOS-->

<!--Navbar-->
<nav class="navbar navbar-expand-sm bg-light navbar-light ">
	
	<a class="navbar-brand" href="{{url('/')}}">
		<img class="imgNavbar"src="{{ asset('img/logoDux2.png') }}" alt="Logo Dux" style="padding-left: 0.5rem;">
	  </a>
	  <!--<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" >
		<span class="navbar-toggler-icon"></span>
	</button>-->
	

	
	  <div class="collapse navbar-collapse" id="navbarNavDropdown">
	  <ul class="navbar-nav ml-auto">
			<li class="nav-item  @if(isset($nosotros)) {{$nosotros}} @endif">
				<a class="nav-link " href="{{url('/sobrenosotros')}}">NOSOTROS</a>
			</li>
			<li class="nav-item @if(isset($curso)) {{$curso}} @endif">
			
				<a class="nav-link" href="{{url('/cursos')}}">EDUCACIÓN</a>
			
			</li>
			<li class="nav-item @if(isset($biblioteca)) {{$biblioteca}} @endif">
				<a class="nav-link" href="{{url('/biblioteca')}}">BIBLIOTECA</a>
			
			</li>
			<li class="nav-item @if(isset($tips)) {{$tips}} @endif">
				<a class="nav-link" href="{{url('/tips')}}">TIPS & TRICKS</a>
			</li>
			
		</ul>
	</div>
</nav>



<!--FIN Navbar if (window.location.href.indexOf("obras.php") != -1){
//colocas la class
} -->

