<!-- Footer -->
<footer class=" footer page-footer font-small blue-grey lighten-5" style="font-size: .8rem; color: white;" >
	<!-- Footer Links -->
	<div class="container text-center text-md-left mt-5">
  
	  <!-- Grid row -->
	  <div class="row mt-3 dark-grey-text">
  
		<!-- Grid column -->
		<div class="col-md-3 col-lg-4 col-xl-3 mb-4">
  
		  <!-- Content -->
		  <h6 class="text-uppercase font-weight-bold">DUX ACADEMY</h6>
		  <hr class="teal accent-3 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
		  <p>DUX ACADEMY es una academia de crecimiento personal y liderazgo para jóvenes.
			Reconocido por tres grandes instituciones Mujeres TIC, Barcelona Activa y FLACSO Argentina.</p>
			
		</div>
		<!-- Grid column -->
  
		<!-- Grid column -->
		<div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
  
		  <!-- Links -->
		  <h6 class="text-uppercase font-weight-bold">ACERCA DE</h6>
		  <hr class="teal accent-3 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
		  
		  <p>
			<a style="color: white;"href="{{url('/sobrenosotros')}}#equipoDux">Equipo</a>
		  </p>
		  <p>
			<a style="color: white;" href="{{url('/sobrenosotros')}}#logros">Logros</a>
		  </p>
		  <p>
		  @auth
                        <a style="color: white;" href="{{ url('/home') }}">HOME</a>
                    @else
					<a style="color: white;" href="{{ route('login') }}">Iniciar sesión</a>

                    @endauth
			
		  </p>

	
		  
		  
  
		</div>
		<!-- Grid column -->
  
		<!-- Grid column -->
		<div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">
		
  
		  <!-- Links -->
		  <h6 class="text-uppercase font-weight-bold">CONTÁCTANOS</h6>
		  <hr class="teal accent-3 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
		  <p><i class="fas fa-envelope mr-2"></i>duxacademy@gmail.com</p>
		  <p><i class="fas fa-phone mr-2"></i> (+591)76543210</p>
			
  
		</div>
		<!-- Grid column -->

		<!-- Grid column -->
		<div class="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">
  
			<!-- Links -->
			<h6 class="text-uppercase font-weight-bold">BÚSCANOS EN</h6>
			<hr class="teal accent-3 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
			<p>
			  <a style="color: white;" href="https://www.facebook.com/watch/duxacademybo/"><i class="fab fa-facebook-f fa-2x" ></i></a>
			  <a style="color: white;" href="https://www.instagram.com/dux.academy/?hl=es-la"><i class="fab fa-instagram fa-2x" style="padding-left: 1rem;padding-right: 1rem;"></i></a>
		  	  <a style="color: white;" href="https://web.whatsapp.com/"><i class="fab fa-whatsapp fa-2x"></i></a>
			</p>
		</div>
		<!-- Grid column -->

	  </div>
	  <!-- Grid row -->
  
	</div>
	<!-- Footer Links -->
  
	<!-- Copyright -->
	<div class="footer-copyright text-center text-black-50 py-3">
	  © 2021 Dux Academy. Todos los derechos reservados.
	</div>
	<!-- Copyright -->
  
  </footer>
  <!-- Footer -->




<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>
</html>