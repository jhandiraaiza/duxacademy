
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Control</title>
    <link rel="stylesheet" href="{{asset('css/pdf.css')}}">
    
</head>
<body>
    <!--<div class="img-fluid">
    <img class="foto1" src="{{asset('img/logoDux.png')}}" alt="imagen 1">
    </div>
    <div class="img-fluid">
    <img class="foto1" src="{{asset('img/pdfima/dux.png')}}" alt="imagen 1">
    </div>

    <br><br><br>-->
<div style="background-image: url('https://carontestudio.com/img/caronte-web-estudio-logo.png');"></div>

<h1>Informe diario del control de pasantes</h1><br><br>

    
<table class="table table-light">

@if(count($personal)>0)
    <thead class="thead-light" >
        <tr>
            <th>id</th>
            <th>NombreCompleto</th>
            <th>Area</th>
            <th>Actividades</th>
            <th>Horaentrada</th>
            <th>Horasalida</th>
            <th>HoraTotal</th>
        </tr>
    </thead>
    <tbody>
    @foreach($personal as $person)

        @if($person->estado != '')
    
            <tr>									 
                <td>{{$person->id}}</td>
                <td>{{$person->nombre . $person->apellidos}}</td>
            
                <td>{{$person->area}}</td>
                <td>{{$person->actividades}}</td>
                <td>{{$person->inicio}}</td>
                <td>{{$person->fin}}</td>
                <td><?php
                $inicio=$person->inicio;
                $final=$person->fin;
                 
                $segundos=strtotime($final)-strtotime($inicio); // 36000
                $horas=$segundos/3600; // 10
                echo $horas ?></td>
            </tr>
        @endif
    @endforeach
        
    </tbody>
    @else
<div class="alert alert-dismissable alert-warning">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <h4>Mensaje del sistema!</h4>
  <p>No se encuentran registros.</p>
</div>

    @endif
</table>

    
</body>
</html>
<!--public_path -->