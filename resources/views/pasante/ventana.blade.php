

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Dux control</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <!----CSS link----->
    <link rel="stylesheet" href="{{asset('css/ventana.css')}}">
  </head>
  <body>
  @if(Session::has('mensaje'))
    <div class="alert alert-success alert-dismissible" role="alert">
    
    {{Session::get('mensaje')}}

    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
        
    </div>
    @endif
    <main class="d-flex align-items-center min-vh-100 py-3 py-md-0">
      <div class="container">
        <div class="card login-card">
          <div class="row no-gutters">
            <div class="col-md-5">
              <img src="{{asset('img/fondoPrincipalDux.png')}}" class="login-card-img">
            </div>
            <div class="col-md-7">
              <div class="card-body">
                <!--<div class="brand-wrapper">
                  <img src="image/logo.svg" class="logo">fondoPrincipalDux.png
                </div>-->
                <p class="login-card-description">Ingreso</p>

                <form action="{{url('/control/verificar')}}" method="get" enctype="multipart/form-data">
                

                    <div class="form-group">
                    <label for="email" class="sr-only">Email</label>
                    <input type="email" class="form-control" name="login" id="login" placeholder="Email adress" value="">
                    </div>

                    <div class="form-group">
                    <label for="Password" class="sr-only">Password</label>
                    <input type="password" class="form-control" name="password" id="password" placeholder="**********" value="">
                    </div>

                    <input type="submit" class="btn btn-block login-btn mb-4" value="Ingresar" id="Enviar">



                </form>

              </div>
            </div>
          </div>
        </div>
      </div>
    </main>
    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
  </body>
</html>