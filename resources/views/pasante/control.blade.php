




<html>
  <head>
    <!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<title>Dux Academy</title>
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css">
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
      <link rel="stylesheet" href="{{asset('css/formAsistencia.css')}}">

  </head>
  
  <body class="formAsistencia">

    <div class="row rowAsistencia" >
      <div class="col-sm-2 textoInforme">INFORME</div>
      <div class="col-sm-2 textoDiario">diario</div>
      <div class="col-sm-8 colForm">
        
        
        <div class="rowForm1">
          <div class="divIcono" style="text-align: left;">
            <span class="fa-stack fa-lg icono">
              <i class="fa fa-circle fa-stack-2x"></i>
              <i class="fa-solid fa-user fa-stack-1x fa-inverse"></i>
            </span>
          </div>
          <div> 
            <input type="text" class="pasante" value="{{$pas->nombre . $pas->apellidos}}" ><!--NOMbre pasante-->
          </div>
          <div class="backLogoDux"><a href="{{url('/')}}"><img src="{{asset('img/logoDux1.png')}}" alt=""></a></div>
        </div> 
        
        @if($pas->estado == '' or $pas->estado == 'Desactivar')
          <form class="form" action="{{url('/control/marcarentrada')}} " enctype="multipart/form-data"  method="get">
            <div class="row rowForm">
            <div class="col-sm-6">
              <input style="display:none;" type="text" class="form-control" name="id" id="id" value="{{$pas->id}}" readonly onmousedown="return false;" >
                <p class="labelName">PROYECTO</p>
                <p><input type="text" class="inputName2" name="proyecto" id="proyecto" value="" readonly onmousedown="return false;" ></p>
              </div>
              <div class="col-sm-6">
              <input style="display:none;" type="text" class="form-control" name="id" id="id" value="{{$pas->id}}" readonly onmousedown="return false;" >
                <p class="labelName ">FECHA</p>
                <p><input class="inputName2" type="text" id="fecha" name="fecha"  value="<?php
                    date_default_timezone_set('America/La_Paz');
                    echo date('m/d/y g:ia');?>" readonly onmousedown="return false;"></p>
              </div>
              <div class="col-sm-12">
              <p class="labelName">ACTIVIDAD</p>
                <p><textarea  name="actividades" id="actividades" id="" cols="30" rows="10" class="inputName3" readonly onmousedown="return false;"></textarea></p>  
                <p class="labelName">LINK DE CONTENIDO</p>
                <p><input type="text" class="inputName" name="link_imagenes" id="link_imagenes" value="" readonly onmousedown="return false;"></p> 
                
                <input class="marcar" id="submit" type="submit" value="MARCAR ENTRADA">
              </div> 
              
            </div>
          </form>
          
          
         @endif

  
         @if($pas->estado == 'Activo')
        <form class="form" action="{{url('/control/marcarsalida')}} " enctype="multipart/form-data"  method="get">
          <div class="row rowForm">
              <div class="col-sm-6">
              <input style="display:none;" type="text" class="form-control" name="id" id="id" value="{{$pas->id}}" readonly onmousedown="return false;" >
                <p class="labelName">PROYECTO</p>
                <p><input type="text" class="inputName2" name="proyecto" id="proyecto" value=""  ></p>
              </div>
              <div class="col-sm-6">
                <p class="labelName ">FECHA</p>
                <p><input class="inputName2" type="text" id="fecha" name="fecha"  value="<?php
                    date_default_timezone_set('America/La_Paz');
                    echo date('m/d/y g:ia');?>" readonly onmousedown="return false;"></p>
              </div>
              <div class="col-sm-12">
                <p class="labelName">ACTIVIDAD</p>
                <p><textarea  name="actividades" id="actividades" id="" cols="30" rows="10" class="inputName3"></textarea></p>  
                <p class="labelName">LINK DE CONTENIDO</p>
                <p><input type="text" class="inputName" name="link_imagenes" id="link_imagenes" value=""></p> 
                <!--<button class="marcar">MARCAR SALIDA</button>-->
                <input class="marcar" id="submit" type="submit" value="MARCAR SALIDA">
              </div> 
            </div>  




        </form>
        @endif
        <a href="{{url('/')}}" class="marcar">SALIR</a>
             
        
      </div>
      
    </div> 
  </body>
</html>


<img src="{{asset('img\figuras\circuloBordeBlancoPqno.png')}}" alt="" class="circuloBordeBlanco">
<img src="{{asset('img\figuras\puntosBlackLargo.png')}}" alt="" class="puntosTurquesa">
<img src="{{asset('img\figuras\zigZagBlack.png')}}" alt="" class="zigzagblack">
<img src="{{asset('img\figuras\trianguloBordeGray.png')}}" alt="" class="trianguloBordeGray">
		
		

