@extends('layouts.app')

@section('content')
<!--<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
            </div> <link rel="stylesheet" href="css/style.css"> 
        </div>onclick='return $resul' 

    



    </div>
</div>-->

<?php
if ( Auth::user()->rol_asignado == "administrador")
{
    $datos ='';
    $val = 'true';
}
else
{
    $datos='display: none';
    $val = 'false';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css"> 
</head>
<body>

<div id ="content">
    <section class="container">
        <div class="row">
            <div class="col-lg-9">
                <h1 class="font-weight-bold mb-0">Bienvenido  {{ Auth::user()->name }}  </h1>
                <p class="lead text-muted">Te mostramos la cantidad de registros</p>
                
                

            </div>
            <div class="col-lg-3">
                <a class="btn btn-primary" href="{{url('home/create')}}">Registrar usuario</a>
                
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="colg-lg-3 stat">
                            <h6 class="tect-muted">Personal registrado</h6>
                            <h3 class="font-weight-bold">{{count($personal)}}</h3>
                            <a href="{{url('/home/personal')}}">Ver personal</a></li>

                        </div>
                        <div class="colg-lg-3 stat">
                            <h6 class="tect-muted">Libros registrados</h6>
                            <h3 class="font-weight-bold">{{count($biblioteca)}}</h3>
                            <a href="{{url('/home/biblioteca')}}">Ver Libros</a></li>
                        </div>
                        <div class="colg-lg-3 stat">
                            <h6 class="tect-muted">Noticias registradas</h6>
                            <h3 class="font-weight-bold">{{count($noticia)}}</h3>
                            <a href="{{url('/home/noticias')}}">Ver noticias</a></li>
                            
                        </div>
                        <div class="colg-lg-3 stat">
                            <h6 class="tect-muted">Cursos registrados</h6>
                            <h3 class="font-weight-bold">{{count($curso)}}</h3>
                            <a href="{{url('/home/cursos')}}">Ver cursos</a></li>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="container">

    <table class="table table-light">
    <thead class="thead-light">
        <tr>
        	
            <th>id</th>
            <th>Nombre</th>
            <th>Correo</th>
            <th>rol</th>
            <th>Accion</th>
        </tr>
    </thead>
    <tbody>
    @foreach($usuarios as $usuario)
	
        <tr>
            <td>{{$usuario->id}}</td>
            <td>{{$usuario->name}}</td>
            <td>{{$usuario->email}}</td>
            <td>{{$usuario->rol_asignado}}</td>
            <td>
            <a href="{{url('/home/'.$usuario->id.'/edit')}}" class="btn btn-warning" style="<?php echo $datos; ?>"  onclick="return <?php echo $val;?>">
            Editar
            
            </a>
                
             || 
                <form action="{{url('/home/'.$usuario->id)}}" class="d-inline" method="post">
                @csrf
                {{method_field('DELETE')}}
                <input type="submit" class="btn btn-danger" onclick="return confirm('Seguro que quieres eliminar')" value="Borrar" style="<?php echo $datos; ?>"  onclick="return <?php echo $val;?>">
                </form>
                
            </td>
            

        </tr>
        @endforeach
        
    </tbody>
</table>
{{$usuarios->links()}}
</div>

    
</div>




    
</body>
</html>





@endsection
