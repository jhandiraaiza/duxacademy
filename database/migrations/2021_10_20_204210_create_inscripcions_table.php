<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInscripcionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inscripcions', function (Blueprint $table) {
            $table->id();
            $table->String('capacitacion');
            $table->String('nombre');
            $table->String('apellido');
            $table->String('correo');
            $table->String('pais');
            $table->String('ciudad');
            $table->String('cod');
            $table->String('celular');
            $table->String('genero');
            $table->String('fec_na');
            $table->String('colegio');
            $table->String('curso');
            $table->String('carrera');
            $table->String('permiso');
            $table->String('dd');
            $table->String('codigoname');
            $table->String('tipo')->nullable();;
            $table->String('num_cuenta');
            $table->String('monto');
            $table->String('imagen');
            $table->Text('mensaje')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inscripcions');
    }
}
