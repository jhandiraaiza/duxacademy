<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePremiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('premios', function (Blueprint $table) {
            $table->id();
            $table->String('nombre');
            $table->String('fecha');
            $table->String('institucion');
            $table->text('descripcion');
            $table->String('pais');
            $table->String('imagen');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('premios');
    }
}

/*NOMBRE	FECHA (Mes, año)	INSTITUCION	DESCRIPCION	PAIS	IMAGEN (.jpg) */
