<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCursosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cursos', function (Blueprint $table) {
            $table->id();
            $table->String('nombrecurso');
            $table->Text('descripcion');
            //$table->String('cupos');
            $table->String('categoria');
            $table->String('tipo');
            $table->String('institucion');
            $table->String('logo_ins');
            //$table->String('empresa');
            $table->String('instructor');
            $table->String('foto');
            //$table->String('duracion');
            $table->String('Modalidad');
            $table->String('fecha');
            $table->String('publico');
            $table->String('certificado');
            $table->String('temario');
            //$table->String('F_inicio');
            //$table->String('F_inscripcion');
            /*$table->String('Precio_des');*/
            $table->String('Precio');
            $table->String('imagen');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cursos');
    }
}

/*		FECHA	PUBLICO	CERTIFICADO	MODALIDAD	TEMARIO */
