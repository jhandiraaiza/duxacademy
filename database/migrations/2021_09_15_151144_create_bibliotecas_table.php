<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBibliotecasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bibliotecas', function (Blueprint $table) {
            $table->id();
            $table->String('titulo');
            $table->String('autor');
            $table->String('editorial');
            $table->String('edicion');
            $table->String('categoria');
            $table->Text('sinopsis');
            $table->String('imagen');
            $table->String('archivo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *titulo autor descripcion imagen de portada y archivo 
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bibliotecas');
    }
}
