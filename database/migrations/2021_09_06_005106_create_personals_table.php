<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\rol;

class CreatePersonalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rols', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre_rol');
            $table->timestamps();
        });

        

        Schema::create('personals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('apellidos');
            $table->string('ci');
            $table->string('fecha_nac');
            $table->string('area');
            $table->string('ciudad');
            $table->string('pais');
            $table->string('genero');
            $table->string('celular');
            $table->string('correo');
            $table->text('descripcion');
            $table->string('foto');
            $table->String('inicio')->nullable();
            $table->String('fin')->nullable();
            $table->String('estado')->nullable();
            $table->String('proyecto')->nullable();
            $table->String('actividades')->nullable();
            $table->String('link_imagenes')->nullable();
            $table->unsignedInteger('rol_id')->unsigned();
            $table->foreign('rol_id')->references('id')->on('rols');
            
           
            $table->timestamps();
        });

        rol::create([
            'nombre_rol'      => 'administrador',
        ]);
        rol::create([
            'nombre_rol'      => 'pasante',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personals');
        Schema::dropIfExists('rols');
    }
}
